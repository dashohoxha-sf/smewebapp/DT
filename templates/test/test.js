// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set()
{
  update_timeframe();
}

function set_curr_day()
{
  var form = document.test;
  session.setVar("simulated_current_day", form.curr_day.value);
}

function set_curr_month()
{
  var form = document.test;
  session.setVar("simulated_current_month", form.curr_month.value);
}

function set_curr_year()
{
  var form = document.test;
  session.setVar("simulated_current_year", form.curr_year.value);
}

function set_actual_values()
{
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth() + 1;
  var year = date.getFullYear();
  var form = document.test;

  session.setVar("simulated_current_day", ''+day);
  session.setVar("simulated_current_month", ''+month);
  session.setVar("simulated_current_year", ''+year);

  form.curr_day.value = day;
  form.curr_month.value = month;
  form.curr_year.value = year;

  update_timeframe();
}

function update_timeframe()
{
  var month = session.getVar("simulated_current_month");
  var year = session.getVar("simulated_current_year");

  if (session.isset("select_by_time->currMonth"))
    {
      session.setVar("select_by_time->currMonth", int2mon(month));
      session.setVar("select_by_time->currYear", year);
      session.setVar("select_by_time->fromMonth", int2mon(month));
      session.setVar("select_by_time->fromYear", year);
      session.setVar("select_by_time->toMonth", int2mon(month));
      session.setVar("select_by_time->toYear", year);
    }

  if (session.isset("time_frame->currMonth"))
    {
      session.setVar("time_frame->currMonth", int2mon(month));
      session.setVar("time_frame->currYear", year);
    }
}

function int2mon(m_id)
{
  var arr_months = new Array(
			     'Jan', 'Feb', 'Mar', 'Apr', 
			     'May', 'Jun', 'Jul', 'Aug', 
			     'Sep', 'Oct', 'Nov', 'Dec' );
  return arr_months[m_id - 1];
}
