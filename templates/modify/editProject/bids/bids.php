<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class bids extends WebObject
{
  function eventHandler($event)
    {
      $params = $event->args;
      switch ($event->name)
        {
        case "add":
          WebApp::execDBCmd("addBid", $params);
          break;
        case "remove":
          WebApp::execDBCmd("removeBid", $params);
          break;
        case "saveDescr":
          WebApp::execDBCmd("saveBidDescr", $params);
          break;
        }
    }

  function onRender()
    {
      $rs = WebApp::openRS("getBidDescr");
      $bid_descr = $rs->Field("bid_description");
      if ($bid_descr==NULL_VALUE)  $bid_descr="";
      WebApp::addVar("bid_description", $bid_descr);
    }
}
?>