// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function saveUserdata()
{
  var event_args;
  var form = document.userdata;
  var firstname = form.firstname.value;
  var lastname = form.lastname.value;
  var old_password = form.old_password.value;
  var new_password_1 = form.new_password_1.value;
  var new_password_2 = form.new_password_2.value;
  var tel1 = form.tel1.value;
  var tel2 = form.tel2.value;
  var e_mail = form.e_mail.value;
  var address = form.address.value;
  var args = new Array();

  //check that New Password and Confirm Password are equal
  if (new_password_1!=new_password_2)
    {
      alert("New passwords are not equal! Please try again.");
      form.new_password_1.value = "";
      form.new_password_2.value = "";
      return;
    }

  //ToDo: input data validation for e-mail and telefone formats

  args.push("firstname=" + firstname);
  args.push("lastname=" + lastname);
  args.push("new_password=" + new_password_1);
  args.push("old_password=" + old_password);
  args.push("tel1=" + tel1);
  args.push("tel2=" + tel2);
  args.push("e_mail=" + e_mail);
  args.push("address=" + encode_arg_value(address));

  event_args = args.join(';');
  GoTo("thisPage?event=userdata.save("+event_args+")");
}
