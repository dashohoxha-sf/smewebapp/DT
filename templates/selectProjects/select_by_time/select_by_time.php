<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class select_by_time extends WebObject
{
  function init()
    {
      $curr_month = get_curr_date("M");
      $curr_year = get_curr_date("Y");
      $this->addSVars( array( "currMonth" => $curr_month,
                              "currYear" => $curr_year,
                              "fromMonth" => $curr_month,
                              "fromYear" => $curr_year,
                              "toMonth" => $curr_month,
                              "toYear" => $curr_year,
                              "currMonthCheck" => "checked"
                              ) );
    }

  function onRender()
    {
      $rs = $this->get_month_RS();

      //add the recordset of months to the $webPage
      $rs->ID = "monthList";
      global $webPage;
      $webPage->addRecordset($rs);

      //add the {{vars}} for the selected months
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $mon = $rs->Field("Mon");
          WebApp::addVar("from_sel_$mon", "");
          WebApp::addVar("to_sel_$mon", "");
          $rs->MoveNext();
        }
      $fromMonth = $this->getSVar("fromMonth");
      $toMonth = $this->getSVar("toMonth");
      WebApp::addVar("from_sel_".$fromMonth, "selected");
      WebApp::addVar("to_sel_".$toMonth, "selected");
    }

  function get_month_RS()
    //get a recordset of months
    {
      $rs = new EditableRS;
      $rs->Open();
      for ($i=1; $i<=12; $i++)
        {
          $rec = array("m_id"=>$i, "Mon"=>int2mon($i));
          $rs->addRec($rec);
        }
      $rs->MoveFirst();
      return $rs;
    }
}
?>