// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function addBid()
{
  var form = document.bids;
  var bidder = form.bidder.value;
  var bid_amount = form.bid_amount.value;
  var tech_points = form.tech_points.value;
  var fin_points = form.fin_points.value;
  var args = new Array();

  args.push("bidder=" + bidder);
  args.push("bid_amount=" + bid_amount);
  args.push("tech_points=" + tech_points);
  args.push("fin_points=" + fin_points);

  GoTo("thisPage?event=bids.add(" + args.join(';') + ")");
}

function removeBid(bid_id)
{
  GoTo("thisPage?event=bids.remove(bid_id=" + bid_id + ")");
}

function saveDescription()
{
  var descr = document.bids.description.value;
  GoTo("thisPage?event=bids.saveDescr(bid_description=" + descr + ")");
}
