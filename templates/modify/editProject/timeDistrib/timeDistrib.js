// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function addDistrib()
{
  if (!editable())  return;

  var form = document.timeDistrib;
  var mList = form.monthList;
  var idx = mList.selectedIndex;
  var month = mList.options[idx].value;
  var year = form.year.value;
  var amount = form.amount.value;

  //check that the month selected is not before the current month
  var curr_date = get_curr_date();
  var selected_date = new Date(year, month, 31);
  if (selected_date < curr_date)
    {
      alert("The month selected cannot be before the current month.");
      return;
    }

  var args = Array();
  args.push("month=" + month);
  args.push("year=" + year);
  args.push("amount=" + amount);
  
  GoTo("thisPage?event=timeDistrib.add(" + args.join(";") + ")");
}

function rmDistrib(distrib_id)
{
  if (!editable())  return;
  GoTo("thisPage?event=timeDistrib.rm(distrib_id=" + distrib_id + ")");
}

function rmAllDistribs()
{
  if (!editable())  return;
  GoTo("thisPage?event=timeDistrib.rmAll");
}

/** returns true if the month selected is before the current month */
function selected_date_before_current(month, year)
{
  var curr_date = get_curr_date();
  var selected_date = new Date(year, month, 28);
  if (selected_date < curr_date)
    {
      alert("The month selected cannot be before the current month.");
      return true;
    }
  else
    {
      return false;
    }
}
