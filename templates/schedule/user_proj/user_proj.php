<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class user_proj extends WebObject
{

  function eventHandler($event)
    {
      global $session,$event;
      extract($event->args);

      switch ($event->name)
	{
	case "update_user_proj_oret":
	  WebApp::addVar("user_id", $user_id);
	  WebApp::addVar("proj_id", $user_proj_id);
	  WebApp::addVar("nr_oret_total", $user_oret_total);
	  WebApp::execDBCmd("insert_oret_total");
                              
                              
	  $project_user_oret_aray = explode(",", $user_proj_java_oret);
	  $user_java_data_array   = explode(",", $user_proj_java_data);
                              
	  $i = 0;
                              
	  while ($i < count($user_java_data_array)):
                                     
	    //$java_data_array = explode(".", $user_java_data_array[$i]);
	    //$java_data = $java_data_array[2]."-".$java_data_array[1]."-".$java_data_array[0];
	    $java_data = $user_java_data_array[$i];
                                     
	  WebApp::addVar("java_data", $java_data);
	  WebApp::addVar("nr_oret_java", $project_user_oret_aray[$i]);

	  WebApp::execDBCmd("insert_oret_java");

	  $rs = WebApp::openRS("user_tjeter_id");
	  $rs->MoveFirst();
	  while (!$rs->EOF())
	    {
	      $user_tjeter_id = $rs->Field("user_tjeter_id");
	      WebApp::addVar("user_tjeter_id", $user_tjeter_id);

	      $rs_count = WebApp::openRS("count_oret_java");
	      IF(!$rs_count->EOF())
		{$count_oret_java = $rs_count->Field("count_oret_java");}
                                           
	      IF ($count_oret_java == 0)
		{WebApp::execDBCmd("insert_oret_java_user_tjeter");}
                                            
	      $rs->MoveNext();
	    }

                                     
	  $i++;
	  endwhile;
	  break;
	}
    }

  function onRender()
    {
      global $session,$event;
      extract($event->args);

      $nr_bg = 0;
      $nr    = 0;
     
      //get the start and end time of the timeframe
      $currMonthCheck = WebApp::getSVar("select_by_time->currMonthCheck");
      if ($currMonthCheck=="checked")
        {
          $fromMonth = get_curr_date("M");  //current month
          $toMonth = get_curr_date("M");
          $fromYear = get_curr_date("Y");   //current year
          $toYear = get_curr_date("Y");
          $timeFrame = "$toMonth $toYear";
        }
      else
        {
          $fromMonth = WebApp::getSVar("select_by_time->fromMonth");
          $toMonth   = WebApp::getSVar("select_by_time->toMonth");
          $fromYear  = WebApp::getSVar("select_by_time->fromYear");
          $toYear    = WebApp::getSVar("select_by_time->toYear");
          $timeFrame = "From $fromMonth $fromYear To $toMonth $toYear";
        }

      //format the starting and ending date of the timeframe
      //in the format required by the database: yyyy-mm-dd;
      //start of the timeframe is 01 of the start month,
      //end of the timeframe is 01 of the next to end month
      $t1 = strtotime("01 $fromMonth $fromYear");
      $data_fillim_interval = date("d.m.Y", $t1);
      $t1 = date("Y-m-d", $t1);
      $t2 = strtotime("01 $toMonth $toYear");
      $t2 = mktime(0,0,0, date("m",$t2)+1, 1, date("Y",$t2));
      $data_mbarim_interval = date("d.m.Y", $t2);
      $t2 = date("Y-m-d", $t2);

      $data_fillim = $t1;
      $data_mbarim = $t2;
      $user_id     = WebApp::getSVar("user_proj->user_proj_id");
             
      $data_interval = $data_fillim_interval." - ".$data_mbarim_interval;
      WebApp::addVar("data_interval",$data_interval);
      //meret emri i userit -----------------------------------------------------
      WebApp::addVar("user_id",$user_id);
      $rs = WebApp::openRS("user");
      IF(!$rs->EOF())
	{$firstname = $rs->Field("firstname");
	$lastname  = $rs->Field("lastname");
	$role_name  = $rs->Field("role_name");}
      $user_i_zgjedhur = $firstname." ".$lastname." (<i>".$role_name."</i>)";
      WebApp::addVar("user_i_zgjedhur",$user_i_zgjedhur);
      //-------------------------------------------------------------------------
      //formohet vektori i datave -----------------------------------------------
      WebApp::addVar("date_dita",$data_fillim);
      $rs = WebApp::openRS("date_dita");
      IF(!$rs->EOF())
	{$date_dita_dif = $rs->Field("date_dita");}
         
      $date_dita_dif = "-".$date_dita_dif;
               
      WebApp::addVar("date_dita_dif",$date_dita_dif);
             
      $rs = WebApp::openRS("data_java");
      IF(!$rs->EOF())
	{$data_fillim     = $rs->Field("data_java");
	$data_fillim_dmy = $rs->Field("data_muaji_viti");}
               
      //per daten e mbarimit -----------------------
      WebApp::addVar("date_dita",$data_mbarim);
      $rs = WebApp::openRS("date_dita");
      IF(!$rs->EOF())
	{$date_dita_dif = $rs->Field("date_dita");}
         
      $date_dita_dif = "-".$date_dita_dif;
               
      WebApp::addVar("date_dita_dif",$date_dita_dif);
             
      $rs = WebApp::openRS("data_java");
      IF(!$rs->EOF())
	{$data_mbarim = $rs->Field("data_java");}
              
      $i = 0;
      $java_data_array[$i]     = $data_fillim;
      $java_data_dmy_array[$i] = $data_fillim_dmy;
      WebApp::addVar("date_dita_dif","+7");
      while ($data_fillim < $data_mbarim):
                      
	WebApp::addVar("date_dita",$data_fillim);
      $rs = WebApp::openRS("data_java");
                      
      IF(!$rs->EOF())
	{$data_fillim     = $rs->Field("data_java");
	$data_fillim_dmy = $rs->Field("data_muaji_viti");}
                      
      $i++;
      $java_data_array[$i]     = $data_fillim;
      $java_data_dmy_array[$i] = $data_fillim_dmy;
      endwhile;
               
      //$gjatesia = strlen($java_data_interval) - 1;
      //$java_data_interval = substr($java_data_interval, 0, $gjatesia);
      //-----------------------------------------------------------
      $ls_koka_tabele  = "";
             
      $ls_koka_tabele  .= "<Table border=0 cellspacing=1 cellpadding=1><TR bgcolor=cccccc><TD align=center width=140><b>Projects</b></TD><TD align=center width=40><b>Total Hours</b></TD>";
      $ls_trupi_tabele = "";
             
      $i = 0;
      while ($i < count($java_data_dmy_array)):
	$ls_koka_tabele = $ls_koka_tabele."<TD align=center width=30><b>".$java_data_dmy_array[$i]."</b></TD>";
      $totali_i_oreve[$i] = 0;
      $i++;
      endwhile;
      $ls_koka_tabele = $ls_koka_tabele."</TR>";
             

      $i = count($java_data_array) - 1;
      WebApp::addVar("data_fillim_interval",$java_data_array[0]);
      WebApp::addVar("data_mbarim_interval",$java_data_array[$i]);
            
      //$rs = WebApp::openRS("proj_user_name");
      $rs = WebApp::openRS("proj_users");
      $rs->MoveFirst();
      while (!$rs->EOF())
	{
	  $proj_name = $rs->Field("proj_name");
	  $proj_id   = $rs->Field("proj_id");

	  WebApp::addVar("proj_id",$proj_id);
                 
	  $nr = $nr + 1;
                 
	  //meren oret totale te userit ------------------------------------
	  $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
	  IF(!$rs_nr_oret_total->EOF())
	    {$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
	  //----------------------------------------------------------------

	  IF ($nr_bg == 0)
	    {$bg_color = "e2e2e2";
	    $nr_bg = 1;}
	  ELSE
	    {$bg_color = "c2c2c2";
	    $nr_bg = 0;}
                     
	  $ls_trupi_tabele = $ls_trupi_tabele."<TR bgcolor=".$bg_color."><TD width=140><a href=\"javascript:GoTo('thisPage?event=user_proj.edit_oret(user_id=".$user_id.";user_proj_id=".$proj_id.")')\">".$proj_name."</a></b></TD>";
	  IF($proj_id == $user_proj_id)
	    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret_total\" value=\"".$nr_oret_total."\" size=5 maxlength=5></TD>";}
	  ELSE
	    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_total."</TD>";}
                 
	  $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;
                 
	  $i = 0;
                 
	  $rs_user_oret = WebApp::openRS("user_oret_javore_project");
	  $rs_user_oret->MoveFirst();
	  while (!$rs_user_oret->EOF())
	    {
	      $java_data            = $rs_user_oret->Field("java_data");
	      $nr_oret_javore_total = $rs_user_oret->Field("nr_oret_java");
                        
	      $user_java[$i]                 = $java_data;
	      $user_nr_oret_javore_total[$i] = $nr_oret_javore_total;
	      $i++;
	      $rs_user_oret->MoveNext();
	    }

	  $i = 0; 
	  while ($i < count($java_data_array)):
                        
	    $oret = "no";
	  $j = 0;
	  while ($j < count($user_java)):
	    {
	      IF ($java_data_array[$i] == $user_java[$j])
		{
		  IF($proj_id == $user_proj_id)
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_proj_java_oret\" value=\"".$user_nr_oret_javore_total[$j]."\" size=5 maxlength=5><input type=hidden name=\"user_proj_java_data\" value=\"".$java_data_array[$i]."\"></TD>";}
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$user_nr_oret_javore_total[$j]."</TD>";}
                                   
		  $oret = "ok";
		  $totali_i_oreve[$i] = $totali_i_oreve[$i] + $user_nr_oret_javore_total[$j];
		  break;
		}
	    }
                                     
	  $j++;
	  endwhile;
                        
	  IF ($oret == "no")
	    {
	      IF($proj_id == $user_proj_id)
		{$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_proj_java_oret\" value=\"0\" size=5 maxlength=5><input type=hidden name=\"user_proj_java_data\" value=\"".$java_data_array[$i]."\"></TD>";}
	      ELSE
		{$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>0</TD>";}
	    }
	  $i++;
	  endwhile;

	  unset($user_java);
	  unset($user_nr_oret_javore_total);
                 
	  $ls_trupi_tabele = $ls_trupi_tabele."</TR>";
                 
	  $rs->MoveNext();
	}
              
      $ls_fundi_tabele = "<TR bgcolor=d2d2d2><TD width=140><b>Total Hours</b></TD><TD align=center width=40>".$nr_oret_total_total."</TD>";
      $i = 0;
      while ($i < count($totali_i_oreve)):
	IF ($totali_i_oreve[$i] < 40)
	{$bg = "FCF2A0";}
      ELSEIF ($totali_i_oreve[$i] == 40)
	{$bg = "ACFD93";}
      ELSEIF ($totali_i_oreve[$i] > 40)
	{$bg = "FC7F7F";}
                        
      $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center bgcolor=".$bg.">".$totali_i_oreve[$i]."</TD>";
      $i++;
      endwhile;
      $ls_fundi_tabele = $ls_fundi_tabele."</TR>";
              
      IF (isset($user_proj_id))
	{$ls_html = "<form name=\"form_user_proj_oret\"><input type=hidden name=\"user_id\" value=\"".$user_id."\"><input type=hidden name=\"user_proj_id\" value=\"".$user_proj_id."\">".$ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table></form><BR><a href=\"javascript:update_user_proj_oret();\">Save</a>";}
      ELSE
	{$ls_html = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";}
     
      WebApp::addVar("ls_html",$ls_html);
     
    }
}
?>
