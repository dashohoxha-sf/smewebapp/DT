<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** 
 * The $menu_items array contains the items of the leftMenu. 
 */
$menu_items = array(
                    "change_password"   => "Change Password",
                    "edit_tables"       => "Edit Tables",
                    "empty_db"          => "Empty Database",
                    "delete_requests"   => "Delete Requests",
                    "reports"           => "Special Reports",
                    "statistics"        => "Statistics",
                    "create_db"         => "Create New DB",
                    "backup_db"         => "Backup/Restore DB",
                    "etc1"              => ". . .",
                    "etc2"              => ". . ."
                    );
?>
