<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class schedule extends WebObject
{
  function init()
    {
      $this->addSVar("file", "proj_list/proj_list.html");
      //$this->addSVar("selectFile", "listOfProjRpt/select.html");
    }

  function on_gotobox($event_args)
    {
      $box = $event_args["box"];
      switch ($box)
        {
        case "list":
          $this->setSVar("file", "proj_list/proj_list.html");
          //$this->setSVar("selectFile", "listOfProjRpt/select.html");
          break;
        case "report":
          $this->setSVar("file", "report/report.html");
          //$this->setSVar("selectFile", "summaryRpt/select.html");
          break;
        case "conflicts":
          $this->setSVar("file", "conflicts/conflicts.html");
          //$this->setSVar("selectFile", "needToSellRpt/select.html");
          break;
        case "user_proj":
          WebApp::setSVar("user_proj->user_proj_id", $event_args["user_id"]);
          $this->setSVar("file", "user_proj/user_proj.html");
          //$this->setSVar("selectFile", "needToSellRpt/select.html");
          break;
        case "edit_oret_user":
          $this->setSVar("file", "proj_schedule/proj_schedule.html");
          WebApp::setSVar("proj_schedule->proj_id", $event_args["user_proj_id"]);
          WebApp::setSVar("proj_schedule->ngjarja", "edit_oret_user");
          WebApp::setSVar("proj_schedule->user_id_oret", $event_args["user_id"]);
          break;
        }
    }

  function on_edit($event_args)
    {
      $proj_id = $event_args["proj_id"];
      WebApp::setSVar("proj_schedule->proj_id", $proj_id);
      $this->setSVar("file", "proj_schedule/proj_schedule.html");
    }
}
?>