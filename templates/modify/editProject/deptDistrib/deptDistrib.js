// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
function deptDistrib_save()
{
  if (!editable())  return;

  var form = document.deptDistrib;
  var depts = form.dept_id;
  var dept_id, percent, sum=0;
  var values = Array();

  for (i=0; depts[i]; i++)
    {
      dept_id = depts[i].value;
      percent = eval("form.dept_" + dept_id + ".value");
      sum += Math.round(percent);
      values.push(dept_id + "_" + percent);
    }

  if (sum!=100)
    {
      alert("All the percentages do not make up 100%.\n"
            + "Please correct them and save again.");
    }
  else
    {
      GoTo("thisPage?event=deptDistrib.save(values=" + values.join(',') + ")");
    }
}
