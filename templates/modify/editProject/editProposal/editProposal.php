<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class editProposal extends WebObject
{
  function onRender()
    {
      $mode = WebApp::getSVar("editProject->mode");
      if ($mode=="add")
        {
          $curr_date = get_curr_date("Y-m-d");
          WebApp::addVars( array( "proposal_due_date" => $curr_date,
                                  "expected_start_date" => $curr_date,
                                  "expected_end_date" => $curr_date,
                                  "sel1" => "checked"
                                  ) );
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getProposal");
          $fields = $rs->Fields();
          WebApp::addVars($fields);

          //add the variables that select one of the proposal statuses
          WebApp::addVars(array("sel1"=>"","sel2"=>"","sel3"=>""));
          $prop_status = $rs->Field("proposal_status");
          switch ($prop_status)
            {
            default:
            case "to_be_submited":
              WebApp::addVar("sel1", "checked");
              break;
            case "won":
              WebApp::addVar("sel2", "checked");
              break;
            case "lost":
              WebApp::addVar("sel3", "checked");
              break;
            }
        }
    }
}
?>