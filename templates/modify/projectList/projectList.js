// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function save(proj_id, status_id)
{
  var form = document.projectList;
  var comments = eval("form.comments_"+proj_id+"_"+status_id+".value");
  var args = Array();

  args.push("proj_id=" + proj_id);
  args.push("status_id=" + status_id);
  args.push("comments=" + comments);
  
  GoTo("thisPage?event=projectList.save(" + args.join(';') + ")");
}

function edit(proj_id, status_id)
{
  var event;
  event = "modify.edit(" 
    + "proj_id=" + proj_id + ";"
    + "status_id=" + status_id + ")";
  GoTo("thisPage?event="+event);
}

function rmProj(proj_id, status_id)
{
  var form = document.projectList;
  var proj_name = eval("form.proj_"+proj_id+"_"+status_id+".value");

  var msg;
  msg = "This will remove all the data of the project: \n"
    + "\t'" + proj_name + "'\n"
    + "from the database. \n\n"
    + "Are you sure that you want to delete it?";
  if (!confirm(msg))  return;

  var event;
  event = "projectList.rm(" 
    + "proj_id=" + proj_id + ";"
    + "status_id=" + status_id + ")";
  GoTo("thisPage?event="+event);
}

function rmAll()
{
  var msg, reason;

  msg = "You are going to delete all the listed projects!\n\n"
    + "Are you sure you want to delete them all?";
  if (!confirm(msg))  return;

  reason = prompt("Please enter the reason for deleting them:");

  GoTo("thisPage?event=projectList.rmAll(reason="+reason+")");
}
