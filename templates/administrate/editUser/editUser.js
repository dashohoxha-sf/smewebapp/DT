// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function addNewUser()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.editUser);
  event_args += ";roles=" + getCheckedRoles();
  event_args += ";accRightChanges=" + getAccRightChanges();
  GoTo("thisPage?event=editUser.add(" + event_args + ")");
}

function updateUser()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.editUser);
  event_args += ";roles=" + getCheckedRoles();
  event_args += ";accRightChanges=" + getAccRightChanges();
  GoTo("thisPage?event=editUser.update(" + event_args + ")");
}

function deleteUser()
{
  var msg = "You are deleting the user.";
  if (!confirm(msg))  return;
  GoTo("thisPage?event=editUser.delete");
}

function data_not_valid()
     //returns true or false after validating the data of the form
{
  var form = document.editUser;
  var username = form.username.value;
  var idx1 = form.office.selectedIndex;
  var office = form.office.options[idx1].value;
  var idx2 = form.department.selectedIndex;
  var department = form.department.options[idx2].value;
  var admin_id = form.admin.value;

  //username should not be empty, otherwise 
  //the user cannot be accessed anymore
  username = username.replace(/ +/, '');
  if (username=='')
    {
      alert("Please enter a username.");
      return true;
    }
  //a user added by an admin must have a department
  //otherwise the admin cannot access it anymore,
  //there is not such a restriction for the superuser
  if (admin_id!="0") //"0" is the user_id of the superuser
    {
      if (office=="0")
        {
          alert("Please select an office.");
          return true;
        }
      if (department=="")
        {
          alert("Please select a department.");
          return true;
        }
    }

  return false;
}

function getCheckedRoles()
{
  var roles = document.editUser.roles;
  var checkedRoles = new Array;
  for(i=0; roles[i]; i++)
    {
      if (roles[i].checked)
        {
          checkedRoles.push(roles[i].value);
        }
    }

  return checkedRoles.join();
}

function select_role(chkbox)
{
  //uncheck all
  var roles = document.editUser.roles;
  for(i=0; roles[i]; i++)
    {
      if (roles[i].checked)  roles[i].checked=false;
    }
  //check this one
  chkbox.checked = true;
}
