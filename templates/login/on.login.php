<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//check if the user's Id and user's password are correct
extract($event->args);
$valid = validate($username, $password);
if ( !$valid )
{       
  //the user is not authenticated, go to the info page
  $event->targetPage = "dt_info.html";
  $event->target = "login";
  $event->name = "PasswdError";
  WebApp::addGlobalVar("login_error", "true");
  WebApp::message("Wrong username or password!");
}
else
{
  //open the main page
  $event->targetPage = "main.html";
  $event->target = "main";
  $event->name = "EvtLogin";
}


function validate($username, $passwd)
     //checks if a given username and password are valid
     //returns true or false
{
  global $session;

  if ($username=="superuser")
    {
      $rs = WebApp::execQuery("SELECT password FROM su");
      $p = $rs->Field("password");
      if ($p<>$passwd)
        {
          return false;
        }
      else
        {
          WebApp::addSVar("username", $username, "DB");
          WebApp::addSVar("password", $passwd, "DB");
          WebApp::addSVar("u_id", "0", "DB");
          return true;
        }
    }

  $query = "SELECT * FROM users WHERE username='$username'";
  $rs = WebApp::execQuery($query);

  if ($rs->EOF())  return false; //query returned no records
  $p = $rs->Field("password");
  if ($p <> $passwd)  return false; //the password is wrong

  //the user is valid, save some data 
  //in the session and return true
  WebApp::addSVar("username", $username, "DB");
  WebApp::addSVar("password", $passwd, "DB");
  WebApp::addSVar("u_id", $rs->Field("user_id"), "DB");
  WebApp::addSVar("u_office", $rs->Field("office"), "DB");
  WebApp::addSVar("u_dept", $rs->Field("department"), "DB");
  
  WebApp::addSVar("firstname", $rs->Field("firstname"));
  WebApp::addSVar("lastname", $rs->Field("lastname"));

  return true;
}
?>