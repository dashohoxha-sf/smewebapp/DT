// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
function view_projects(req_id)
{
  SendEvent('delete_requests', 'view', "request_id="+req_id);
}

function deny(req_id)
{
  SendEvent('delete_requests', 'deny', "request_id="+req_id);
}

function approve(req_id, row_nr)
{
  var msg = "You are removing all the projects in the request: "+row_nr+".";
  if (confirm(msg))
    {
      SendEvent('delete_requests', 'approve', "request_id="+req_id);
    }
}
