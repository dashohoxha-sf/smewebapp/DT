// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
function update_oret_user()
{
  var proj_id         = document.form_user_oret.proj_id.value;
  var user_id         = document.form_user_oret.user_id.value;
  var user_oret_total = document.form_user_oret.user_oret_total.value;
  var user_oret       = oret_check('1', 'java');
  var user_java_data  = oret_check('3', 'java_data');

  if(!(user_oret_total>0))
    {alert("Oret totale duhet te jene me te medha se 0!");
    document.form_user_oret.user_oret_total.focus();
    return}
          
  if(user_oret!='')
    {GoTo('thisPage?event=proj_schedule.update_user_oret(proj_id='+proj_id+';project_user_id='+user_id+';user_oret_total='+user_oret_total+';project_user_oret='+user_oret+';user_java_data='+user_java_data+')');}
  else 
    {alert('Oret duhet te jene number me i math se 0!');}
}

function update_oret_total()
{
  var proj_id   = document.form_user_oret.proj_id.value;
  var user_oret = oret_check('1', 'total');
  var user_id   = oret_check('2', 'total');

  var alrt = '';
          
  alrt = 'Oret duhet te jene number me i math se 0!';
        
  if(user_oret!='')
    {GoTo('thisPage?event=proj_schedule.oret_total(proj_id='+proj_id+';project_user_oret='+user_oret+';project_user_id='+user_id+')');}
  else 
    {alert(alrt);}
}

function update_oret_java()
{
  var java_data = document.form_user_oret.java_data.value;
  var proj_id   = document.form_user_oret.proj_id.value;
  var user_oret = oret_check('1', 'java');
  var user_id   = oret_check('2', 'java');

  //kontrollohet nese dita eshte e hene -----------------------------------
  //var java_data_data = new Date(java_data);
  //var dita_data = java_data_data.getDay();
  //var muaji_data = java_data_data.getMonth();
  //var viti_data = java_data_data.getFullYear();
        
        
  //if(dita_data != 1)
  //  {alert("Data e zgjedhur nuk eshte e Hene. Ju lutemi beni korigjimin!");
  //   document.form_user_oret.java_data.focus();
  //   return;}
  //-----------------------------------------------------------------------       
        
  if(user_oret!='')
    {GoTo('thisPage?event=proj_schedule.oret_java(proj_id='+proj_id+';java_data_oret='+java_data+';project_user_oret='+user_oret+';project_user_id='+user_id+')');}
  else 
    {alert('Oret duhet te jene number me i math ose i barabarte me 0!');}
}

function oret_check(arg,total_java) 
{
  var form = document.form_user_oret;
  if (arg=='1')
    {var selection = form.user_oret;}
  else if (arg=='2')
    {var selection = form.user_id;}      
  else
    {var selection = form.user_java_data;}       
        
  var selected = "";
  if (selection.length > 1)
    {
      for(var i=0; i<selection.length; i++)
	{
	  selection[i].value = left_right_trim(selection[i].value);
	  if(selection[i].value=='')
	    {
	      if(total_java == 'total')
		{selection[i].focus();
		selected = '';
		return selected;}
	      else
		{selected += ",0";}
	    }
	  else
	    {
	      if(total_java == 'total')
		{
		  if(selection[i].value > 0)
		    {selected += ","+selection[i].value;}
		  else
		    {selection[i].focus();
		    selected = '';
		    return selected;}
		}
	      else
		{
		  if(total_java == 'java')
		    {
		      if(selection[i].value >= 0)
			{selected += ","+selection[i].value;}
		      else
			{selection[i].focus();
			selected = '';
			return selected;}
		    }
		  else
		    {selected += ","+selection[i].value;}
                               
		}
	    }
	}
      selected = selected.substr(1);
    } 
  else 
    {selected = selection.value;
            
    if(total_java == 'total')
      {
	if(selected > 0)
	  {selected = selected;}
	else
	  {selection.focus();
	  selected = '';}
      }
    else
      {
	if(total_java == 'java')
	  {             
	    if(selected >= 0)
	      {selected = selected;}
	    else
	      {selection.focus();
	      selected = '';}
	  }
	else
	  {selected = selected;}
      }
           
    }
  return selected;
}

function left_right_trim(fild_value) 
{
  while(fild_value.charAt(0)==' ')
    {fild_value=fild_value.substring(1,fild_value.length)};

  while(fild_value.charAt(fild_value.length - 1)==' ')
    {fild_value=fild_value.substring(0,fild_value.length - 1)};
       
  return fild_value;
}
   
