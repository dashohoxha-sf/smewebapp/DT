// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
function update_user_proj_oret()
{
  var user_proj_id    = document.form_user_proj_oret.user_proj_id.value;
  var user_id         = document.form_user_proj_oret.user_id.value;
  var user_oret_total = document.form_user_proj_oret.user_oret_total.value;

  var user_proj_java_oret  = oret_check('1');
  var user_proj_java_data  = oret_check('2');

  if(!(user_oret_total>0))
    {alert("Oret totale duhet te jene me te medha se 0!");
    document.form_user_oret.user_oret_total.focus();
    return}
          
  if(user_proj_java_oret!='')
    {GoTo('thisPage?event=user_proj.update_user_proj_oret(user_proj_id='+user_proj_id+';user_id='+user_id+';user_oret_total='+user_oret_total+';user_proj_java_oret='+user_proj_java_oret+';user_proj_java_data='+user_proj_java_data+')');}
  else 
    {alert('Oret duhet te jene number me i math ose i barabarte me 0!');}
}

function oret_check(arg,total_java) 
{
  var form = document.form_user_proj_oret;
  if (arg=='1')
    {var selection = form.user_proj_java_oret;}
  else
    {var selection = form.user_proj_java_data;}  
        
  var selected = "";
  if (selection.length > 1)
    {
      for(var i=0; i<selection.length; i++)
	{
	  selection[i].value = left_right_trim(selection[i].value);
	  if(selection[i].value=='')
	    {selected += ",0";}
	  else
	    {
	      if(arg == '1')
		{
		  if(selection[i].value >= 0)
		    {selected += ","+selection[i].value;}
		  else
		    {selection[i].focus();
		    selected = '';
		    return selected;}
		}
	      else
		{selected += ","+selection[i].value;}
	    }
	}
      selected = selected.substr(1);
    } 
  else 
    {selected = selection.value;
            
    if(arg == '1')
      {             
	if(selected >= 0)
	  {selected = selected;}
	else
	  {selection.focus();
	  selected = '';}
      }
    else
      {selected = selected;}
           
    }
  return selected;
}

function left_right_trim(fild_value) 
{
  while(fild_value.charAt(0)==' ')
    {fild_value=fild_value.substring(1,fild_value.length)};

  while(fild_value.charAt(fild_value.length - 1)==' ')
    {fild_value=fild_value.substring(0,fild_value.length - 1)};
       
  return fild_value;
}
   
