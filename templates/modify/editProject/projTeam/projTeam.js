// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function addMember()
{
  if (!editable())  return;

  var form = document.projTeam;
  var task = form.task.value;
  var list = document.projTeam.list;
  var idx = list.selectedIndex;
  var user_id = list.options[idx].value;
  var event_args = "user_id=" + user_id + ";task=" + task;

  GoTo("thisPage?event=projTeam.add(" + event_args + ")");
}

function rmMember(id)
{
  if (!editable())  return;
  GoTo("thisPage?event=projTeam.rm(id=" + id + ")");
}

function rmAllMembers()
{
  if (!editable())  return;
  GoTo("thisPage?event=projTeam.rmAll");
}
