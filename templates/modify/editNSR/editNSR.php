<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class editNSR extends WebObject
{
  var $department;

  function editNSR()
    {
      //ensure single selection
      global $webPage;
      $select_by_dept = $webPage->getObject("select_by_dept");
      $select_by_dept->ensure_single_selection();

      $dept = WebApp::getSVar("select_by_dept->values");
      list($off_id, $dept_id) = explode("_", $dept);
      $this->department = $dept_id;
    }

  function on_save($event_args)
    {
      extract($event_args);

      //update the previous month value
      $params = array(
                      "nsr_id" => $prev_id,
                      "nsr" => $prev_month
                      );
      WebApp::execDBCmd("updateNSR", $params);

      //update the current month value
      $params = array(
                      "nsr_id" => $curr_id,
                      "nsr" => $curr_month
                      );
      WebApp::execDBCmd("updateNSR", $params);
    }

  function on_saveNsrB($event_args)
    {
      WebApp::execDBCmd("updateNSRBudget", $event_args);
    }

  function onRender()
    {
      $curr_month = get_curr_date("n");
      $curr_year = get_curr_date("Y");
      $this->add_nsr_vars($curr_month, $curr_year);
      $this->add_nsrBudget_vars($curr_month, $curr_year);
    }

  function add_nsr_vars($curr_month, $curr_year)
    {
      //get a recordset with the values of this year
      //and the last year
      $params = array(
                      "dept_id" => $this->department,
                      "curr_year" => $curr_year,
                      "prev_year" => $curr_year - 1
                      );
      $rs_nsr = WebApp::openRS("getNSR", $params);
      if ($rs_nsr->count < 24)
        {
          if ($rs_nsr->count < 12)
            {
              //add new records for the previous year
              $params["curr_year"] -= 1;
              WebApp::execDBCmd("insertNewRecs", $params);
              $params["curr_year"] += 1;
            }
          //add new records for the current year
          WebApp::execDBCmd("insertNewRecs", $params);

          //get the recordset again
          $rs_nsr = WebApp::openRS("getNSR", $params);
        }
      
      //copy $rs_nsr into an editable rs
      $rs = new EditableRS;
      $rs->Open();
      while (!$rs_nsr->EOF())
        {
          $rs->addRec($rs_nsr->Fields());
          $rs_nsr->MoveNext();
        }
      $rs->MoveFirst();

      //go to the current month in $rs
      $rs->find("month='$curr_month'");
      $rs->find_next();

      //add variables {{curr_month}}, {{curr_year}}, 
      //{{curr_month_value}} and {{curr_nsr_id}}
      WebApp::addVar("curr_month", int2mon($curr_month));
      WebApp::addVar("curr_year", $curr_year);
      WebApp::addVar("curr_month_value", $rs->Field("nsr"));
      WebApp::addVar("curr_nsr_id", $rs->Field("nsr_id"));

      //go to the previous month
      $rs->MovePrev();

      //add {{prev_month}}, {{prev_month_year}},
      //{{prev_month_value}} and {{prev_nsr_id}}
      WebApp::addVar("prev_month", int2mon($rs->Field("month")));
      WebApp::addVar("prev_month_year", $rs->Field("year"));
      WebApp::addVar("prev_month_value", $rs->Field("nsr"));
      WebApp::addVar("prev_nsr_id", $rs->Field("nsr_id"));
      
      //sum up all the NSR-s from the prev_month until the previous Jun
      while (!$rs->BOF() and $rs->Field("month") <> "6")
        {
          $sum += $rs->Field("nsr");
          $rs->MovePrev();
        }
      $sum += $rs->Field("nsr"); //include the Jun in the sum

      //add {{NSR_YTD}} and {{Jun_year}} variables
      WebApp::addVar("NSR_YTD", $sum);
      WebApp::addVar("Jun_year", $rs->Field("year"));
    }

  function add_nsrBudget_vars($curr_month, $curr_year)
    {
      //get a recordset with the values of this year
      //and the next year
      $params = array(
                      "dept_id" => $this->department,
                      "first_year" => $curr_year - 1,
                      "last_year" => $curr_year
                      );
      if ($curr_month >= 6)
        {
          $params["first_year"] = $curr_year;
          $params["last_year"] = $curr_year + 1;
        }
      $rs_nsr = WebApp::openRS("getNSRBudget", $params);
      if ($rs_nsr->count < 24)
        {
          if ($rs_nsr->count < 12)
            {
              //add new records for the first year
              $params["last_year"] -= 1;
              WebApp::execDBCmd("insertNewBudgetRecs", $params);
              $params["last_year"] += 1;
            }
          //add new records for the last year
          WebApp::execDBCmd("insertNewBudgetRecs", $params);

          //get the recordset again
          $rs_nsr = WebApp::openRS("getNSRBudget", $params);
        }

      //copy $rs_nsr into an editable rs
      $rs = new EditableRS;
      $rs->Open();
      while (!$rs_nsr->EOF())
        {
          $rs->addRec($rs_nsr->Fields());
          $rs_nsr->MoveNext();
        }
      $rs->MoveFirst();

      //get two slices from $rs
      $rs1 = $rs->slice(5,7);
      $rs2 = $rs->slice(12,5);

      //convert month_id to month name
      $rs1->apply("editNSR_int2mon");
      $rs2->apply("editNSR_int2mon");

      //add these recordsets to the $webPage
      $rs1->ID = "nsr_budget_1";
      $rs2->ID = "nsr_budget_2";
      global $webPage;
      $webPage->addRecordset($rs1);
      $webPage->addRecordset($rs2);
    }

  function get_month_RS()
    //get a recordset of months
    {
      $rs = new EditableRS;
      $rs->Open();
      for ($i=1; $i<=12; $i++)
        {
          $rec = array("m_id"=>$i, "Mon"=>int2mon($i));
          $rs->addRec($rec);
        }
      $rs->MoveFirst();
      return $rs;
    }
}

function editNSR_int2mon(&$rec)
{
  $rec["month"] = int2mon($rec["month"]);
}
?>