// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function selectUsername(textbox)
{
  //alert(textbox.value);
  session.setVar("selectUsers->username", textbox.value);
}

function selectName(textbox)
{
  //alert(textbox.value);
  session.setVar("selectUsers->name", textbox.value);
}

function selectOffice(offices)
{
  var selIdx = offices.selectedIndex;
  var off_id = offices.options[selIdx].value;
  session.setVar("selectUsers->office", off_id);
  session.setVar("selectUsers->department", "");
}

function selectDept(select)
{
  var selIdx = select.selectedIndex;
  var dept_id = select.options[selIdx].value
    session.setVar("selectUsers->department", dept_id);
}

function selectAccRight(select)
{
  //alert(option.value);
  var selIdx = select.selectedIndex;
  var accr_id = select.options[selIdx].value
    session.setVar("selectUsers->accessright", accr_id);
}
