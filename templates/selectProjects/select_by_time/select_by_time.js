// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function setCurrMonth(chkbox)
{
  var checked = (chkbox.checked ? "checked" : "");      
  session.setVar("select_by_time->currMonthCheck", checked);
}

function set_from_date()
{
  var form = document.select_by_time;

  //get selected from_month and from_year and create the object from_date
  var from_idx = form.from_month.selectedIndex;
  var from_month = form.from_month.options[from_idx].value;
  var from_year = form.from_year.value;
  var from_date = new Date(from_month+" 1, "+from_year);

  //get selected to_month and to_year and create the object to_date
  var to_idx = form.to_month.selectedIndex;
  var to_month = form.to_month.options[to_idx].value;
  var to_year = form.to_year.value;
  var to_date = new Date(to_month+" 1, "+to_year);

  //check that from_date is not greater then to_date
  if (from_date <= to_date)
    {
      session.setVar("select_by_time->fromMonth", from_month);
      session.setVar("select_by_time->fromYear", from_year);
    }
  else
    {
      alert("From date cannot be greater then To date.");

      //set From date equal to To date
      form.from_month.options[to_idx].selected = true;
      form.from_year.value = ""+to_year;
      //change it in the session as well
      session.setVar("select_by_time->fromMonth", to_month);
      session.setVar("select_by_time->fromYear", ""+to_year);
    }
}

function set_to_date()
{
  var form = document.select_by_time;

  //get selected to_month and to_year and create the object to_date
  var to_idx = form.to_month.selectedIndex;
  var to_month = form.to_month.options[to_idx].value;
  var to_year = form.to_year.value;
  var to_date = new Date(to_month+" 1, "+to_year);

  //get selected from_month and from_year and create the object from_date
  var from_idx = form.from_month.selectedIndex;
  var from_month = form.from_month.options[from_idx].value;
  var from_year = form.from_year.value;
  var from_date = new Date(from_month+" 1, "+from_year);

  //check that to_date is not smaller then from_date
  if (to_date < from_date)
    {
      alert("To date cannot be smaller then From date.");

      //set To date equal to From date
      form.to_month.options[from_idx].selected = true;
      form.to_year.value = ""+from_year;
      //change it in the session as well
      session.setVar("select_by_time->toMonth", from_month);
      session.setVar("select_by_time->toYear", ""+from_year);

      return;
    }

  //create curr_date, which is the date on the 1 of the current month
  var current_date = get_curr_date();
  var curr_year = current_date.getFullYear();
  var curr_month = current_date.getMonth();
  var curr_date = new Date(curr_year, curr_month, 1);

  //check that to_date is not greater then curr_date
  if (to_date <= curr_date)
    {
      session.setVar("select_by_time->toMonth", to_month);
      session.setVar("select_by_time->toYear", to_year);
    }
  else
    {
      var msg = "The selected date cannot be\n"
	+"greater than the current date.";
      alert(msg);

      //set To date equal to the current date
      form.to_month.options[curr_month].selected = true;
      form.to_year.value = ""+curr_year;
      //set it in the session as well
      var curr_month_value = form.to_month.options[curr_month].value;
      session.setVar("select_by_time->toMonth", curr_month_value);
      session.setVar("select_by_time->toYear", ""+curr_year);
    }
}

