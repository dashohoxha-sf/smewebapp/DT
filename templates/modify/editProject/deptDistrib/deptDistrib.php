<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class deptDistrib extends WebObject
{
  function on_save($event_args)
    {
      $values = $event_args["values"];
      $arr_values = explode(",", $values);
      for ($i=0; $i < sizeof($arr_values); $i++)
        {
          list($dept, $percent) = explode("_", $arr_values[$i]);
          $params = array("dept"=>$dept, "percent"=>$percent);
          WebApp::execDBCmd("save_deptDistrib", $params);
        }
    }
  function onRender()
    {
      //get dept distribs for this project
      $rs = WebApp::openRS("get_deptDistrib");
      if ($rs->EOF())  $this->new_deptDistribs();

      //add the template variables {{dept_x}}
      while (!$rs->EOF())
        {
          $rec = $rs->Fields();
          WebApp::addVar("dept_".$rec["dept"], $rec["percent"]);
          $rs->MoveNext();
        }
    }

  function new_deptDistribs()
    {
      $rs = WebApp::openRS("deptDistrib_departments");
      while (!$rs->EOF())
        {
          $dept_id = $rs->Field("dept_id");
          //add a 0% distribution for this department
          $params = array("dept"=>$dept_id, "percent"=>"0");
          WebApp::execDBCmd("new_deptDistrib", $params);

          //add the template variable {{dept_x}}
          WebApp::addVar("dept_".$dept_id, "0");

          //move next
          $rs->MoveNext();
        }
    }
}
?>