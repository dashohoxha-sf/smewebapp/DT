// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_to_date()
{
  var form = document.time_frame;

  //get selected month and year and create the object to_date
  var selIdx = form.to_month.selectedIndex;
  var to_month = form.to_month.options[selIdx].value;
  var to_year = form.to_year.value;
  var to_date = new Date(to_month+" 1, "+to_year);

  //create curr_date, which is the date  on the 1 of the current month
  var current_date = get_curr_date();
  var curr_year = current_date.getFullYear();
  var curr_month = current_date.getMonth();
  var curr_date = new Date(curr_year, curr_month, 1);

  //check that to_date is not greater then curr_date
  if (to_date <= curr_date)
    {
      session.setVar("time_frame->currMonth", to_month);
      session.setVar("time_frame->currYear", to_year);
    }
  else
    {
      var msg = "The selected date cannot be\n"
	+"greater than the current date.";
      alert(msg);

      //select the current month and year
      var curr_month_value = form.to_month.options[curr_month].value;
      form.to_month.options[curr_month].selected = true;
      form.to_year.value = ""+curr_year;
      session.setVar("time_frame->currMonth", curr_month_value);
      session.setVar("time_frame->currYear", ""+curr_year);
    }
}
