<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class listOfUsers extends WebObject
{
  function init()
    {
      $this->addSVar("currentUser", UNDEFINED);      
      //set current the first user in the list
      $this->selectFirst();  
    }

  function on_refresh($event_args)
    {
      //rebuild the query condition
      global $webPage;
      $selectUsers = $webPage->getObject("selectUsers");
      $selectUsers->buildQueryCondition();

      //recount the selected users
      $rs_id = WebApp::getVar("selectedUsers");
      WebApp::setSVar($rs_id."->recount", "true");

      //select the first one in the current page
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      //set currentUser to the selected one
      $rs = WebApp::openRS("getUser", $event_args);
      if ($rs->EOF())
        {
          $this->selectFirst();   
        }
      else
        {
          $user = $event_args["user_id"];
          $this->setSVar("currentUser", $user);
        }
    }

  function on_addNewUser($event_args)
    {
      //when the button AddNewUser is clicked
      //make currentUser UNDEFINED
      $this->setSVar("currentUser", UNDEFINED);
    }

  function on_setPage($event_args)
    {
      $page = $event_args["page"];
      $rs_id = WebApp::getVar("selectedUsers");
      WebApp::setSVar($rs_id."->current_page", $page);
      $this->selectFirst();  
    }

  function selectFirst()
    //set currentUser as the first user in the list
    {  
      $rs_id = WebApp::getVar("selectedUsers");
      $rs = WebApp::openRS($rs_id);
      $first_user = $rs->Field("user_id");
      $this->setSVar("currentUser", $first_user);
    }
}
?>