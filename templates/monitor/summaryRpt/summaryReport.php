<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class summaryReport extends WebObject
{
  function onRender()
    {
      //add the variable {{departments}}
      $depts = WebApp::getSVar("select_by_dept->names");
      if ($depts=="")  $depts="(all)";
      $this->setSVar("departments", $depts);

      $contracted = 0;
      $proposed = 0;
      $qualified = 0;
      $targeted = 0;
      $dis_contracted = 0;
      $dis_proposed = 0;
      $dis_qualified = 0;
      $dis_targeted = 0;

      //get a recordset with all the selected projects
      $rs = $this->getProjects();

      //iterate over the projects
      while (!$rs->EOF())
        {
          $fees = $rs->Field("fees");
          $prob = $rs->Field("prob");
          $w_fees = $fees * $prob / 100;

          $status = $rs->Field("status_id");
          switch ($status)
            {
            case CONTRACTED:
              $contracted += $fees;
              $dis_contracted += $fees;
              break;
            case PROPOSED:
              $proposed += $fees;
              $dis_proposed += $w_fees;
              break;
            case QUALIFIED:
              $qualified +=  $fees;
              $dis_qualified +=  $w_fees;
              break;
            case OPPORTUNITY:
              $targeted +=  $fees;
              $dis_targeted +=  $w_fees;
              break;
            }
          $rs->MoveNext();
        }

      //calculate the totals
      $total = $contracted + $proposed + $qualified + $targeted;
      $dis_total = $dis_contracted + $dis_proposed 
	+ $dis_qualified + $dis_targeted;

      $contracted = $this->round($contracted);
      $proposed = $this->round($proposed);
      $qualified = $this->round($qualified);
      $targeted = $this->round($targeted);
      $total = $this->round($total);
      $dis_contracted = $this->round($dis_contracted);
      $dis_proposed = $this->round($dis_proposed);
      $dis_qualified = $this->round($dis_qualified);
      $dis_targeted = $this->round($dis_targeted);
      $dis_total = $this->round($dis_total);

      //add template {{vars}}
      $tpl_vars = array(
                        "total_contracted" => "$contracted",
                        "total_proposed"   => "$proposed",
                        "total_qualified"  => "$qualified",
                        "total_targeted"   => "$targeted",
                        "total" => "$total",
                        "discounted_total_contracted" => "$dis_contracted",
                        "discounted_total_proposed"   => "$dis_proposed",
                        "discounted_total_qualified"  => "$dis_qualified",
                        "discounted_total_targeted"   => "$dis_targeted",
                        "discounted_total" => "$dis_total"
                        );
      WebApp::addVars($tpl_vars);
    }

  /** round to one digit after the decimal point */
  function round($number)
    {
      $number = (round($number * 10)) / 10;
      return $number;
    }

  function getProjects()
    //returns a recordset with all the active projects in the 
    //selected timeframe and departments
    {
      $condition = $this->buildSelectCondition();
      $params = array("select_condition"=>$condition);
      $rs = WebApp::openRS("getProjects", $params);

      //the above recordset may contain multiple entries
      //for the same proj_id, so we must filter them and keep
      //only the one with the lowest status_id
      $rs1 = new EditableRS;
      $rs1->Open();
      $rec = $rs->Fields();
      while (!$rs->EOF())
        {
          if ($rec["proj_id"] <> $rs->Field("proj_id"))
            {
              $rs1->addRec($rec);
              $rec = $rs->Fields();
            }
          else //same proj_id 
            if ($rec["status_id"] >= $rs->Field("status_id"))
	      {
		$rec = $rs->Fields();
	      }
          $rs->MoveNext();
        }
      $rs1->addRec($rec);

      $rs1->MoveFirst();
      return $rs1;
    }

  function buildSelectCondition()
    {        
      //build department condition
      $office_depts = WebApp::getSVar("select_by_dept->values");
      $arr_depts = array();
      $arr_office_depts = explode(";",$office_depts);
      while ( list($i,$item) = each($arr_office_depts) )
        {
          list($off_id,$dept_id) = explode("_", $item);
          if ($dept_id<>"")  $arr_depts[] = $dept_id;
        }
      if (sizeof($arr_depts) > 0) 
        $dept_condition = "projects.dept_id IN ("
	  . implode(',', $arr_depts) . ")"; 

      //build select condition
      $condition = $this->timeFrameCondition();
      if ($dept_condition<>"")  $condition .= "\n AND ($dept_condition)";
      $condition = "( $condition )";
      
      return $condition;
    }

  function timeFrameCondition()
    //returns the condition for selecting the projects 
    //according to the timeframe
    {
      //get the start and end time of the timeframe
      $currMonthCheck = WebApp::getSVar("select_by_time->currMonthCheck");
      if ($currMonthCheck=="checked")
        {
          $fromMonth = get_curr_date("M");  //current month
          $toMonth = get_curr_date("M");
          $fromYear = get_curr_date("Y");   //current year
          $toYear = get_curr_date("Y");
          $timeFrame = "$toMonth $toYear";
        }
      else
        {
          $fromMonth = WebApp::getSVar("select_by_time->fromMonth");
          $toMonth   = WebApp::getSVar("select_by_time->toMonth");
          $fromYear  = WebApp::getSVar("select_by_time->fromYear");
          $toYear    = WebApp::getSVar("select_by_time->toYear");
          $timeFrame = "From $fromMonth $fromYear To $toMonth $toYear";
        }

      //add the variable {{timeFrame}}
      WebApp::addVar("timeFrame", $timeFrame);

      //format the starting and ending date of the timeframe
      //in the format required by the database: yyyy-mm-dd;
      //start of the timeframe is 01 of the start month,
      //end of the timeframe is 01 of the next to end month
      $t1 = strtotime("01 $fromMonth $fromYear");
      $t1 = date("Y-m-d", $t1);
      $t2 = strtotime("01 $toMonth $toYear");
      $t2 = mktime(0,0,0, date("m",$t2)+1, 1, date("Y",$t2));
      $t2 = date("Y-m-d", $t2);

      //build and return the condition that selects the projects
      //that are active in the timeframe, or that are opened
      //and closed inside the timeframe
      $condition = 
	'( (close_date IS NULL OR close_date>="'.$t2.'")
  OR 
   (close_date<"'.$t2.'" AND projects.register_date>="'.$t1.'"))';
      return $condition;
    }
}
?>