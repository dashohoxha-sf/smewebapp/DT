<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class userdata extends WebObject
{
  function on_save($event_args)
    {
      WebApp::execDBCmd("saveUserdata", $event_args);

      $new_password = $event_args["new_password"];
      if ($new_password != "")
        {
          //check that the old password supplied is correct
          $rs = WebApp::openRS("getPassword");
          $password = $rs->Field("password");
          $old_password = $event_args["old_password"];
          if ($old_password==$password)
            {
              //save the new password
              $query_vars["password"] = $new_password;
              WebApp::execDBCmd("savePassword", $query_vars);
              WebApp::message("Password changed successfully.");
            }
          else
            {
              $msg = "The old password you supplied is not correct!\n"
                ."Password not changed. Please try again.";
              WebApp::message($msg);
            }         
        }
    }

  function onRender()
    {
      $rs = WebApp::openRS("getUserdata");
      WebApp::addVars($rs->Fields());
    }
}
?>