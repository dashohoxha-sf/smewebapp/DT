<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class su_module extends WebObject
{
  function init()
    {
      $this->check_delete_requests();
    }

  function onParse()
    {
      $module = WebApp::getSVar("leftMenu::su->selected_item");
      switch ($module)
        {
        case "change_password":
          $module_file = "change_password/change_password.html";
          break;
        case "edit_tables":
          $module_file = "edit_tables/edit_tables.html";
          break;
        case "empty_db":
          $module_file = "empty_db/empty_db.html";
          break;
        case "delete_requests":
          $module_file = "delete_requests/delete_requests.html";
          break;
        default:
          $module_file = "not_implemented.html";
          break;
        }
      WebApp::addVar("module_file", $module_file);
    }

  function check_delete_requests()
    {
      $query = "SELECT COUNT(1) AS count FROM delete_requests";
      $rs = WebApp::execQuery($query);
      $count = $rs->Field("count");
      if ($count > 0) 
        {
          $msg = "There are $count delete requests waiting.";
          WebApp::message($msg);
          WebApp::setSVar("leftMenu::su->selected_item", "delete_requests");
        }
    }
}
?>