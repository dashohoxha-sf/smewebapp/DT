<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class delete_requests extends WebObject
{
  function on_view($event_args)
    {
      $req_id = $event_args["request_id"];
      $proj_list_condition = $this->build_proj_list_condition($req_id);
      WebApp::addGlobalVar("view_projects", "true");
      WebApp::addGlobalVar("proj_list_condition", $proj_list_condition);
    }

  function on_deny($event_args)
    {
      WebApp::execDBCmd("remove_request", $event_args);
    }

  function on_approve($event_args)
    {
      $req_id = $event_args["request_id"];
      list($proj_list, $proj_list_condition) 
        = $this->build_proj_list_conditions($req_id);

      WebApp::addVar("proj_list_condition", $proj_list_condition);
      WebApp::execDBCmd("rm_project");
      WebApp::execDBCmd("rm_deptDistrib");
      WebApp::execDBCmd("rm_timeDistrib");
      WebApp::execDBCmd("rm_managers");
      WebApp::execDBCmd("rm_partners");
      WebApp::execDBCmd("rm_team");

      WebApp::addVar("proj_list", $proj_list);
      WebApp::execDBCmd("rm_contracted");
      WebApp::execDBCmd("rm_opportunity");
      WebApp::execDBCmd("rm_proposal");
      WebApp::execDBCmd("rm_bids");

      WebApp::execDBCmd("remove_request", $event_args);
      WebApp::message("Projects deleted.");
    }

  function onRender()
    {
      global $webPage;
      $rs = $webPage->getRecordset("del_requests");
      $rs->Open();
      $rs->apply("modify_del_requests");
      $webPage->addRecordset($rs);
    }

  /**
   * Gets the proj_list of the given request 
   * and builds the proj_list_condition.
   */
  function build_proj_list_condition($req_id)
    {
      $rs = WebApp::openRS("get_proj_list", array("request_id"=>$req_id));
      $proj_list = $rs->Field("proj_list");
      $arr_proj_list = explode(",", $proj_list);
      $arr_proj_condition = array();
      for ($i=0; $i < sizeof($arr_proj_list); $i++)
        {
          $proj = $arr_proj_list[$i];
          list($proj_id,$status_id) = explode("_", $proj);
          $arr_proj_condition[] = "(proj_id=$proj_id "
	    . "AND projects.status_id=$status_id)\n";
        }
      if (sizeof($arr_proj_condition)==0)
        {
          $proj_list_condition = "1=2";
        }
      else
        {
          $proj_list_condition = "(".implode(" OR ", $arr_proj_condition).")";
        }

      return $proj_list_condition;
    }

  /**
   * Like build_proj_list_condition(), but returns two conditions,
   * one only with proj_id-s, and one with proj_id-s and status_id-s.
   * Called by on_approve().
   */
  function build_proj_list_conditions($req_id)
    {
      $rs = WebApp::openRS("get_proj_list", array("request_id"=>$req_id));
      $proj_list = $rs->Field("proj_list");
      $arr_proj_list = explode(",", $proj_list);
      $arr_proj_condition = array();
      $arr_proj_id = array();
      for ($i=0; $i < sizeof($arr_proj_list); $i++)
        {
          $proj = $arr_proj_list[$i];
          list($proj_id,$status_id) = explode("_", $proj);
          $arr_proj_condition[] = "(proj_id=$proj_id "
	    . "AND status_id=$status_id)\n";
          $arr_proj_id[] = $proj_id;
        }
      if (sizeof($arr_proj_condition)==0)
        {
          $proj_list_condition = "1=2";
          $proj_list = "-1";
        }
      else
        {
          $proj_list_condition = "(".implode(" OR ", $arr_proj_condition).")";
          $proj_list = implode(",", $arr_proj_id);
        }

      return array($proj_list, $proj_list_condition);
    }
}

function modify_del_requests(&$rec)
{
  //change the format of the date
  $date = $rec["request_date"];
  $arr_date = explode("-", $date);
  $date = $arr_date[2]."/".$arr_date[1]."/".$arr_date[0];
  $rec["request_date"] = $date;

  //add the number of prjects
  $proj_list = $rec["proj_list"];
  $arr_proj_list = explode(",", $proj_list);
  $rec["nr_proj"] = sizeof($arr_proj_list);

  //add time_frame, status and depts
  $selection = $rec["selection"];
  $arr_selection = explode(";", $selection);
  $rec["time_frame"] = $arr_selection[0];
  $rec["status"] = $arr_selection[1];
  $rec["depts"] = $arr_selection[2];
}
?>