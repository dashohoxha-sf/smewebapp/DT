<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class proj_schedule extends WebObject
{

  function eventHandler($event)
    {
      global $session,$event;
      extract($event->args);

      //$session->Vars["proj_id"] = $proj_id;
      $session->Vars["mesazh"]  = "";
        
      $this->setSVar("proj_id", $proj_id);
      $this->setSVar("mesazh", "");
        
      switch ($event->name)
	{
	case "edit_oret_total":
	  //$session->Vars["ngjarja"] = 'edit_oret_total';
	  $this->setSVar("ngjarja", 'edit_oret_total');
	  break;
	case "edit_oret_java":
	  $this->setSVar("ngjarja", 'edit_oret_java');
	  $this->setSVar("java_data_edit", $java_data);
	  //$session->Vars["ngjarja"]        = 'edit_oret_java';
	  //$session->Vars["java_data_edit"] = $java_data;
	  break;
	case "edit_oret_user":
	  $this->setSVar("ngjarja", 'edit_oret_user');
	  $this->setSVar("user_id_oret", $user_id);
	  //$session->Vars["ngjarja"]        = 'edit_oret_user';
	  //$session->Vars["user_id_oret"]   = $user_id;
	  break;
	case "oret_total":
	  $user_id_array   = explode(",", $project_user_id);
	  $user_oret_array = explode(",", $project_user_oret);
	  $i = 0;
                
	  while ($i < count($user_id_array)):
                                     
	    WebApp::addVar("user_id", $user_id_array[$i]);
	  WebApp::addVar("nr_oret_total", $user_oret_array[$i]);

	  WebApp::execDBCmd("insert_oret_total");
                                     
	  $i++;
	  endwhile;
	  break;
	case "oret_java":
	  $user_id_array   = explode(",", $project_user_id);
	  $user_oret_array = explode(",", $project_user_oret);
	  $java_data_array = explode(".", $java_data_oret);
	  $i = 0;
	  $java_data = $java_data_array[2]."-".$java_data_array[1]."-".$java_data_array[0];
                
	  //shikohet nese dita eshte e hene --------------------
	  WebApp::addVar("proj_date",$java_data);
	  $rs = WebApp::openRS("proj_date_dita");
	  IF(!$rs->EOF())
	    {$proj_date_dita = $rs->Field("proj_date_dita");}
                              
	  IF ($proj_date_dita != 0)
	    {$session->Vars["mesazh"] = "Data e zgjedhur nuk eshte e Hene. Ju lutem beni korigjimet e duhura!";
	    //$this->setSVar("mesazh", "Data e zgjedhur nuk eshte e Hene. Ju lutem beni korigjimet e duhura!");
	    return;}
                              
	  WebApp::addVar("java_data", $java_data);
	  while ($i < count($user_id_array)):
                                     
	    WebApp::addVar("user_id", $user_id_array[$i]);
	  WebApp::addVar("nr_oret_java", $user_oret_array[$i]);

	  WebApp::execDBCmd("insert_oret_java");
                                     
	  $i++;
	  endwhile;
	  break;
	case "update_user_oret":
	  WebApp::addVar("user_id", $project_user_id);
	  WebApp::addVar("nr_oret_total", $user_oret_total);
	  WebApp::execDBCmd("insert_oret_total");
                              
                              
	  $project_user_oret_aray = explode(",", $project_user_oret);
	  $user_java_data_array   = explode(",", $user_java_data);
                              
	  $i = 0;
                              
	  while ($i < count($user_java_data_array)):
                                     
	    $java_data_array = explode(".", $user_java_data_array[$i]);
	  $java_data = $java_data_array[2]."-".$java_data_array[1]."-".$java_data_array[0];
                                     
	  WebApp::addVar("java_data", $java_data);
	  WebApp::addVar("nr_oret_java", $project_user_oret_aray[$i]);

	  WebApp::execDBCmd("insert_oret_java");
                                     
	  $i++;
	  endwhile;
	  break;
	}

    }

  function onRender()
    {
      global $session,$event;
      extract($event->args);

      $proj_id = $this->getSVar("proj_id");
      WebApp::addVar("proj_id",$proj_id);
     
      $nr_bg = 0;
     
      //----------------------------------------------------------------------------------
      $project_id_input = "<input type=hidden name=\"proj_id\" value=\"".$proj_id."\">";
     
      $mesazh_text = "";
      IF(isset($session->Vars["mesazh"]))
	{$mesazh_text = $session->Vars["mesazh"];}
        
      //merret emri i projektit -----------------------------------
      $rs = WebApp::openRS("proj_name");
      IF(!$rs->EOF())
	{$proj_name = $rs->Field("name");
	$proj_date = $rs->Field("proj_date");}
      WebApp::addVar("proj_name",$proj_name);
      //----------------------------------------------------------------------------------
    
      IF ($this->getSVar("ngjarja") == 'edit_oret_user')
	{
	  $ls_koka_tabele  = "<Table border=0 cellspacing=1 cellpadding=1><input type=hidden name=\"user_id\" value=\"".$this->getSVar("user_id_oret")."\"><TR bgcolor=cccccc><TD align=center width=110><b>User</b></TD><TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_total(proj_id=".$proj_id.")')\">Total Hours</a></TD>";
	  $ls_trupi_tabele = "";
	  $user_id_old     = "";
	  $java_user       = "";
	  $nr_rresht       = 0;
	  $nr_oret_total_total = 0;
          
	  $rs = WebApp::openRS("projects_user_oret_java");
	  $rs->MoveFirst();
	  while (!$rs->EOF())
	    {
	      $user_id          = $rs->Field("user_id");
	      $java_data        = $rs->Field("java_data");
	      $nr_oret_java     = $rs->Field("nr_oret_java");
	      $java_data_format = $rs->Field("java_data_format");

	      $nr_rresht = $nr_rresht + 1;
                 
	      IF ($nr_rresht == 1)
		{$ls_koka_tabele = $ls_koka_tabele."<TD><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";
		$java_user = $user_id;}
	      ELSE
		{
		  IF ($java_user == $user_id)
		    {$ls_koka_tabele = $ls_koka_tabele."<TD><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";}
		}
                 
	      WebApp::addVar("user_id",$user_id);
                 
	      IF ($user_id != $user_id_old)
		{
		  $j = 0;
		  //meren oret totale te userit ------------------------------------
		  $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
		  IF(!$rs_nr_oret_total->EOF())
		    {$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
		  //----------------------------------------------------------------

		  //meren emri dhe mbiemri userit ------------------------------------
		  $rs_user_name = WebApp::openRS("user_name");
		  IF(!$rs_user_name->EOF())
		    {$firstname = $rs_user_name->Field("firstname");
		    $lastname  = $rs_user_name->Field("lastname");}
		  //------------------------------------------------------------------
		  IF ($nr_bg == 0)
		    {$bg_color = "e2e2e2";
		    $nr_bg = 1;}
		  ELSE
		    {$bg_color = "f2f2f2";
		    $nr_bg = 0;}
                          
		  IF($nr_rresht > 1)
		    {$ls_trupi_tabele = $ls_trupi_tabele."</TR>";}
                     
		  $ls_trupi_tabele = $ls_trupi_tabele."<TR bgcolor=".$bg_color.">";
                 
		  IF($this->getSVar("user_id_oret") == $user_id)
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD width=110 NOWRAP><b>".$firstname." ".$lastname."</b></TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret_total\" value=\"".$nr_oret_total."\" size=5 maxlength=5></TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<TD width=30 align=center><input type=text name=\"user_oret\" value=\"".$nr_oret_java."\" size=5 maxlength=5></TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<input type=hidden name=\"user_java_data\" value=\"".$java_data_format."\">";}
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD width=110 NOWRAP><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_user(proj_id=".$proj_id.";user_id=".$user_id.")')\">".$firstname." ".$lastname."</a></TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_total."</TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<TD width=30 align=center>".$nr_oret_java."</TD>";}
                     
		  $user_id_old = $user_id;
		  $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
		  $nr_oret_total_total      = $nr_oret_total_total + $nr_oret_total;
		}
	      ELSE
		{
                     
		  IF($this->getSVar("user_id_oret") == $user_id)
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD width=30 align=center><input type=text name=\"user_oret\" value=\"".$nr_oret_java."\" size=5 maxlength=5></TD>";
		    $ls_trupi_tabele = $ls_trupi_tabele."<input type=hidden name=\"user_java_data\" value=\"".$java_data_format."\">";}
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center width=30>".$nr_oret_java."</TD>";}

		  $j++;
		  $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
		}

	      $rs->MoveNext();
	    }
                 
	  $ls_koka_tabele = $ls_koka_tabele."</TR>";
	  $ls_trupi_tabele = $ls_trupi_tabele."</TR>";
               
	  $ls_fundi_tabele = "<TR bgcolor=d2d2d2><TD align=center width=110><b>Total<b></TD><TD align=center><b>".$nr_oret_total_total."</b></TD>";
	  $j = 0;
	  while ($j < count($ls_fundi_tabele_aray)):
	    $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center width=30><b>".$ls_fundi_tabele_aray[$j]."</b></TD>";
	  $j++;
	  endwhile;
	  $ls_fundi_tabele = $ls_fundi_tabele."</TR>";
               
	  $ls_html   = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";
	  $link_ruaj = "<a href=\"javascript:update_oret_user();\">Save</a>";
	  //}
        }
      ELSEIF ($this->getSVar("ngjarja") == 'edit_oret_total')
	{
	  //shikohet nese jane percaktuar oret javore te projektit ----
	  $rs = WebApp::openRS("count_oret_java");
	  IF(!$rs->EOF())
	    {$count_oret_java = $rs->Field("count_oret_java");}
	  //-----------------------------------------------------------
     
          IF($count_oret_java == 0)
            {
	      $ls_html = "<Table border=0 cellspacing=1 cellpadding=1>";
	      $ls_html = $ls_html."<TR bgcolor=cccccc><TD align=center width=110><b>User</b></TD><TD align=center><b>Total Hours</b></TD></TR>";
	      $nr_oret_total_total = 0;
             
	      $rs = WebApp::openRS("proj_user_name");
	      $rs->MoveFirst();
	      while (!$rs->EOF())
                {
		  $user_id   = $rs->Field("user_id");
		  $firstname = $rs->Field("firstname");
		  $lastname  = $rs->Field("lastname");

		  WebApp::addVar("user_id",$user_id);
                 
		  //shikohet nese jane percaktuar oret javore te projektit ----
		  $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
		  IF(!$rs_nr_oret_total->EOF())
		    {$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
		  //-----------------------------------------------------------
                 
		  IF ($nr_bg == 0)
                    {$bg_color = "e2e2e2";
		    $nr_bg = 1;}
		  ELSE
                    {$bg_color = "f2f2f2";
		    $nr_bg = 0;}
                     
		  $ls_html = $ls_html."<TR bgcolor=".$bg_color.">";
		  $ls_html = $ls_html."<TD width=110 NOWRAP><b>".$firstname." ".$lastname."</b></TD>";
		  $ls_html = $ls_html."<TD align=center><input type=hidden name=\"user_id\" value=\"".$user_id."\"><input type=text name=\"user_oret\" value=\"".$nr_oret_total."\" size=5 maxlength=5></TD>";
		  $ls_html = $ls_html."</TR>";
                 
		  $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;
		  $rs->MoveNext();
		}
              $ls_html = $ls_html."<TR><TD align=center>Total</TD><TD align=center>".$nr_oret_total_total."</TD></Table>";
              $link_ruaj = "<a href=\"javascript:update_oret_total();\">Save</a>";
	    }
          ELSE
	    {
              $ls_koka_tabele  = "<Table border=0 cellspacing=1 cellpadding=1><TR bgcolor=cccccc><TD align=center><b>User</b></TD><TD align=center><b>Total Hours</b></TD>";
              $ls_trupi_tabele = "";
              $user_id_old     = "";
              $java_user       = "";
              $nr_rresht       = 0;
              $nr_oret_total_total = 0;
          
              $rs = WebApp::openRS("projects_user_oret_java");
              $rs->MoveFirst();
              while (!$rs->EOF())
		{
		  $user_id          = $rs->Field("user_id");
		  $java_data        = $rs->Field("java_data");
		  $nr_oret_java     = $rs->Field("nr_oret_java");
		  $java_data_format = $rs->Field("java_data_format");

		  $nr_rresht = $nr_rresht + 1;
                 
		  IF ($nr_rresht == 1)
		    {$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";
		    $java_user = $user_id;}
		  ELSE
		    {
		      IF ($java_user == $user_id)
			{$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";}
		    }
                 
		  WebApp::addVar("user_id",$user_id);
                 
		  IF ($user_id != $user_id_old)
		    {
		      $j = 0;
		      //meren oret totale te userit ------------------------------------
		      $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
		      IF(!$rs_nr_oret_total->EOF())
			{$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
		      //----------------------------------------------------------------
		      $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;
                         
		      //meren emri dhe mbiemri userit ------------------------------------
		      $rs_user_name = WebApp::openRS("user_name");
		      IF(!$rs_user_name->EOF())
			{$firstname = $rs_user_name->Field("firstname");
			$lastname  = $rs_user_name->Field("lastname");}
		      //----------------------------------------------------------------
                     
		      IF ($nr_bg == 0)
			{$bg_color = "e2e2e2";
			$nr_bg = 1;}
		      ELSE
			{$bg_color = "f2f2f2";
			$nr_bg = 0;}
                         
		      $ls_trupi_tabele = $ls_trupi_tabele."<TR bgcolor=".$bg_color."><input type=hidden name=\"user_id\" value=\"".$user_id."\">";
		      $ls_trupi_tabele = $ls_trupi_tabele."<TD width=110 NOWRAP><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_user(proj_id=".$proj_id.";user_id=".$user_id.")')\">".$firstname." ".$lastname."</a></TD>";
		      $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret\" value=\"".$nr_oret_total."\" size=5 maxlength=5></TD>";
		      $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";
                     
		      $user_id_old = $user_id;
		      $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
		    }
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";
		    $j++;
		    $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;}

		  $rs->MoveNext();
		}
                 
	      //$ls_koka_tabele = $ls_koka_tabele."<TD>dd.mm.yyyy<br><input type=text name=\"java_data\" size=11 maxlength=10 value=\"".$data_java_re."\"></TD></TR>";
	      $ls_koka_tabele = $ls_koka_tabele."</TR>";
	      $ls_trupi_tabele = $ls_trupi_tabele."</TR>";
               
	      $ls_fundi_tabele = "<TR bgcolor=d2d2d2><TD align=center width=110><b>Total<b></TD><TD align=center><b>".$nr_oret_total_total."</b></TD>";
	      $j = 0;
	      while ($j < count($ls_fundi_tabele_aray)):
		$ls_fundi_tabele = $ls_fundi_tabele."<TD align=center width=30><b>".$ls_fundi_tabele_aray[$j]."</b></TD>";
	      $j++;
	      endwhile;
	      $ls_fundi_tabele = $ls_fundi_tabele."</TR>";
               
	      $ls_html   = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";

	      $link_ruaj = "<a href=\"javascript:update_oret_total();\">Save</a>";
	    }
        }
      ELSEIF ($this->getSVar("ngjarja") == 'edit_oret_java')
	{
	  $ls_koka_tabele  = "<Table border=0 cellspacing=1 cellpadding=1><input type=hidden name=\"java_data\" size=11 maxlength=10 value=\"".$this->getSVar("java_data_edit")."\"><TR bgcolor=cccccc><TD align=center><b>User</b></TD><TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_total(proj_id=".$proj_id.")')\">Total Hours</a></TD>";
	  $ls_trupi_tabele = "";
	  $user_id_old     = "";
	  $java_user       = "";
	  $nr_rresht       = 0;
	  $nr_oret_total_total = 0;
          
	  $rs = WebApp::openRS("projects_user_oret_java");
	  $rs->MoveFirst();
	  while (!$rs->EOF())
	    {
	      $user_id          = $rs->Field("user_id");
	      $java_data        = $rs->Field("java_data");
	      $nr_oret_java     = $rs->Field("nr_oret_java");
	      $java_data_format = $rs->Field("java_data_format");

	      $nr_rresht = $nr_rresht + 1;
                 
	      IF ($nr_rresht == 1)
		{$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";
		$java_user = $user_id;}
	      ELSE
		{
		  IF ($java_user == $user_id)
		    {$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";}
		}
                 
	      WebApp::addVar("user_id",$user_id);
                 
	      IF ($user_id != $user_id_old)
		{
		  $j = 0;
		  //meren oret totale te userit ------------------------------------
		  $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
		  IF(!$rs_nr_oret_total->EOF())
		    {$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
		  //----------------------------------------------------------------
		  $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;

		  //meren emri dhe mbiemri userit ------------------------------------
		  $rs_user_name = WebApp::openRS("user_name");
		  IF(!$rs_user_name->EOF())
		    {$firstname = $rs_user_name->Field("firstname");
		    $lastname  = $rs_user_name->Field("lastname");}
		  //----------------------------------------------------------------
                     
		  IF ($nr_bg == 0)
		    {$bg_color = "e2e2e2";
		    $nr_bg = 1;}
		  ELSE
		    {$bg_color = "f2f2f2";
		    $nr_bg = 0;}

		  $ls_trupi_tabele = $ls_trupi_tabele."<TR bgcolor=".$bg_color."><input type=hidden name=\"user_id\" value=\"".$user_id."\">";
		  $ls_trupi_tabele = $ls_trupi_tabele."<TD width=110 NOWRAP><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_user(proj_id=".$proj_id.";user_id=".$user_id.")')\">".$firstname." ".$lastname."</a></TD>";
		  $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_total."</TD>";
                        
		  IF ($this->getSVar("java_data_edit") == $java_data_format)
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret\" value=\"".$nr_oret_java."\" size=5 maxlength=5></TD>";}
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";}
                     
		  $user_id_old = $user_id;
		  $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
		}
	      ELSE
		{
		  IF ($this->getSVar("java_data_edit") == $java_data_format)
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret\" value=\"".$nr_oret_java."\" size=5 maxlength=5></TD>";}
		  ELSE
		    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";}
		  $j++;
		  $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
		}

	      $rs->MoveNext();
	    }
                 
	  $ls_koka_tabele = $ls_koka_tabele."</TR>";
	  $ls_trupi_tabele = $ls_trupi_tabele."</TR>";
               
	  $ls_fundi_tabele = "<TR bgcolor=d2d2d2><TD align=center width=110><b>Total<b></TD><TD align=center><b>".$nr_oret_total_total."</b></TD>";
	  $j = 0;
	  while ($j < count($ls_fundi_tabele_aray)):
	    $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center width=30><b>".$ls_fundi_tabele_aray[$j]."</b></TD>";
	  $j++;
	  endwhile;
	  $ls_fundi_tabele = $ls_fundi_tabele."</TR>";
               
	  $ls_html   = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";
	  $link_ruaj = "<a href=\"javascript:update_oret_java();\">Save</a>";
        }
      ELSE
	{
	  //shikohet nese jane percaktuar oret totale te projektit ----
          $rs = WebApp::openRS("count_oret_total");
          IF(!$rs->EOF())
            {$count_oret_total = $rs->Field("count_oret_total");}
	  //-----------------------------------------------------------
     
          IF ($count_oret_total == 0)
	    {
              $link_ruaj = "";
              $ls_html = "<Table border=0 cellspacing=1 cellpadding=1>";
              $ls_html = $ls_html."<TR bgcolor=cccccc><TD align=center><b>User</b></TD><TD align=center><b>Total Hours</b></TD></TR>";
        
              $rs = WebApp::openRS("proj_user_name");
              $rs->MoveFirst();
              while (!$rs->EOF())
                {
		  $user_id   = $rs->Field("user_id");
		  $firstname = $rs->Field("firstname");
		  $lastname  = $rs->Field("lastname");

		  IF ($nr_bg == 0)
                    {$bg_color = "e2e2e2";
		    $nr_bg = 1;}
		  ELSE
                    {$bg_color = "f2f2f2";
		    $nr_bg = 0;}

		  $ls_html = $ls_html."<TR bgcolor=".$bg_color.">";
		  $ls_html = $ls_html."<TD width=110 NOWRAP>".$firstname." ".$lastname."</TD>";
		  $ls_html = $ls_html."<TD align=center><input type=hidden name=\"user_id\" value=\"".$user_id."\"><input type=text name=\"user_oret\" value=\"\" size=5 maxlength=5></TD>";
		  $ls_html = $ls_html."</TR>";
                 
		  $link_ruaj = "<a href=\"javascript:update_oret_total();\">Save</a>";
		  $rs->MoveNext();
		}
	      $ls_html = $ls_html."<TR bgcolor=d2d2d2><TD align=center><b>Total</b></TD><TD align=center><b>0</b></TD></TR></Table>";
	    }
          ELSE
            {
	      //shikohet nese jane percaktuar oret javore te projektit ----
	      $rs = WebApp::openRS("count_oret_java");
	      IF(!$rs->EOF())
		{$count_oret_java = $rs->Field("count_oret_java");}
	      //-----------------------------------------------------------
     
	      IF($count_oret_java == 0)
		{
		  //meret dita kur fillon projekti ----------------------------
		  WebApp::addVar("proj_date",$proj_date);
		  $rs = WebApp::openRS("proj_date_dita");
		  IF(!$rs->EOF())
		    {$proj_date_dita = $rs->Field("proj_date_dita");}

		  $proj_date_dita = "-".$proj_date_dita;
		  WebApp::addVar("proj_date_dita",$proj_date_dita);
             
		  $rs = WebApp::openRS("data_java_re");
		  IF(!$rs->EOF())
		    {$data_java_re = $rs->Field("data_java_re");}
             
		  //-----------------------------------------------------------


		  $ls_html = "<Table border=0 cellspacing=1 cellpadding=1>";
		  $ls_html = $ls_html."<TR bgcolor=cccccc><TD align=center><b>User</b></TD><TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_total(proj_id=".$proj_id.")')\">Total Hours</a></TD><TD align=center>dd.mm.yyyy<br><input type=text name=\"java_data\" size=11 maxlength=10 value=\"".$data_java_re."\"></TD></TR>";
		  $nr_oret_total_total = 0;
        
		  $rs = WebApp::openRS("proj_user_name");
		  $rs->MoveFirst();
		  while (!$rs->EOF())
		    {
		      $user_id   = $rs->Field("user_id");
		      $firstname = $rs->Field("firstname");
		      $lastname  = $rs->Field("lastname");

		      WebApp::addVar("user_id",$user_id);
                 
		      //shikohet nese jane percaktuar oret javore te projektit ----
		      $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
		      IF(!$rs_nr_oret_total->EOF())
			{$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
		      //-----------------------------------------------------------
		      $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;
                 
		      IF ($nr_bg == 0)
			{$bg_color = "e2e2e2";
			$nr_bg = 1;}
		      ELSE
			{$bg_color = "f2f2f2";
			$nr_bg = 0;}

		      $ls_html = $ls_html."<TR bgcolor=".$bg_color.">";
		      $ls_html = $ls_html."<TD width=110 NOWRAP>".$firstname." ".$lastname."</TD>";
		      $ls_html = $ls_html."<TD align=center><input type=hidden name=\"user_id\" value=\"".$user_id."\">".$nr_oret_total."</TD>";
		      $ls_html = $ls_html."<TD align=center><input type=text name=\"user_oret\" value=\"\" size=5 maxlength=5></TD>";
		      $ls_html = $ls_html."</TR>";
                 
		      $rs->MoveNext();
		    }
		  $ls_html = $ls_html."<TR bgcolor=d2d2d2><TD align=center>Total</TD><TD align=center>".$nr_oret_total_total."</TD><TD align=center>0</TD></TR></Table>";
		  $link_ruaj = "<a href=\"javascript:update_oret_java();\">Save</a>";
                }
	      ELSE
                {
		  //meret java e re -------------------------------------------
		  $rs = WebApp::openRS("data_java_oret_re");
		  IF(!$rs->EOF())
		    {$data_java_re = $rs->Field("data_java_oret_re");}
		  //-----------------------------------------------------------
             
		  $ls_koka_tabele  = "<Table border=0 cellspacing=1 cellpadding=1><TR bgcolor=cccccc><TD align=center><b>User</b></TD><TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_total(proj_id=".$proj_id.")')\">Total Hours</a></TD>";
		  $ls_trupi_tabele = "";
		  $user_id_old     = "";
		  $java_user       = "";
		  $nr_rresht       = 0;
		  $nr_oret_total_total = 0;
          
		  $rs = WebApp::openRS("projects_user_oret_java");
		  $rs->MoveFirst();
		  while (!$rs->EOF())
		    {
		      $user_id          = $rs->Field("user_id");
		      $java_data        = $rs->Field("java_data");
		      $nr_oret_java     = $rs->Field("nr_oret_java");
		      $java_data_format = $rs->Field("java_data_format");

		      $nr_rresht = $nr_rresht + 1;
                 
		      IF ($nr_rresht == 1)
			{$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";
			$java_user = $user_id;}
		      ELSE
			{
			  IF ($java_user == $user_id)
			    {$ls_koka_tabele = $ls_koka_tabele."<TD align=center><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_java(proj_id=".$proj_id.";java_data=".$java_data_format.")')\">".$java_data."</a></TD>";}
			}
                 
		      WebApp::addVar("user_id",$user_id);
                 
		      IF ($user_id != $user_id_old)
			{
			  $j = 0;
			  //meren oret totale te userit ------------------------------------
			  $rs_nr_oret_total = WebApp::openRS("nr_oret_total");
			  IF(!$rs_nr_oret_total->EOF())
			    {$nr_oret_total = $rs_nr_oret_total->Field("nr_oret_total");}
			  //----------------------------------------------------------------
			  $nr_oret_total_total = $nr_oret_total_total + $nr_oret_total;

			  //meren emri dhe mbiemri userit ------------------------------------
			  $rs_user_name = WebApp::openRS("user_name");
			  IF(!$rs_user_name->EOF())
			    {$firstname = $rs_user_name->Field("firstname");
			    $lastname  = $rs_user_name->Field("lastname");}
			  //----------------------------------------------------------------
                       
			  IF ($nr_bg == 0)
			    {$bg_color = "e2e2e2";
			    $nr_bg = 1;}
			  ELSE
			    {$bg_color = "f2f2f2";
			    $nr_bg = 0;}
                           
                           
			  IF($nr_rresht > 1)
			    {$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret\" value=\"\" size=5 maxlength=5></TD></TR>";}
                     
			  $ls_trupi_tabele = $ls_trupi_tabele."<TR bgcolor=".$bg_color."><input type=hidden name=\"user_id\" value=\"".$user_id."\">";
			  $ls_trupi_tabele = $ls_trupi_tabele."<TD width=110 NOWRAP><a href=\"JavaScript:GoTo('thisPage?event=proj_schedule.edit_oret_user(proj_id=".$proj_id.";user_id=".$user_id.")')\">".$firstname." ".$lastname."</a></TD>";
			  $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_total."</TD>";
			  $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";
                     
			  $user_id_old = $user_id;
			  $ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
			}
		      ELSE
			{$ls_trupi_tabele = $ls_trupi_tabele."<TD align=center>".$nr_oret_java."</TD>";
			$j++;
			$ls_fundi_tabele_aray[$j] = $ls_fundi_tabele_aray[$j] + $nr_oret_java;
			}

		      $rs->MoveNext();
		    }
                 
		  $ls_koka_tabele = $ls_koka_tabele."<TD>dd.mm.yyyy<br><input type=text name=\"java_data\" size=11 maxlength=10 value=\"".$data_java_re."\"></TD></TR>";
		  $ls_trupi_tabele = $ls_trupi_tabele."<TD align=center><input type=text name=\"user_oret\" value=\"\" size=5 maxlength=5></TD></TR>";
               
		  $ls_fundi_tabele = "<TR bgcolor=d2d2d2><TD align=center width=110><b>Total<b></TD><TD align=center><b>".$nr_oret_total_total."</b></TD>";
		  $j = 0;
		  while ($j < count($ls_fundi_tabele_aray)):
		    $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center width=30><b>".$ls_fundi_tabele_aray[$j]."</b></TD>";
		  $j++;
		  endwhile;
		  $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center><b>0</b></TD></TR>";
               
		  $ls_html   = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";

		  $link_ruaj = "<a href=\"javascript:update_oret_java();\">Save</a>";
		}
	    }
	}
     
      WebApp::addVar("project_id_input",$project_id_input);
      WebApp::addVar("ls_html",$ls_html);
      WebApp::addVar("link_ruaj",$link_ruaj);
      WebApp::addVar("mesazh_text",$mesazh_text);

      $session->rmVar("mesazh");
      $this->setSVar("ngjarja", '');
      //$session->rmVar("ngjarja");
      //$session->rmVar("java_data_edit");
      //$session->rmVar("user_id_oret");
    }
}
?>
