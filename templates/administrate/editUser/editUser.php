<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

class editUser extends formWebObj
{
  var $newUser_data =  array( 
                             "office"           => "",
                             "department"       => "",
                             "username"         => "",
                             "firstname"        => "",
                             "lastname"         => "",
                             "roles"            => "",
                             "tel1"             => "",
                             "tel2"             => "",
                             "e_mail"           => "",
                             "address"          => ""
                             );

  function init()
    {
      $this->addSVar("mode", "addNew");
      $this->addSVar("user", UNDEFINED);
    }

  function on_add($event_args)
    //add the new user in database
    {
      //check that such a username does not exist in DB
      $username = $event_args["username"];
      $rs = WebApp::openRS("getUserID", array("username"=>$username));
      if (!$rs->EOF())
        {
          $msg = "The username '$username' already exists.\n"
            . "Please choose another one.";
          WebApp::message($msg);
          $this->newUser_data = $event_args;
          $this->newUser_data["username"] = "";
          return;
        }

      //add the user
      WebApp::execDBCmd("addUser", $event_args);

      //get the user_id of the new user
      $rs = WebApp::openRS("getUserID", array("username"=>$username));
      $user_id = $rs->Field("user_id");

      //save the access rights as well
      $accr_changes = $event_args["accRightChanges"];
      $this->updateAccessRights($user_id, $accr_changes);

      //acknowledgment message
      WebApp::message("User added, add another one.");
    }

  function on_delete($event_args)
    //delete the user and his access rights
    {
      if (!$this->has_admin_right_on_user())
        {
          $msg = "You have no admin rights on this user.\n"
            ."The user is not deleted.";
          WebApp::message($msg);
          return;
        }

      WebApp::execDBCmd("deleteUser");
      WebApp::execDBCmd("delAccRights");

      //the currentUser is deleted,
      //set current the first user in the list
      $listOfUsers = WebApp::getObject("listOfUsers");
      $listOfUsers->selectFirst();

      //acknowledgment message
      WebApp::message("User deleted.");
    }

  function on_update($event_args)
    //save the changes
    {
      if (!$this->has_admin_right_on_user())
        {
          $msg = "You have no admin rights on this user.";
          WebApp::message($msg);
          return;         
        }

      //update
      WebApp::execDBCmd("updateUser", $event_args);

      //update the password if the admin has set a value to it
      $passwd = $event_args["password"];
      if ($passwd<>"") WebApp::execDBCmd("updatePassword", $event_args);

      //update the access rights as well
      $user_id = WebApp::getSVar("listOfUsers->currentUser");
      $accr_changes = $event_args["accRightChanges"];
      $this->updateAccessRights($user_id, $accr_changes);
    }

  function has_admin_right_on_user()
    //return true if the logged user has admin rights
    //on the selected user
    {
      $user = WebApp::getSVar("username");
      if ($user=="superuser")  return true;

      //check that the user belongs to the
      //admin domain of the logged user
      $rs = WebApp::openRS("currentUser");
      $dept_id = $rs->Field("department");
      $adm_domain = WebApp::getSVar("adminDomain");
      $arr_adm_domain = explode(",", $adm_domain);
      if (in_array($dept_id, $arr_adm_domain))
        {
          return true;
        }
      else
        {
          return false;
        }
    }

  function onParse()
    {
      //get the current user from the list of users
      $user = WebApp::getSVar("listOfUsers->currentUser");

      $this->setSVar("user", $user);

      if ($user==UNDEFINED)
        {
          $this->setSVar("mode","addNew");
        }
      else
        {
          $this->setSVar("mode", "edit");
        }

      //divide the recordset "roles" into "roles_1" and "roles_2"
      global $webPage;
      $rs = $webPage->getRecordset("roles");
      $rs->Open();
      $count = $rs->count;
      $count = ceil($count/2);
      $rs_1 = $rs->slice(0,$count);
      $rs_2 = $rs->slice($count);
      $rs_1->ID = "roles_1";
      $rs_2->ID = "roles_2";
      $webPage->addRecordset($rs_1);
      $webPage->addRecordset($rs_2);
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="addNew")
        {
          $user_data = $this->newUser_data; 
          $user_data["office"] = WebApp::getSVar("u_office");
          $user_data["department"] = WebApp::getSVar("u_dept");
        }
      else
        {
          $rs = WebApp::openRS("currentUser");
          $user_data = $rs->Fields();
        }
      $user_data["admin"] = WebApp::getSVar("u_id");
      WebApp::addVars($user_data);      
    }

  function updateAccessRights($user, $accr_changes)
    //$accr_changes has the format: "remove(1-1-2,1-3-1) add(2-2-2,1-1-3)"
    {
      //get the remove and add parts of the changes
      $pattern = "remove\(([^)]*)\) *add\(([^)]*)\)";
      ereg($pattern, $accr_changes, $regs);
      $remove = $regs[1];
      $add = $regs[2];

      //remove from DB the access rights in $remove
      if ($remove<>"")
        {
          $arr_remove = explode(",", $remove);
          while (list($i, $item) = each($arr_remove))
            {
              list($office,$dept,$accr) = explode("-", $item);
              $arr_condition[] = "(off_id=$office AND user_id=$user AND dept_id=$dept AND accr_id=$accr)";
            }
          $rm_condition = implode(" OR ", $arr_condition);
          WebApp::addVar("rm_condition", $rm_condition);
          WebApp::execDBCmd("rmAccRights");
        }

      //add to DB the access rights in $add
      if ($add<>"")
        {
          $arr_add = explode(",", $add);
          while (list($i, $item) = each($arr_add))
            {
              list($office,$dept,$accr) = explode("-", $item);
              $arr_values[] = "($user,$office,$dept,$accr)";
            }
          $ins_values = implode(",", $arr_values);
          WebApp::addVar("ins_values", $ins_values);
          WebApp::execDBCmd("addAccRights");
        }
    }
}
?>