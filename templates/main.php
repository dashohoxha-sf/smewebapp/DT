<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class main extends WebObject
{
  function init()
    {
      $username = WebApp::getSVar("username");
      if ($username<>"superuser")
        {
          WebApp::addSVar("viewDomain", "-1", "DB");
          WebApp::addSVar("editDomain", "-1", "DB");
          WebApp::addSVar("financDomain", "-1", "DB");
          WebApp::addSVar("modifyDomain", "-1", "DB");
          WebApp::addSVar("scheduleDomain", "-1", "DB");
          WebApp::addSVar("adminDomain", "-1", "DB");
        }
    }

  function onParse()
    {
      $module = WebApp::getSVar("module");
      switch ($module)
        {
        case "monitor":
          $module_file = "monitor/monitor.html";
          break;
        case "financ":
        case "modify":
          $module_file = "modify/modify.html";
          break;
        case "schedule":
          $module_file = "schedule/schedule.html";
          break;
        case "administrate":
          $module_file = "administrate/administrate.html";
          break;
        case "superuser":
          $module_file = "su/superuser.html";
          break;
        default:
        case "userdata":
          $module_file = "userdata/userdata.html";
          break;
        case "test":
          $module_file = "test/test.html";
          break;
        }
      WebApp::addVar("module_file", $module_file);    

      $this->setAccrDomains();
    }

  /**
   * Sets viewDomain, editDomain, financDomain, scheduleDomain
   * and adminDomain, which are comma separated lists of dept_id-s.
   */
  function setAccrDomains()
    {
      $username = WebApp::getSVar("username");
      if ($username=="superuser") return;

      $rs = WebApp::openRS("getUserAccRights");
      $viewDomain = $this->get_accr_domain($rs, ACCR_VIEW);
      $editDomain = $this->get_accr_domain($rs, ACCR_EDIT);
      $financDomain = $this->get_accr_domain($rs, ACCR_FINANC);
      $scheduleDomain = $this->get_accr_domain($rs, ACCR_SCHEDULE);
      $adminDomain = $this->get_accr_domain($rs, ACCR_ADMIN);
      $modifyDomain = $editDomain.",".$financDomain;

      WebApp::setSVar("viewDomain", $viewDomain);
      WebApp::setSVar("editDomain", $editDomain);
      WebApp::setSVar("financDomain", $financDomain);
      WebApp::setSVar("modifyDomain", $modifyDomain);
      WebApp::setSVar("scheduleDomain", $scheduleDomain);
      WebApp::setSVar("adminDomain", $adminDomain);
    }

  function get_accr_domain(&$rs, $accr_id)
    {
      $arr_depts = array();
      $rs_domain = $rs->filter("accr_id='$accr_id'");
      while (!$rs_domain->EOF())
        {
          $arr_depts[] = $rs_domain->Field("dept_id");
          $rs_domain->MoveNext();
        }
      $accr_domain =  implode(",", $arr_depts);
      if ($accr_domain=="")  $accr_domain="-1";
      return $accr_domain;
    }
}
?>