// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function date_not_in_future(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = arr_date[0];
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date > curr_date)
    {
      alert("Warning: The selected date is later than the current date.");
      //set_current_date(txtbox);
    }
}

function date_not_in_past(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = (arr_date[0]*1) + 1;
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date < curr_date)
    {
      alert("Warning: The selected date is earlier than the current date.");
      //set_current_date(txtbox);
    }
}

function set_current_date(txtbox)
{
  var curr_date = get_curr_date();
  var day = curr_date.getDate();
  var month = curr_date.getMonth() + 1;
  var year = curr_date.getFullYear();

  if (day < 10)    day   = "0" + day;
  if (month < 10)  month = "0" + month;
  str_date = day + '/' + month + '/' + year;
  txtbox.value = str_date;
}

function save()
{
  var form = document.editProject;
  var event_args = getEventArgs(form);

  var mode = session.getVar("editProject->mode");
  if (mode=="add")  saveFormData(form);
  SendEvent("editProject", "save", event_args);
}

function close_doc()
{
  var confirm_msg;
  confirm_msg = "This project will be closed definitly!\n"
    + "You will not be able to change it anymore.\n\n\n"
    + "Are you sure that you want to close this project?\n\n";
  if (confirm(confirm_msg))
    {
      GoTo("thisPage?event=editProject.close");
    }
}

function changeStatus(radioButt)
{
  var new_status = radioButt.value;
  var form = document.editProject;
  var old_status = session.getVar("editProject->status_id");
  var old_option;

  //get the option that was checked previously
  for (i=0; form.status[i]; i++)
    {
      if (form.status[i].value==old_status)
        {
          old_option = form.status[i];
          break;
        }
    }

  //this button is already checked; do nothing
  if (radioButt.defaultChecked)   return;

  //if the mode is "add" no need for checking 
  //and confirmation, just change the status and return
  var mode = session.getVar("editProject->mode");
  if (mode=="add")
    {
      //transmit the data of the form, so that 
      //the changed values are not lost
      saveFormData(form);

      //change the status
      var event = "editProject.changeStatus(status_id=" + new_status + ")";
      GoTo("thisPage?event=" + event);
      return;
    }

  //the following code is for mode "edit"

  //if the document is closed, the status cannot be changed
  var closed = session.getVar("editProject->closed");
  if (closed=="true")
    {
      alert("You cannot change the status of a closed document!");
      old_option.checked = true;  //check again the old option
      return;
    }
  //check that the user is not trying to change 
  //to an invalid status
  if (new_status > old_status)
    {
      alert("You cannot move back to this status!");
      old_option.checked = true;  //check again the old option
      return;
    }

  //confirm the change of the status
  var confirm_msg;
  confirm_msg = "When you change the status this document\n"
    + "is closed and a new document is created.\n"
    + "You will not be able to edit this document again.\n\n\n"
    + "Are you sure that you want to change the status?\n\n";
  if (!confirm(confirm_msg))
    {
      old_option.checked = true;  //check again the old option
      return;
    }
 
  //transmit the data of the form, so that 
  //the changed values are not lost
  saveFormData(form);

  //change the status
  var event = "editProject.changeStatus(status_id=" + new_status + ")";
  GoTo("thisPage?event=" + event);
}

function editable()
     //returns false if the document is closed
{
  var closed = session.getVar("editProject->closed");

  if (closed=="true")
    {
      alert("This document is closed and cannot be edited.");
      return false;
    }

  return true;
}
