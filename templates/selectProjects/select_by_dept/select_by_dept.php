<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class select_by_dept extends WebObject
{
  function init()
    {
      $this->addSVar("values", "");
      $this->addSVar("names", "");
      $this->addSVar("accessDomain", "-1");

      $this->select_user_dept();
    }

  /** selects the user office and department */
  function select_user_dept()
    {
      //set 'values'
      $u_office = WebApp::getSVar("u_office");
      $u_dept = WebApp::getSVar("u_dept");
      $this->setSVar("values", $u_office."_".$u_dept);

      //set 'names'
      $rs = WebApp::openRS("get_user_officename");
      $office_name = $rs->Field("name");
      $rs = WebApp::openRS("get_user_deptname");
      $dept_name = $rs->Field("name");
      $this->setSVar("names", $office_name."/".$dept_name);
    }

  function onParse()
    {
      $module = WebApp::getSVar("module");
      switch ($module)
        {
        case "monitor":
          $accessDomain = WebApp::getSVar("viewDomain");
          break;
        case "modify":
          $submodule = WebApp::getSVar("modify->submodule");
          if ($submodule=="financ")
            {
              $accessDomain = WebApp::getSVar("financDomain");
            }
          else
            {
              $accessDomain = WebApp::getSVar("modifyDomain");
              WebApp::setSVar("projects->recount", "true");
            }
          break;
        case "financ":
          $accessDomain = WebApp::getSVar("financDomain");
          break;
        case "schedule":
          $accessDomain = WebApp::getSVar("scheduleDomain");
          break;
        default:
          $accessDomain = "-1";
        }

      $last_accessDomain = $this->getSVar("accessDomain");
      if ($accessDomain<>$last_accessDomain)
        {
          $this->setSVar("accessDomain", $accessDomain);
          $this->moderate_selection();
        }
    }

  function onRender()
    {
      //check the selected values
      $values = $this->getSVar("values");
      $arr_selected = explode(";", $values);
      for ($i=0; $i < sizeof($arr_selected); $i++)
        {
          $id = $arr_selected[$i];
          WebApp::addVar("notChecked_$id", "checked");
        }
    }

  /**
   * This function makes the intersection between the selected
   * departments and the accessDomain; it modifies the selected 
   * departments so that they are inside the accessDomain
   */
  function moderate_selection()
    {
      $accessDomain = $this->getSVar("accessDomain");

      $names = $this->getSVar("names");
      $values = $this->getSVar("values");
      
      $arrAccDomain = array();
      $arrNames = array();
      $arrValues = array();
      $arrNewNames = array();
      $arrNewValues = array();

      $arrAccDomain = explode(",", $accessDomain);
      $arrNames = explode(" ", $names);
      $arrValues = explode(";", $values);
      
      for ($i=0; $i < sizeof($arrValues); $i++)
        {
          $val = $arrValues[$i];
          list($off_id,$dept_id) = explode("_", $val);
          if (in_array($dept_id, $arrAccDomain))
            {
              $arrNewValues[] = $val;
              $arrNewNames[] = $arrNames[$i];
            }
        }
        
      $this->setSVar("names", implode(" ", $arrNewNames));
      $this->setSVar("values", implode(";", $arrNewValues));
    }

  /** 
   * Make sure that there is one and only one department selected.
   * It is called by needToSellRpt and enterNSR webboxes.
   */
  function ensure_single_selection()
    {
      $values = WebApp::getSVar("select_by_dept->values");
      if ($values=="")
        {
          //no dept selected, select the department of the user
          $this->select_user_dept();
          return;
        }
      $arr_values = explode(";", $values);
      if (sizeof($arr_values) > 1) 
        {
          //there are multiple depts selected
          //keep only the first one
          WebApp::setSVar("select_by_dept->values", $arr_values[0]);
          $arr_names = explode(" ", WebApp::getSVar("select_by_dept->names"));
          WebApp::setSVar("select_by_dept->names", $arr_names[0]);
        }
    }
}
?>
