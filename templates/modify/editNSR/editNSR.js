// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function save()
{
  var form = document.editNSR;
  var prev_month = form.prev_month.value;
  var prev_id = form.prev_nsr_id.value;
  var curr_month = form.curr_month.value;
  var curr_id = form.curr_nsr_id.value;
  var args = Array();

  args.push("prev_month=" + prev_month);
  args.push("prev_id=" + prev_id);
  args.push("curr_month=" + curr_month);
  args.push("curr_id=" + curr_id);

  GoTo("thisPage?event=editNSR.save(" + args.join(';') + ")");
}

function save_nsrBudget(nsr_id)
{
  var form = document.nsr_budget;
  var nsr = eval("form.nsr_" + nsr_id + ".value");
  var event_args = "nsr_id=" + nsr_id + ";nsr=" + nsr;

  GoTo("thisPage?event=editNSR.saveNsrB(" + event_args + ")");
}
