<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class selectUsers extends WebObject
{
  function init()
    {
      $off_id = $this->get_first_office();
      $this->addSVars( array(
                             "username"    => "",
                             "name"        => "",
                             "office"      => "$off_id",
                             "department"  => "",
                             "accessright" => "",
                             "select_condition" => "1=1"
                             ) );
      $this->buildQueryCondition();
    }

  function get_first_office()
    {
      $username = WebApp::getSVar("username");
      if ($username=="superuser")
        $rs_id =  "xlistbox_su_offices";
      else $rs_id = "xlistbox_user_offices";
      $rs = WebApp::openRS($rs_id);
      $off_id = $rs->Field("id");
      return $off_id;
    }

  function buildQueryCondition()
    {
      $username     = $this->getSVar("username");
      $name         = $this->getSVar("name");
      $office       = $this->getSVar("office");
      $department   = $this->getSVar("department");
      $accessright  = $this->getSVar("accessright");

      if ($username=="")
        {  $cond1 = "";  }
      else
        {  $cond1 = '(username LIKE "%'.$username.'%")'; }

      if ($name=="")
        {  $cond2 = "";  }
      else
        {
          $cond2 = '( (firstname LIKE "%'.$name.'%") '
            . 'OR (lastname LIKE "%'.$name.'%") )'; 
        }

      if ($office=="")
        {  $cond3 = "";  }
      else
        {  $cond3 = '(office = "'.$office.'")'; }

      if ($department=="")
        {  $cond4 = "";  }
      else
        {  $cond4 = '(department = "'.$department.'")'; }

      if ($accessright=="")
        {  $cond5 = "";  }
      else
        {  $cond5 = '(accr_id = "'.$accessright.'")'; }

      if ($cond1<>"")  $arr_cond[] = $cond1;            
      if ($cond2<>"")  $arr_cond[] = $cond2;            
      if ($cond3<>"")  $arr_cond[] = $cond3;            
      if ($cond4<>"")  $arr_cond[] = $cond4;            
      if ($cond5<>"")  $arr_cond[] = $cond5;

      $condition = "";  //keeps the query condition
      if (sizeof($arr_cond) > 0)
        {
          $condition = implode(" AND ", $arr_cond);
        }
      if ($condition=="")   { $condition = "1=1"; }

      $this->setSVar("select_condition", $condition);
    }  
}
?>