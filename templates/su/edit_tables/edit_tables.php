<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class edit_tables extends WebObject
{
  function init()
    {
      //'edit_one_table' or 'edit_two_tables'
      $this->addSVar("edit_table_file", "edit_one_table.html");
      $this->addSVar("table", "countries");
    }

  function on_edit1($event_args)
    {
      $table = $event_args["table"];
      $this->setSVar("edit_table_file", "edit_one_table.html");
      $this->setSVar("table", $table);
    }

  function on_edit2($event_args)
    {
      $table1 = $event_args["table1"];
      $table2 = $event_args["table2"];
      $this->setSVar("edit_table_file", "edit_two_tables.html");
      $this->setSVar("table1", $table1);
      $this->setSVar("table2", $table2);
    }

  function add_table2_filter()
    {
      $file = $this->getSVar("edit_table_file");
      if ($file=="edit_one_table.html")  return;

      $table1 = $this->getSVar("table1");
      switch ($table1)
        {
        case "offices":
          $table2_filter = "off_id='{{off_id}}'";
          break;
        case "industry_sectors":
          $table2_filter = "sector_id='{{sector_id}}'";
          break;
        default:
          $msg = "edit_tables::add_table2_filter(): "
            ."table1='$table1' is unhandled.";
          print WebApp::warning_msg($msg);
          break;
        }
      WebApp::addVar("filter", $table2_filter);
    }

  function onRender()
    {
      $this->add_table_list_rs();
      $this->add_table2_filter();
    }

  function add_table_list_rs()
    {
      $rs = new EditableRS("table_list");

      $rec = array("label"=>"countries", "action"=>"edit_table('countries')");
      $rs->addRec($rec);
      $rec = array("label"=>"access", "action"=>"edit_table('access')");
      $rs->addRec($rec);
      $rec = array("label"=>"roles/titles", "action"=>"edit_table('roles')");
      $rs->addRec($rec);
      $rec = array("label"=>"status", "action"=>"edit_table('status')");
      $rs->addRec($rec);
      $rec = array("label"=>"offices/departments", 
                   "action"=>"edit_tables('offices','departments')");
      $rs->addRec($rec);
      $rec = array("label"=>"industry codes/subcodes", 
                   "action"=>"edit_tables('industry_sectors','industry_subsectors')");
      $rs->addRec($rec);

      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>