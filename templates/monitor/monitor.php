<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class monitor extends WebObject
{
  function init()
    {
      $this->addSVar("reportFile", "listOfProjRpt/listOfProj.html");
      $this->addSVar("selectFile", "listOfProjRpt/select.html");
    }

  function on_showRpt($event_args)
    {
      $report = $event_args["rpt"];
      switch ($report)
        {
        case "listOfProj":
          $this->setSVar("reportFile", "listOfProjRpt/listOfProj.html");
          $this->setSVar("selectFile", "listOfProjRpt/select.html");
          break;
        case "summary":
          $this->setSVar("reportFile", "summaryRpt/summaryReport.html");
          $this->setSVar("selectFile", "summaryRpt/select.html");
          break;
        case "needToSell":
          $this->setSVar("reportFile", "needToSellRpt/needToSell.html");
          $this->setSVar("selectFile", "needToSellRpt/select.html");
          break;
        }
    }
}
?>