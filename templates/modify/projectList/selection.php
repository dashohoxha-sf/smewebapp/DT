<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class selection extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "selectedTimeframe" => "",
                             "selectedDepts" => "",
                             "selectedStatus" => "" 
                             ));
    }

  function onParse()
    {
      $currMonthCheck = WebApp::getSVar("select_by_time->currMonthCheck");
      if ($currMonthCheck=="checked")
        {
          $currMonth = get_curr_date("M");
          $currYear = get_curr_date("Y");
          $timeFrame =" $currMonth $currYear";
        }
      else
        {
          $fromMonth = WebApp::getSVar("select_by_time->fromMonth");
          $toMonth   = WebApp::getSVar("select_by_time->toMonth");
          $fromYear  = WebApp::getSVar("select_by_time->fromYear");
          $toYear    = WebApp::getSVar("select_by_time->toYear");
          if ($fromMonth==$toMonth and $fromYear==$toYear)
            {
              $timeFrame = "$toMonth $toYear";
            }
          else
            {
              $timeFrame = "From $fromMonth $fromYear to $toMonth $toYear";
            }
        }
      $this->setSVar("selectedTimeframe", $timeFrame);

      $status = WebApp::getSVar("select_by_status->names");
      if ($status=="")  $status="(all)";
      $this->setSVar("selectedStatus", $status);

      $depts = WebApp::getSVar("select_by_dept->names");
      if ($depts=="")  $depts="(all)";
      $this->setSVar("selectedDepts", $depts);
    }
}
?>