<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class projectList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("projects->current_page", $page);
    }

  function on_save($event_args)
    {
      $rs = WebApp::openRS("is_closed", $event_args);
      $closed = $rs->Field("closed");
      if ($closed=="true")
        {
          $proj_name = $rs->Field("name");
          WebApp::message("The modifications to: '$proj_name'\n"
                          ."cannot be saved because it is closed.\n");
        }
      else
        {
          WebApp::execDBCmd("save", $event_args);
        }
    }

  function on_rm($event_args)
    //remove all the data of the project from DB
    {
      if (!$this->has_remove_right($event_args))
        {
          WebApp::message("You have not enough rights to remove\n"
                          ."this project! Ask the administrator\n"
                          ."to help you with removing it.");
          return;
        }

      WebApp::execDBCmd("rm_project", $event_args);
      WebApp::execDBCmd("rm_deptDistrib", $event_args);
      WebApp::execDBCmd("rm_timeDistrib", $event_args);
      WebApp::execDBCmd("rm_managers", $event_args);
      WebApp::execDBCmd("rm_partners", $event_args);
      WebApp::execDBCmd("rm_team", $event_args);
      switch ($event_args["status_id"])
        {
        case CONTRACTED:
          WebApp::execDBCmd("rm_contracted", $event_args);
          break;
        case PROPOSAL:
          WebApp::execDBCmd("rm_proposal", $event_args);
          WebApp::execDBCmd("rm_bids", $event_args);
          break;
        case QUALIFIED:
        case OPPORTUNITY:
          WebApp::execDBCmd("rm_opportunity", $event_args);
          break;
        }
      //recount the number of projects
      WebApp::setSVar("projects->recount", "true");
    }

  function has_remove_right($proj_key)
    //returns true if the logged user 
    //has admin rights on the given project
    {
      $rs = WebApp::openRS("get_project_dept_id", $proj_key);
      $dept_id = $rs->Field("dept_id");

      $param = array("dept_id" => $dept_id);
      $rs = WebApp::openRS("get_admin_right", $param);

      return ($rs->EOF() ? false : true); 
    }

  function on_rmAll($event_args)
    {
      //get the reason
      $reason = $event_args["reason"];

      //get the selection
      $time_frame = WebApp::getSVar("selection->selectedTimeframe");
      $depts = WebApp::getSVar("selection->selectedDepts");
      $status = WebApp::getSVar("selection->selectedStatus");

      //get the project list
      $arr_proj_id = array();
      $rs = WebApp::openRS("proj_list");
      while (!$rs->EOF())
        {
          $proj_id = $rs->Field("proj_id");
          $status_id = $rs->Field("status_id");
          $arr_proj_id[] = $proj_id."_".$status_id;
          $rs->MoveNext();
        }
      $proj_list = implode(",", $arr_proj_id);

      //save the request
      $params["request_date"] = date("Y-m-d");
      $params["user_id"] = WebApp::getSVar("u_id");
      $params["reason"] = $reason;
      $params["selection"] = $time_frame.";".$status.";".$depts;
      $params["proj_list"] = $proj_list;
      WebApp::execDBCmd("new_delete_request", $params);

      //notify the user with a message
      $msg = "Your delete request and your reason:\n"
        ."'$reason'\n"
        ."have been submitted to the superuser.\n"
        ."These projects will be deleted immediately\n"
        ."after the superuser approves the request."
        ."\n\nNote: not implemented yet."; 
      WebApp::message($msg);
    }
}
?>