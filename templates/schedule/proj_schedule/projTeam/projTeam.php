<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class projTeam extends WebObject
{
  function on_add($event_args)
    {
      WebApp::execDBCmd("addMember", $event_args);

      //per shtimin e nje useri --------------------------------------------------------------

      global $session,$event;
      extract($event->args);
      //kontrollohet nese ka rekorde per kete projekt te tabela projects_user_oret_total -----
      WebApp::addVar("proj_id", $proj_id);
      $rs = WebApp::openRS("count_oret_total");
      IF(!$rs->EOF())
	{$count_oret_total = $rs->Field("count_oret_total");}
  
      IF ($count_oret_total != 0)
	{
          WebApp::addVar("user_id", $user_id);
          WebApp::execDBCmd("insert_oret_total");

          //kontrollohet nese ka rekorde per kete projekt te tabela projects_users_oret_java
          $rs = WebApp::openRS("count_oret_java");
          IF(!$rs->EOF())
            {$count_oret_java = $rs->Field("count_oret_java");}
     
          IF ($count_oret_java != 0) //futen rekordet per cdo jave me vleren 0
	    {
              $rs = WebApp::openRS("proj_java_data");
              $rs->MoveFirst();
              while (!$rs->EOF())
		{
		  $java_data  = $rs->Field("java_data");
		  WebApp::addVar("java_data", $java_data);
		  WebApp::execDBCmd("insert_oret_java");
                 
		  $rs->MoveNext();
		}
	    }
	}
      //-----------------------------------------------------------------------------------


    }

  function on_rm($event_args)
    {
      WebApp::execDBCmd("rmMember", $event_args);

      //per fshirjen e nje useri ----------------------------------------------------------
      global $session,$event;
      extract($event->args);
      WebApp::addVar("proj_id", $proj_id);
      WebApp::addVar("user_id", $user_id);

      WebApp::execDBCmd("delete_user_oret_total");
      WebApp::execDBCmd("delete_user_oret_java");
      //---------------------------------------------------------------------------------

    }

  function on_rmAll($event_args)
    {
      WebApp::execDBCmd("rmAllMembers");

      //per fshirjen e te gjthe userave ----------------------------------------------------------
      WebApp::addVar("proj_id", $proj_id);

      WebApp::execDBCmd("delete_oret_total");
      WebApp::execDBCmd("delete_oret_java");
      //---------------------------------------------------------------------------------
    }
}
?>