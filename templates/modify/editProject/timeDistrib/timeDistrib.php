<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class timeDistrib extends WebObject
{
  function onRender()
    {
      global $webPage;

      //add the variable {{fees}}
      $rs = WebApp::openRS("getFees");
      $fees = ($rs->EOF() ? "0" : $rs->Field("fees"));
      WebApp::addVar("fees", $fees);

      //fill the recordset "monList"
      $rs = new EditableRS;
      $rs->Open();
      for ($i=1; $i<=12; $i++)
        {
          $mon = int2mon($i);
          $rec = array("m_id"=>$i, "Mon"=>$mon);
          $rs->addRec($rec);
        }
      $rs->ID = "monList";
      $webPage->addRecordset($rs);

      $rs1 = $webPage->getRecordset("projectTimeDistrib");
      $rs1->Open();

      //add as default values the last values
      $rs1->MoveLast();
      $month = $rs1->Field("month");
      $year = $rs1->Field("year");
      if ($month==UNDEFINED)  $month = get_curr_date("n");
      if ($year==UNDEFINED)  $year = get_curr_date("Y");
      WebApp::addVars(array("last_month"=>$month, "last_year"=>$year));

      //change the month from int (12) to string (Dec)
      $rs1->apply("timeDistrib_strMonth");
      $webPage->addRecordset($rs1);   
    }

  function on_add($event_args)
    {
      WebApp::execDBCmd("addDistrib", $event_args);
      $this->update_fees();
    }

  function on_rm($event_args)
    {
      WebApp::execDBCmd("rmDistrib", $event_args);
      $this->update_fees();
    }

  function on_rmAll($event_args)
    {
      WebApp::execDBCmd("rmAllDistribs");
      $this->update_fees();
    }

  function update_fees()
    {
      $rs = WebApp::openRS("projectTimeDistrib");
      while (!$rs->EOF())
        {
          $fees += $rs->Field("amount");
          $rs->MoveNext();
        }
      WebApp::execDBCmd("setFees", array("fees"=>$fees));
    }
}

function timeDistrib_strMonth(&$rec)
{
  $rec["month"] = int2mon($rec["month"]);
}
?>