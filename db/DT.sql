-- MySQL dump 9.09
--
-- Host: localhost    Database: inima_DT
-- ------------------------------------------------------
-- Server version	4.0.16-standard

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS access;
CREATE TABLE `access` (
  `accr_id` int(11) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`accr_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `access`
--

INSERT INTO access (accr_id, name) VALUES (1,'View'),(2,'Edit'),(3,'Financ'),(4,'Schedule'),(5,'Admin');

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS bids;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL auto_increment,
  `proj_id` int(11) NOT NULL default '0',
  `bidder` varchar(50) NOT NULL default 'xx',
  `bid_amount` int(11) NOT NULL default '0',
  `tech_points` int(11) NOT NULL default '0',
  `fin_points` int(11) NOT NULL default '0',
  PRIMARY KEY  (`bid_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `bids`
--

INSERT INTO bids (bid_id, proj_id, bidder, bid_amount, tech_points, fin_points) VALUES (5,52,'dsd',10,5,2),(6,52,'fdfgdg ggfgfg gff',13,2,5);

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS countries;
CREATE TABLE `countries` (
  `country_id` varchar(50) NOT NULL default '',
  `country_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`country_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `countries`
--

INSERT INTO countries (country_id, country_name) VALUES ('AL','Albania'),('BH','B i H'),('CR','Croatia'),('SL','Slovenia');

--
-- Table structure for table `delete_requests`
--

DROP TABLE IF EXISTS delete_requests;
CREATE TABLE `delete_requests` (
  `request_id` int(11) NOT NULL auto_increment,
  `request_date` date NOT NULL default '0000-00-00',
  `user_id` int(11) NOT NULL default '0',
  `reason` varchar(250) NOT NULL default '',
  `selection` varchar(250) NOT NULL default '',
  `proj_list` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`request_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `delete_requests`
--


--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS departments;
CREATE TABLE `departments` (
  `dept_id` int(5) NOT NULL default '0',
  `off_id` int(11) NOT NULL default '0',
  `name` varchar(20) NOT NULL default '',
  `telephone` varchar(15) NOT NULL default '',
  PRIMARY KEY  (`dept_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `departments`
--

INSERT INTO departments (dept_id, off_id, name, telephone) VALUES (1,1,'Audit',''),(2,1,'T & L',''),(3,1,'MCS',''),(4,1,'FAS',''),(5,2,'Audit',''),(6,2,'Tax',''),(7,2,'MCS',''),(8,2,'FAS',''),(9,3,'Audit',''),(10,3,'Tax',''),(11,3,'MCS',''),(12,3,'FUS',''),(13,4,'Audit',''),(14,4,'Tax',''),(15,4,'MCS',''),(16,4,'FAS',''),(17,4,'test1',''),(18,4,'test2','');

--
-- Table structure for table `industry_sectors`
--

DROP TABLE IF EXISTS industry_sectors;
CREATE TABLE `industry_sectors` (
  `sector_id` varchar(50) NOT NULL default '',
  `sector_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`sector_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `industry_sectors`
--

INSERT INTO industry_sectors (sector_id, sector_name) VALUES ('FS','Financial Services'),('MAN','Manufacturing'),('REI','Resources/Energy/Infra'),('PS','Public Sector'),('CIB','Consumer Intensive Business'),('COMM','Communication'),('OTH','Other');

--
-- Table structure for table `industry_subsectors`
--

DROP TABLE IF EXISTS industry_subsectors;
CREATE TABLE `industry_subsectors` (
  `sector_id` varchar(50) NOT NULL default '',
  `subsector_id` varchar(50) NOT NULL default '',
  `subsector_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`sector_id`,`subsector_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `industry_subsectors`
--

INSERT INTO industry_subsectors (sector_id, subsector_id, subsector_name) VALUES ('FS','Bank','Banking'),('FS','Cm','Capital markets'),('FS','Inv.fund','Investment fund'),('FS','Ins','Insurance'),('MAN','Proc.ind.','Process industry'),('MAN','Pharma','Pharmaceutics'),('MAN','Auto','Automotive'),('MAN','Gen.man.','General manufacturing'),('MAN','Aer/Def','Aerospace/Defense'),('CIB','Ret','Retail'),('CIB','Food','Food industry'),('REI','E&M','Energy and Mining'),('REI','Trans','Transport'),('REI','Petrol','Petroleum'),('REI','Util','Utility'),('PS','Health','Health'),('PS','Govern','Government'),('PS','Educ','Education - non profit'),('PS','Hosp','Hospitality'),('COMM','Telecom','Telecommunications'),('COMM','Media','Media'),('COMM','Info/Comm','Information & Communication');

--
-- Table structure for table `nsr`
--

DROP TABLE IF EXISTS nsr;
CREATE TABLE `nsr` (
  `nsr_id` int(11) NOT NULL auto_increment,
  `dept_id` int(11) NOT NULL default '0',
  `nsr` int(11) NOT NULL default '0',
  `month` int(11) NOT NULL default '0',
  `year` int(50) NOT NULL default '0',
  PRIMARY KEY  (`nsr_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `nsr`
--

INSERT INTO nsr (nsr_id, dept_id, nsr, month, year) VALUES (1,2,0,1,2001),(2,2,0,2,2001),(3,2,0,3,2001),(4,2,0,4,2001),(5,2,0,5,2001),(6,2,0,6,2001),(7,2,0,7,2001),(8,2,0,8,2001),(9,2,0,9,2001),(10,2,0,10,2001),(11,2,0,11,2001),(12,2,0,12,2001),(13,2,0,1,2002),(14,2,0,2,2002),(15,2,0,3,2002),(16,2,0,4,2002),(17,2,0,5,2002),(18,2,0,6,2002),(19,2,0,7,2002),(20,2,0,8,2002),(21,2,0,9,2002),(22,2,0,10,2002),(23,2,0,11,2002),(24,2,0,12,2002),(25,2,0,1,2003),(26,2,0,2,2003),(27,2,0,3,2003),(28,2,0,4,2003),(29,2,0,5,2003),(30,2,0,6,2003),(31,2,0,7,2003),(32,2,0,8,2003),(33,2,0,9,2003),(34,2,0,10,2003),(35,2,0,11,2003),(36,2,0,12,2003),(37,2,0,1,2004),(38,2,0,2,2004),(39,2,0,3,2004),(40,2,0,4,2004),(41,2,0,5,2004),(42,2,0,6,2004),(43,2,0,7,2004),(44,2,0,8,2004),(45,2,0,9,2004),(46,2,0,10,2004),(47,2,0,11,2004),(48,2,0,12,2004),(49,2,0,1,2005),(50,2,0,2,2005),(51,2,0,3,2005),(52,2,0,4,2005),(53,2,0,5,2005),(54,2,0,6,2005),(55,2,0,7,2005),(56,2,0,8,2005),(57,2,0,9,2005),(58,2,0,10,2005),(59,2,0,11,2005),(60,2,0,12,2005),(61,2,0,1,2006),(62,2,0,2,2006),(63,2,0,3,2006),(64,2,0,4,2006),(65,2,0,5,2006),(66,2,0,6,2006),(67,2,0,7,2006),(68,2,0,8,2006),(69,2,0,9,2006),(70,2,0,10,2006),(71,2,0,11,2006),(72,2,0,12,2006);

--
-- Table structure for table `nsr_budget`
--

DROP TABLE IF EXISTS nsr_budget;
CREATE TABLE `nsr_budget` (
  `nsr_id` int(11) NOT NULL auto_increment,
  `dept_id` int(11) NOT NULL default '0',
  `year` int(11) NOT NULL default '0',
  `month` int(11) NOT NULL default '0',
  `nsr` int(11) NOT NULL default '0',
  PRIMARY KEY  (`nsr_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `nsr_budget`
--

INSERT INTO nsr_budget (nsr_id, dept_id, year, month, nsr) VALUES (1,2,2002,1,0),(2,2,2002,2,0),(3,2,2002,3,0),(4,2,2002,4,0),(5,2,2002,5,0),(6,2,2002,6,0),(7,2,2002,7,0),(8,2,2002,8,0),(9,2,2002,9,0),(10,2,2002,10,0),(11,2,2002,11,0),(12,2,2002,12,0),(13,2,2003,1,0),(14,2,2003,2,0),(15,2,2003,3,0),(16,2,2003,4,0),(17,2,2003,5,0),(18,2,2003,6,0),(19,2,2003,7,0),(20,2,2003,8,0),(21,2,2003,9,0),(22,2,2003,10,0),(23,2,2003,11,0),(24,2,2003,12,0),(25,2,2004,1,0),(26,2,2004,2,0),(27,2,2004,3,0),(28,2,2004,4,0),(29,2,2004,5,0),(30,2,2004,6,0),(31,2,2004,7,0),(32,2,2004,8,0),(33,2,2004,9,0),(34,2,2004,10,0),(35,2,2004,11,0),(36,2,2004,12,0),(37,2,2005,1,0),(38,2,2005,2,0),(39,2,2005,3,0),(40,2,2005,4,0),(41,2,2005,5,0),(42,2,2005,6,0),(43,2,2005,7,0),(44,2,2005,8,0),(45,2,2005,9,0),(46,2,2005,10,0),(47,2,2005,11,0),(48,2,2005,12,0),(49,2,2006,1,0),(50,2,2006,2,0),(51,2,2006,3,0),(52,2,2006,4,0),(53,2,2006,5,0),(54,2,2006,6,0),(55,2,2006,7,0),(56,2,2006,8,0),(57,2,2006,9,0),(58,2,2006,10,0),(59,2,2006,11,0),(60,2,2006,12,0);

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS offices;
CREATE TABLE `offices` (
  `off_id` int(3) NOT NULL default '0',
  `name` varchar(15) NOT NULL default '',
  `address` varchar(200) default NULL,
  `telephone` varchar(15) default NULL,
  PRIMARY KEY  (`off_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `offices`
--

INSERT INTO offices (off_id, name, address, telephone) VALUES (1,'Albania','',''),(2,'B i H','',''),(3,'Croatia','',''),(4,'Slovenia','','');

--
-- Table structure for table `proj_contracted`
--

DROP TABLE IF EXISTS proj_contracted;
CREATE TABLE `proj_contracted` (
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `contract_date` date default NULL,
  `start_date` date default NULL,
  `end_date` date default NULL,
  `engagement_code` varchar(50) default NULL,
  `total_amount` int(11) default NULL,
  `DT_amount` int(11) default NULL,
  PRIMARY KEY  (`proj_id`,`status_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `proj_contracted`
--

INSERT INTO proj_contracted (proj_id, status_id, contract_date, start_date, end_date, engagement_code, total_amount, DT_amount) VALUES (3,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(2,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(4,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(5,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(6,1,'2002-10-02','2002-10-03','2002-10-04','',0,0),(7,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(8,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(10,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(13,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(14,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(15,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(16,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(17,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(18,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(19,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(20,1,'2002-09-28','2002-09-28','2002-09-28','',0,0),(1,1,'2002-10-07','2002-10-07','2002-10-07','',0,0),(46,1,'2002-10-17','2002-10-17','2002-10-17','',0,0),(52,1,'2002-10-29','2002-10-29','2002-10-29','',0,0);

--
-- Table structure for table `proj_opportunity`
--

DROP TABLE IF EXISTS proj_opportunity;
CREATE TABLE `proj_opportunity` (
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `next_step` varchar(50) default NULL,
  `nextstep_time` date default NULL,
  PRIMARY KEY  (`proj_id`,`status_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `proj_opportunity`
--

INSERT INTO proj_opportunity (proj_id, status_id, next_step, nextstep_time) VALUES (1,4,'meet','2002-09-28'),(2,4,'meet','2002-09-28'),(3,4,'meet','2002-09-28'),(4,4,'meet','2002-09-28'),(11,4,'meet','2002-09-28'),(12,4,'meet','2002-09-28'),(23,4,'meet','2002-09-28'),(26,4,'meet','2002-09-28'),(27,4,'meet','2002-09-28'),(28,4,'meet','2002-09-28'),(29,4,'meet','2002-09-28'),(30,4,'meet','2002-10-10'),(32,4,'meet','2002-10-16'),(33,4,'meet','2002-10-16'),(34,4,'meet','2002-10-16'),(35,4,'meet','2002-10-16'),(36,4,'meet','2002-10-16'),(40,4,'meet','2002-10-17'),(41,4,'meet','2002-10-17'),(45,4,'meet','2002-10-17'),(47,4,'meet','2002-10-18'),(66,4,'meet','2002-10-21'),(11,3,'meet','2004-01-18');

--
-- Table structure for table `proj_proposal`
--

DROP TABLE IF EXISTS proj_proposal;
CREATE TABLE `proj_proposal` (
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `proposal_due_date` date default NULL,
  `expected_start_date` date default NULL,
  `expected_end_date` date default NULL,
  `proposal_status` varchar(50) default NULL,
  `bid_description` text,
  PRIMARY KEY  (`proj_id`,`status_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `proj_proposal`
--

INSERT INTO proj_proposal (proj_id, status_id, proposal_due_date, expected_start_date, expected_end_date, proposal_status, bid_description) VALUES (2,2,'2002-09-28','2002-09-28','2002-09-28','to_be_submited',''),(21,2,'2002-09-28','2002-09-28','2002-09-28','to_be_submited',''),(22,2,'2002-09-28','2002-09-28','2002-09-28','to_be_submited',''),(24,2,'2002-09-28','2002-09-28','2002-09-28','to_be_submited',''),(25,2,'2002-09-28','2002-09-28','2002-09-28','to_be_submited',''),(32,2,'0000-00-00','0000-00-00','0000-00-00','to_be_submited',''),(33,2,'0000-00-00','0000-00-00','0000-00-00','to_be_submited',''),(48,2,'2002-10-23','2002-10-24','2002-10-25','lost',''),(47,2,'2002-10-18','2002-10-18','2002-10-18','to_be_submited',''),(46,2,'2002-10-17','2002-10-17','2002-10-17','to_be_submited',''),(52,2,'2002-10-25','2002-10-31','2002-11-20','to_be_submited','dfdgfgghg\r\nhkjhfj\r\n\'kjfkjdf\'\r\n\"kjkdfj\"\r\n'),(11,2,'2004-01-18','2004-01-18','2004-01-18','to_be_submited',NULL);

--
-- Table structure for table `project_deptDistrib`
--

DROP TABLE IF EXISTS project_deptDistrib;
CREATE TABLE `project_deptDistrib` (
  `proj_id` int(5) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `dept` int(11) NOT NULL default '0',
  `percent` int(3) NOT NULL default '0',
  PRIMARY KEY  (`proj_id`,`status_id`,`dept`)
) TYPE=MyISAM;

--
-- Dumping data for table `project_deptDistrib`
--

INSERT INTO project_deptDistrib (proj_id, status_id, dept, percent) VALUES (5,1,1,10),(5,1,2,20),(5,1,3,30),(5,1,4,40),(4,1,1,0),(4,1,2,0),(4,1,3,0),(4,1,4,0),(30,4,1,10),(30,4,2,30),(30,4,3,50),(30,4,4,10),(3,1,1,0),(3,1,2,0),(3,1,3,0),(3,1,4,0),(6,1,1,0),(6,1,2,0),(6,1,3,0),(6,1,4,0),(1,4,1,0),(1,4,2,0),(1,4,3,0),(1,4,4,0),(8,1,1,0),(8,1,2,0),(8,1,3,0),(8,1,4,0),(32,4,1,0),(32,4,2,0),(32,4,3,0),(32,4,4,0),(32,2,1,10),(32,2,2,30),(32,2,3,40),(32,2,4,20),(23,4,4,0),(23,4,3,0),(23,4,2,0),(23,4,1,0),(2,1,4,0),(2,1,3,0),(2,1,2,0),(2,1,1,0),(33,4,1,10),(33,4,2,50),(33,4,3,20),(33,4,4,20),(33,2,1,10),(33,2,2,30),(33,2,3,30),(33,2,4,30),(52,1,4,0),(52,1,3,0),(52,1,2,0),(52,1,1,0),(47,2,4,0),(47,2,3,0),(47,2,2,0),(47,2,1,0),(34,4,1,25),(34,4,2,25),(34,4,3,50),(34,4,4,0),(34,2,1,25),(34,2,2,25),(34,2,3,50),(34,2,4,0),(47,4,4,0),(47,4,3,0),(47,4,2,0),(47,4,1,0),(35,4,1,0),(35,4,2,0),(35,4,3,0),(35,4,4,0),(35,2,1,0),(35,2,2,0),(35,2,3,0),(35,2,4,0),(7,1,4,0),(7,1,3,0),(7,1,2,0),(7,1,1,0),(36,4,1,0),(36,4,2,0),(36,4,3,0),(36,4,4,0),(40,4,4,0),(40,4,3,20),(40,4,2,70),(40,4,1,10),(44,4,2,0),(44,4,3,0),(44,4,4,0),(0,0,0,0),(37,4,1,0),(37,4,2,0),(37,4,3,0),(37,4,4,0),(46,1,2,0),(46,1,3,0),(46,1,4,0),(46,2,1,0),(46,2,2,0),(46,2,3,0),(46,2,4,0),(41,4,4,0),(41,4,3,0),(41,4,2,0),(41,4,1,0),(44,4,1,0),(46,1,1,0),(45,4,4,0),(45,4,3,0),(45,4,2,0),(45,4,1,0),(48,2,1,10),(48,2,2,30),(48,2,3,60),(48,2,4,0),(11,3,1,0),(11,3,2,0),(11,3,3,0),(11,3,4,0),(11,2,1,0),(11,2,2,0),(11,2,3,0),(11,2,4,0),(54,4,1,50),(54,4,2,50),(54,4,3,0),(54,4,4,0),(11,4,4,0),(11,4,3,0),(11,4,2,0),(11,4,1,0),(66,4,4,0),(66,4,3,0),(66,4,2,0),(66,4,1,0),(52,2,4,0),(52,2,3,0),(52,2,2,0),(52,2,1,0),(54,3,4,0),(54,3,3,0),(54,3,2,50),(54,3,1,50),(53,2,4,40),(53,2,3,30),(53,2,2,20),(53,2,1,10);

--
-- Table structure for table `project_managers`
--

DROP TABLE IF EXISTS project_managers;
CREATE TABLE `project_managers` (
  `id` int(11) NOT NULL auto_increment,
  `proj_id` int(5) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Dumping data for table `project_managers`
--

INSERT INTO project_managers (id, proj_id, status_id, user_id) VALUES (1,6,0,0),(2,8,1,22),(3,8,1,21),(6,32,4,21),(46,52,2,62),(45,52,2,11),(10,33,4,21),(11,33,4,22),(12,33,2,21),(13,33,2,22),(14,33,2,3),(49,52,1,11),(48,52,1,62),(35,48,2,21),(21,34,4,22),(22,34,2,22),(40,7,1,22),(24,35,4,21),(28,40,4,21),(27,0,0,0),(29,40,4,22),(50,52,1,21),(47,52,2,21),(41,7,1,21),(65,53,2,21),(66,53,2,22),(67,53,2,0),(71,54,4,11),(72,54,4,62),(73,54,4,0),(74,54,3,11),(75,54,3,62),(76,54,3,0);

--
-- Table structure for table `project_partners`
--

DROP TABLE IF EXISTS project_partners;
CREATE TABLE `project_partners` (
  `id` int(11) NOT NULL auto_increment,
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Dumping data for table `project_partners`
--

INSERT INTO project_partners (id, proj_id, status_id, user_id) VALUES (1,6,0,0),(2,32,4,21),(3,32,4,3),(42,8,1,21),(6,33,4,21),(7,33,4,3),(8,33,2,21),(9,33,2,3),(26,7,1,0),(23,48,2,3),(14,34,4,21),(15,34,2,21),(25,7,1,21),(17,40,4,21),(18,40,4,21),(31,52,2,0),(30,52,2,21),(32,52,2,21),(33,52,1,0),(34,52,1,21),(35,52,1,21),(46,53,2,0),(47,53,2,21),(50,54,4,0),(51,54,3,0);

--
-- Table structure for table `project_team`
--

DROP TABLE IF EXISTS project_team;
CREATE TABLE `project_team` (
  `id` int(11) NOT NULL auto_increment,
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `task` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Dumping data for table `project_team`
--

INSERT INTO project_team (id, proj_id, status_id, user_id, task) VALUES (5,8,1,22,''),(14,7,1,21,''),(15,7,1,0,'');

--
-- Table structure for table `project_timeDistrib`
--

DROP TABLE IF EXISTS project_timeDistrib;
CREATE TABLE `project_timeDistrib` (
  `distrib_id` int(11) NOT NULL auto_increment,
  `proj_id` int(11) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '0',
  `amount` decimal(11,1) NOT NULL default '0.0',
  `month` int(50) NOT NULL default '0',
  `year` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`distrib_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `project_timeDistrib`
--

INSERT INTO project_timeDistrib (distrib_id, proj_id, status_id, amount, month, year) VALUES (1,2,4,12.0,1,'2003'),(2,2,4,6.0,2,'2003'),(3,3,4,17.0,1,'2002'),(4,3,4,3.0,2,'2002'),(5,2,1,12.0,1,'2003'),(6,2,1,6.0,2,'2003'),(7,3,1,17.0,1,'2003'),(8,3,1,3.0,2,'2003'),(13,6,1,8.0,3,'2003'),(14,7,1,40.0,4,'2003'),(15,7,1,40.0,5,'2003'),(16,8,1,20.0,3,'2003'),(17,8,1,40.0,4,'2003'),(18,8,1,40.0,5,'2003'),(21,10,1,11.0,1,'2003'),(22,10,1,12.0,2,'2003'),(23,11,4,10.0,3,'2003'),(24,11,4,9.0,4,'2003'),(25,12,4,15.0,2,'2003'),(26,12,4,10.0,4,'2003'),(27,12,4,5.0,5,'2003'),(28,13,1,12.0,2,'2003'),(29,14,1,3.0,1,'2003'),(30,15,1,20.0,3,'2003'),(31,16,1,5.0,1,'2003'),(32,17,1,8.0,2,'2003'),(33,17,1,8.0,3,'2003'),(34,18,1,2.0,1,'2003'),(35,19,1,4.0,4,'2003'),(36,19,1,8.0,5,'2003'),(37,20,1,4.0,1,'2003'),(38,21,2,5.0,2,'2003'),(39,21,2,20.0,3,'2003'),(40,22,2,25.0,3,'2003'),(41,22,2,25.0,4,'2003'),(42,23,4,10.0,2,'2003'),(43,23,4,2.0,3,'2003'),(44,24,2,8.0,3,'2003'),(45,25,2,20.0,4,'2002'),(46,25,2,20.0,5,'2002'),(47,26,4,15.0,4,'2003'),(48,26,4,5.0,5,'2003'),(49,27,4,20.0,3,'2003'),(50,28,4,2.0,5,'2003'),(51,29,4,15.0,4,'2002'),(52,29,4,15.0,5,'2002'),(59,5,1,11.0,9,'2002'),(57,5,1,15.0,10,'2002'),(61,3,1,2.0,3,'2003'),(62,32,2,5.0,10,'2002'),(63,32,2,1.0,10,'2002'),(113,52,1,10.0,10,'2002'),(112,52,2,10.0,10,'2002'),(66,33,4,10.0,10,'2002'),(67,33,4,0.0,10,'2002'),(68,33,2,10.0,10,'2002'),(69,33,2,0.0,10,'2002'),(70,33,2,30.0,10,'2002'),(111,52,2,15.0,11,'2002'),(104,48,2,15.0,11,'2002'),(103,48,2,20.0,12,'2002'),(79,34,4,10.0,10,'2002'),(80,34,4,15.0,11,'2002'),(81,34,2,10.0,10,'2002'),(82,34,2,15.0,11,'2002'),(114,52,1,15.0,11,'2002'),(85,35,4,5.0,10,'2002'),(94,40,4,5.0,11,'2002'),(92,0,0,0.0,0,''),(93,40,4,10.0,10,'2002'),(99,0,0,0.0,0,''),(105,48,2,10.0,10,'2002'),(125,53,2,10.0,12,'2002'),(126,53,2,15.0,1,'2003'),(138,11,2,10.0,3,'2003'),(137,11,2,9.0,4,'2003'),(129,54,4,14.1,12,'2002'),(130,54,4,3.6,12,'2002'),(131,54,3,14.1,12,'2002'),(132,54,3,3.6,12,'2002'),(136,11,3,9.0,4,'2003'),(135,11,3,10.0,3,'2003');

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS projects;
CREATE TABLE `projects` (
  `proj_id` int(5) NOT NULL default '0',
  `status_id` int(11) NOT NULL default '1',
  `register_date` date NOT NULL default '0000-00-00',
  `close_date` date default NULL,
  `name` varchar(100) NOT NULL default 'Project Name',
  `client_status` varchar(50) NOT NULL default 'OT',
  `client_name` varchar(100) NOT NULL default '',
  `country` varchar(50) NOT NULL default '""',
  `industry_code` varchar(15) NOT NULL default '',
  `industry_subsector` varchar(15) NOT NULL default '',
  `description` varchar(255) NOT NULL default 'Project Description',
  `comments` varchar(255) NOT NULL default '',
  `fees` decimal(11,1) NOT NULL default '0.0',
  `dept_id` int(5) NOT NULL default '0',
  `off_id` int(11) NOT NULL default '0',
  `information_source` varchar(50) NOT NULL default 'Client',
  `information_staff` varchar(50) NOT NULL default '',
  `information_date` date NOT NULL default '0000-00-00',
  `probability_for_success` int(11) NOT NULL default '0',
  `follow_up_person` varchar(50) NOT NULL default '',
  `managers` varchar(50) NOT NULL default '',
  `partners` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`proj_id`,`status_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `projects`
--

INSERT INTO projects (proj_id, status_id, register_date, close_date, name, client_status, client_name, country, industry_code, industry_subsector, description, comments, fees, dept_id, off_id, information_source, information_staff, information_date, probability_for_success, follow_up_person, managers, partners) VALUES (1,4,'2002-09-28','2002-10-07','Project 1','OT','Client Name','Albania','222','333','This is a test project.','test',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(2,4,'2002-09-28','2002-09-28','Siguria','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(3,4,'2002-09-28','2002-09-28','AMC audit 2001','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(3,1,'2002-09-28',NULL,'AMC audit 2001','OT','','','','','','',22.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(2,1,'2002-09-28',NULL,'Siguria','OT','','','','','','dsds',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(4,4,'2002-09-28','2002-09-28','New Bank of Kosova','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(4,1,'2002-09-28',NULL,'New Bank of Kosova','OT','','','','','','ghgh',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(5,1,'2002-09-28',NULL,'Economic Bank of Kosova','OT','','','','','','',26.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(6,1,'2002-09-28',NULL,'Bank of Privat Business in Kosova','OT','','','','','','',0.0,2,1,'Personal','3','2002-10-01',0,'3','',''),(7,1,'2002-09-28',NULL,'Alb Telecom','OT','','BH','CIB','Food','','',0.0,2,1,'DT Info','21','2002-09-28',0,'22','',''),(8,1,'2002-09-28',NULL,'INSIG','OT','','AL','COMM','Media','','',0.0,3,1,'Personal','3','2002-09-28',0,'3','maxi,gimi','gimi'),(10,1,'2002-09-28',NULL,'FEFAD','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(11,4,'2002-09-28','2004-01-18','Besa Foundation','OT','','','','','','hhghg,ljl ( jl ) lklk',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(12,4,'2002-09-28',NULL,'Vodafone','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(13,1,'2002-09-28',NULL,'CDF Kosova','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(14,1,'2002-09-28',NULL,'Riinvest','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(15,1,'2002-09-28',NULL,'Grand Hotel','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(16,1,'2002-09-28',NULL,'SBASHK','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(17,1,'2002-09-28',NULL,'Union Finance','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(18,1,'2002-09-28',NULL,'Plan Int\'l','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(19,1,'2002-09-28',NULL,'EU Kosova','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(20,1,'2002-09-28',NULL,'Danida','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(23,4,'2002-09-28',NULL,'CISA','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',95,'3','',''),(26,4,'2002-09-28',NULL,'Soros in Kosova','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(27,4,'2002-09-28',NULL,'Alpha Bank','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',25,'3','',''),(28,4,'2002-09-28',NULL,'American Bank of Albania','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',25,'3','',''),(29,4,'2002-09-28',NULL,'KESH','OT','','','','','','',0.0,2,1,'Personal','3','2002-09-28',40,'3','',''),(1,1,'2002-09-28','2002-10-07','Project 1','OT','Client Name','Albania','222','333','This is a test project.','test',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(30,4,'2002-10-10',NULL,'Opportunity 1','OT','','','','','','',0.0,3,1,'Personal','3','2002-10-10',0,'3','',''),(32,4,'0000-00-00','0000-00-00','proj 32','OT','','','','','','',0.0,2,1,'Personal','3','2002-10-16',0,'3','',''),(33,4,'0000-00-00','0000-00-00','Oportuniti 12','OT','','','','','','',10.0,2,1,'Personal','3','2002-10-16',0,'3','',''),(34,4,'0000-00-00','0000-00-00','Proj13','OT','','','','','','',25.0,2,1,'Personal','3','2002-10-16',0,'3','',''),(35,4,'0000-00-00','0000-00-00','Proj14','OT','','','','','','',5.0,4,1,'Personal','3','2002-10-16',0,'3','',''),(36,4,'0000-00-00','0000-00-00','Proj 15','OT','','','','','','',0.0,4,1,'Personal','3','2002-10-16',0,'3','',''),(39,1,'0000-00-00',NULL,'Project Name','OT','','\"\"','','','Project Description','',0.0,0,0,'Client','','0000-00-00',0,'','',''),(38,1,'0000-00-00',NULL,'Project Name','OT','','\"\"','','','Project Description','',0.0,0,0,'Client','','0000-00-00',0,'','',''),(46,1,'2002-10-17',NULL,'Proj 21','OT','','','','','','',0.0,2,1,'Personal','3','2002-10-17',0,'3','',''),(40,4,'2002-10-17','2002-10-17','Proj 15','OT','','','','','','',15.0,2,1,'Personal','3','2002-10-17',0,'3','',''),(45,4,'2002-10-17',NULL,'Project 20','OT','','','','','','',0.0,2,1,'Personal','3','2002-10-17',0,'3','',''),(44,4,'2002-10-17',NULL,'Project 19','OT','','','','','','',0.0,2,1,'Personal','3','2002-10-17',0,'3','',''),(47,4,'2002-10-18','2002-10-18','Proj 20','OC','','','','','','',0.0,2,1,'Personal','3','2002-10-18',0,'3','',''),(52,2,'2002-10-29','2002-10-29','Test 1','KT','','BH','COMM','Media','','',25.0,1,1,'Internet','62','2002-10-01',0,'62','',''),(52,1,'2002-10-29',NULL,'Test 1','KT','','BH','COMM','Media','','',25.0,1,1,'Internet','62','2002-10-01',0,'62','',''),(54,4,'2002-12-16','2002-12-16','p2','CCJC','','','','','','',17.7,2,1,'','3','2002-12-16',0,'3','test,test1,NULL','NULL'),(11,2,'2002-09-28',NULL,'Besa Foundation','OT','','','','','','hhghg,ljl ( jl ) lklk',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(11,3,'2002-09-28','2004-01-18','Besa Foundation','OT','','','','','','hhghg,ljl ( jl ) lklk',0.0,2,1,'Personal','3','2002-09-28',0,'3','',''),(53,2,'2002-12-16','2002-12-16','p1','CCJC','','','','','','',25.0,2,1,'','3','2002-12-16',0,'3','',''),(54,3,'2002-12-16','2002-12-16','p2','CCJC','','','','','','',17.7,2,1,'','3','2002-12-16',0,'3','','');

--
-- Table structure for table `projects_users_oret_java`
--

DROP TABLE IF EXISTS projects_users_oret_java;
CREATE TABLE `projects_users_oret_java` (
  `proj_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `java_data` date NOT NULL default '0000-00-00',
  `nr_oret_java` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`proj_id`,`user_id`,`java_data`)
) TYPE=MyISAM;

--
-- Dumping data for table `projects_users_oret_java`
--


--
-- Table structure for table `projects_users_oret_total`
--

DROP TABLE IF EXISTS projects_users_oret_total;
CREATE TABLE `projects_users_oret_total` (
  `proj_id` int(11) NOT NULL default '0',
  `user_id` int(11) NOT NULL default '0',
  `nr_oret_total` int(11) NOT NULL default '0',
  PRIMARY KEY  (`proj_id`,`user_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `projects_users_oret_total`
--


--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS roles;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL default '0',
  `role_name` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`role_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `roles`
--

INSERT INTO roles (role_id, role_name) VALUES (1,'Senior Manager'),(2,'Manager'),(3,'Supervisor'),(4,'Senior Associate'),(5,'Associate'),(6,'Director'),(7,'Analyst'),(8,'Partner');

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS session;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS status;
CREATE TABLE `status` (
  `status_id` int(3) NOT NULL default '0',
  `status_name` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `status`
--

INSERT INTO status (status_id, status_name) VALUES (1,'Contracted'),(2,'Proposal'),(3,'Qualified'),(4,'Opportunity');

--
-- Table structure for table `su`
--

DROP TABLE IF EXISTS su;
CREATE TABLE `su` (
  `password` varchar(50) NOT NULL default ''
) TYPE=MyISAM;

--
-- Dumping data for table `su`
--

INSERT INTO su (password) VALUES ('su');

--
-- Table structure for table `user_access`
--

DROP TABLE IF EXISTS user_access;
CREATE TABLE `user_access` (
  `user_id` int(5) NOT NULL default '0',
  `off_id` int(11) NOT NULL default '0',
  `dept_id` int(3) NOT NULL default '0',
  `accr_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`off_id`,`dept_id`,`accr_id`)
) TYPE=MyISAM;

--
-- Dumping data for table `user_access`
--

INSERT INTO user_access (user_id, off_id, dept_id, accr_id) VALUES (3,1,1,0),(3,1,1,1),(3,1,1,2),(3,1,1,3),(3,1,1,4),(3,1,1,5),(3,1,2,0),(3,1,2,1),(3,1,2,2),(3,1,2,3),(3,1,2,4),(3,1,2,5),(3,1,3,0),(3,1,3,1),(3,1,3,2),(3,1,3,3),(3,1,3,4),(3,1,3,5),(3,1,4,0),(3,1,4,1),(3,1,4,2),(3,1,4,3),(3,1,4,4),(3,1,4,5),(3,2,5,0),(3,2,5,1),(3,2,5,4),(3,2,6,0),(3,2,6,1),(3,2,6,4),(3,2,7,0),(3,2,7,1),(3,2,7,4),(3,2,8,0),(3,2,8,1),(3,2,8,4),(3,3,9,0),(3,3,9,1),(3,3,10,0),(3,3,10,1),(3,3,11,0),(3,3,11,1),(3,3,12,0),(3,3,12,1),(3,4,13,0),(3,4,13,1),(3,4,14,0),(3,4,14,1),(3,4,15,0),(3,4,15,1),(3,4,16,0),(3,4,16,1),(3,4,17,0),(3,4,17,1),(3,4,18,0),(3,4,18,1),(11,1,1,0),(11,1,1,1),(11,1,1,2),(11,1,2,0),(11,1,2,2),(62,1,1,0),(62,1,1,1),(62,1,1,2),(62,1,1,3),(62,1,1,4),(62,1,1,5),(62,1,2,0),(62,1,2,1),(62,1,2,2),(62,1,2,3),(62,1,2,4),(62,1,2,5);

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `user_id` int(5) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `office` varchar(50) NOT NULL default '1',
  `department` varchar(50) NOT NULL default '1',
  `firstname` varchar(15) NOT NULL default '',
  `lastname` varchar(15) NOT NULL default '',
  `address` varchar(50) NOT NULL default '',
  `tel1` varchar(15) NOT NULL default '',
  `tel2` varchar(50) NOT NULL default '',
  `e_mail` varchar(50) NOT NULL default '',
  `roles` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `x` (`username`)
) TYPE=MyISAM;

--
-- Dumping data for table `users`
--

INSERT INTO users (user_id, username, password, office, department, firstname, lastname, address, tel1, tel2, e_mail, roles) VALUES (3,'dasho','dasho','1','2','Dashamir','Hoxha','test','','','dasho@yahoo.com','1'),(6,'alban2','albo','0','','Alban','Ruci','','','','','2,8'),(11,'test','','1','1','333333333','---------','','tel1','','','2'),(21,'gimi','gimi','1','4','Agim','Gashi','','','','','2,8'),(22,'maxi','','1','1','','','','','','','2'),(54,'test10','','0','','','','','','','',''),(55,'test11','','0','','','','','','','',''),(56,'test12','','0','','','','','','','',''),(62,'test1','tst','1','2','tst','','','tel1','','','2,6');

