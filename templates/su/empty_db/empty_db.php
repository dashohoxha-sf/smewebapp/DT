<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class empty_db extends WebObject
{
  function on_empty($event_args)
    {
      $table_list = $event_args["table_list"];
      $arr_tables = explode(",", $table_list);
      for ($i=0; $i < sizeof($arr_tables); $i++)
        {
          $table = $arr_tables[$i];
          if (TEST)
            {
              WebApp::message("Table '$table' emptied.");
              WebApp::execQuery("DELETE FROM $table");
            }
        }
      if (!TEST)
        {
          $msg = "No table emptied! Sorry, this feature of the\n"
            ."application is enabled only during the test.";
          WebApp::message($msg);
        }
    }
}
?>