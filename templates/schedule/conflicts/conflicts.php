<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class conflicts extends WebObject
{

  function eventHandler($event)
    {
      global $session,$event;
      extract($event->args);



    }

  function onRender()
    {
      global $session,$event;
      extract($event->args);

      $nr_bg = 0;
      $nr    = 0;
     
      //get the start and end time of the timeframe
      $currMonthCheck = WebApp::getSVar("select_by_time->currMonthCheck");
      if ($currMonthCheck=="checked")
        {
          $fromMonth = get_curr_date("M");  //current month
          $toMonth = get_curr_date("M");
          $fromYear = get_curr_date("Y");   //current year
          $toYear = get_curr_date("Y");
          $timeFrame = "$toMonth $toYear";
        }
      else
        {
          $fromMonth = WebApp::getSVar("select_by_time->fromMonth");
          $toMonth   = WebApp::getSVar("select_by_time->toMonth");
          $fromYear  = WebApp::getSVar("select_by_time->fromYear");
          $toYear    = WebApp::getSVar("select_by_time->toYear");
          $timeFrame = "From $fromMonth $fromYear To $toMonth $toYear";
        }

      //kushti i filtrimit ---------------------------------------------------------------
      //build department condition
      $office_depts = WebApp::getSVar("select_by_dept->values");
      $arr_depts = array();
      $arr_office_depts = explode(";",$office_depts);
      while ( list($i,$item) = each($arr_office_depts) )
	{
	  list($off_id,$dept_id) = explode("_", $item);
	  if ($dept_id<>"")  $arr_depts[] = $dept_id;
	}
        
      if (sizeof($arr_depts) > 0) 
	{$dept_condition = " AND t1.department IN (".implode(',', $arr_depts).")";}
      else
	{$dept_condition = "";}
           
      WebApp::addVar("dept_condition",$dept_condition);

      //Sipas roleve --------------------------------------------------------------------
      $roleCheck = WebApp::getSVar("select_by_role->values");
      IF($roleCheck != "")
	{$role_kushti = " AND t2.role_id IN (".$roleCheck.")";}
      ELSE
	{$role_kushti = "";}
        
      WebApp::addVar("role_kushti",$role_kushti);
      //---------------------------------------------------------------------------------

      //format the starting and ending date of the timeframe
      //in the format required by the database: yyyy-mm-dd;
      //start of the timeframe is 01 of the start month,
      //end of the timeframe is 01 of the next to end month
      $t1 = strtotime("01 $fromMonth $fromYear");
      $data_fillim_interval = date("d.m.Y", $t1);
      $t1 = date("Y-m-d", $t1);
      $t2 = strtotime("01 $toMonth $toYear");
      $t2 = mktime(0,0,0, date("m",$t2)+1, 1, date("Y",$t2));
      $data_mbarim_interval = date("d.m.Y", $t2);
      $t2 = date("Y-m-d", $t2);

      $data_fillim = $t1;
      $data_mbarim = $t2;
             
      $data_interval = $data_fillim_interval." - ".$data_mbarim_interval;
      WebApp::addVar("data_interval",$data_interval);
      //$report->rmVar("report_data_fillim");
      //$report->rmVar("report_data_mbarim");
      //$data_mbarim = $event_args["data_mbarim"];
      //$data_mbarim = $report_data_mbarim;
             
      //formohet vektori i datave -----------------------------------------------
      WebApp::addVar("date_dita",$data_fillim);
      $rs = WebApp::openRS("date_dita");
      IF(!$rs->EOF())
	{$date_dita_dif = $rs->Field("date_dita");}
         
      $date_dita_dif = "-".$date_dita_dif;
               
      WebApp::addVar("date_dita_dif",$date_dita_dif);
             
      $rs = WebApp::openRS("data_java");
      IF(!$rs->EOF())
	{$data_fillim     = $rs->Field("data_java");
	$data_fillim_dmy = $rs->Field("data_muaji_viti");}
               
      //per daten e mbarimit -----------------------
      WebApp::addVar("date_dita",$data_mbarim);
      $rs = WebApp::openRS("date_dita");
      IF(!$rs->EOF())
	{$date_dita_dif = $rs->Field("date_dita");}
         
      $date_dita_dif = "-".$date_dita_dif;
               
      WebApp::addVar("date_dita_dif",$date_dita_dif);
             
      $rs = WebApp::openRS("data_java");
      IF(!$rs->EOF())
	{$data_mbarim = $rs->Field("data_java");}
              
      $i = 0;
      $java_data_array[$i]     = $data_fillim;
      $java_data_dmy_array[$i] = $data_fillim_dmy;
      WebApp::addVar("date_dita_dif","+7");
      while ($data_fillim < $data_mbarim):
                      
	WebApp::addVar("date_dita",$data_fillim);
      $rs = WebApp::openRS("data_java");
                      
      IF(!$rs->EOF())
	{$data_fillim     = $rs->Field("data_java");
	$data_fillim_dmy = $rs->Field("data_muaji_viti");}
                      
      $i++;
      $java_data_array[$i]     = $data_fillim;
      $java_data_dmy_array[$i] = $data_fillim_dmy;
      endwhile;
               
      //$gjatesia = strlen($java_data_interval) - 1;
      //$java_data_interval = substr($java_data_interval, 0, $gjatesia);
      //-----------------------------------------------------------
             
      $ls_koka_tabele  = "<Table border=0 cellspacing=1 cellpadding=1><TR bgcolor=cccccc><TD align=center width=110><b>User</b></TD>";
      $ls_trupi_tabele = "";
             
      $i = 0;
      while ($i < count($java_data_dmy_array)):
	$ls_koka_tabele = $ls_koka_tabele."<TD align=center width=30><b>".$java_data_dmy_array[$i]."</b></TD>";
      $totali_i_oreve[$i] = 0;
      $i++;
      endwhile;
      $nr_col_span=$i+1;
      $ls_koka_tabele = $ls_koka_tabele."</TR>";
             

      $i = count($java_data_array) - 1;
      WebApp::addVar("data_fillim_interval",$java_data_array[0]);
      WebApp::addVar("data_mbarim_interval",$java_data_array[$i]);
            
      //$rs = WebApp::openRS("proj_user_name");
      $rs = WebApp::openRS("users");
      $rs->MoveFirst();
      while (!$rs->EOF())
	{
	  $user_id   = $rs->Field("user_id");
	  $firstname = $rs->Field("firstname");
	  $lastname  = $rs->Field("lastname");
	  $role_name = $rs->Field("role_name");

	  WebApp::addVar("user_id",$user_id);
                 
	  $nr = $nr + 1;
                 
	  IF($role_name != $role_name_old)
	    {$role_html = "<TR bgcolor=e2e2e2><TD colspan=".$nr_col_span."><b><i>".$role_name."</i></b></TD></TR>";
	    $role_name_old = $role_name;}
	  ELSE
	    {$role_html = "";}

	  //IF ($nr_bg == 0)
	  //   {$bg_color = "ffffff";
	  //    $nr_bg = 1;}
	  //ELSE
	  //   {$bg_color = "f7f7f7";
	  //    $nr_bg = 0;}
                     

	  $i = 0;
	  $print_user = 0;
                 
	  $rs_user_oret = WebApp::openRS("user_oret_javore_totale");
	  $rs_user_oret->MoveFirst();
	  while (!$rs_user_oret->EOF())
	    {
	      $java_data            = $rs_user_oret->Field("java_data");
	      $nr_oret_javore_total = $rs_user_oret->Field("nr_oret_javore_total");
                        
	      $user_java[$i]                 = $java_data;
	      $user_nr_oret_javore_total[$i] = $nr_oret_javore_total;
                        
	      IF ($nr_oret_javore_total != 40)
		{$print_user = 1;}
                        
	      $i++;
	      $rs_user_oret->MoveNext();
	    }

	  IF (count($user_nr_oret_javore_total) == 0)
	    {$print_user = 1;}
                    
	  IF ($print_user == 1)
	    {
                 
	      $ls_trupi_tabele = $ls_trupi_tabele.$role_html."<TR bgcolor=eeeeee><TD width=110 NOWRAP><a href=\"JavaScript:GoTo('thisPage?event=schedule.gotobox(box=user_proj;user_id=".$user_id.")')\">".$firstname." ".$lastname."</a></TD>";
                     
	      $i = 0; 
	      while ($i < count($java_data_array)):
                        
		$oret = "no";
	      $j = 0;
	      while ($j < count($user_java)):
		{
		  IF ($java_data_array[$i] == $user_java[$j])
		    {
		      IF ($user_nr_oret_javore_total[$j] < 40)
			{$bg = "FCF2A0";}
		      ELSEIF ($user_nr_oret_javore_total[$j] == 40)
			{$bg = "ACFD93";}
		      ELSEIF ($user_nr_oret_javore_total[$j] > 40)
			{$bg = "FC7F7F";}
                                   
		      $nr_oret = -40 + $user_nr_oret_javore_total[$j];
		      $ls_trupi_tabele = $ls_trupi_tabele."<TD width=30 align=center bgcolor=".$bg.">".$nr_oret."</TD>";
		      $oret = "ok";
		      //$totali_i_oreve[$i] = $totali_i_oreve[$i] + $user_nr_oret_javore_total[$j];
		      break;
		    }
		}
	      $j++;
	      endwhile;
                        
	      IF ($oret == "no")
		{$ls_trupi_tabele = $ls_trupi_tabele."<TD width=30 align=center bgcolor=FCF2A0>-40</TD>";}
	      $i++;
	      endwhile;
	    }

	  unset($user_java);
	  unset($user_nr_oret_javore_total);
                 
	  $ls_trupi_tabele = $ls_trupi_tabele."</TR>";
                 
	  $rs->MoveNext();
	}
              
      //$ls_fundi_tabele = "<TR bgcolor=eeeeee><TD><b>Totali i oreve</b></TD>";
      //$i = 0;
      //while ($i < count($totali_i_oreve)):
      //       $ls_fundi_tabele = $ls_fundi_tabele."<TD align=center>".$totali_i_oreve[$i]."</TD>";
      //       $i++;
      //endwhile;
      //$ls_fundi_tabele = $ls_fundi_tabele."</TR>";
              
      //$ls_html = $ls_koka_tabele.$ls_trupi_tabele.$ls_fundi_tabele."</Table>";
      $ls_html = $ls_koka_tabele.$ls_trupi_tabele."</Table>";
     
      WebApp::addVar("ls_html",$ls_html);
    }
}
?>
