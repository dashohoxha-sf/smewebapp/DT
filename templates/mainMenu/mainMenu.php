<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class mainMenu extends WebObject
{
  function init()
    {
      global $session;

      //get the access levels
      $username = WebApp::getSVar("username");
      $u_id = WebApp::getSVar("u_id");
      $arrAccLevel = array();
      if ($username=="superuser")
        {
          $arrAccLevel = array(ACCR_SU, ACCR_ADMIN);
        }
      else
        {
          $query = "SELECT DISTINCT accr_id FROM user_access WHERE user_id='$u_id'";
          $rs = WebApp::execQuery($query);
          while (!$rs->EOF())
            {
              $arrAccLevel[] = $rs->Field("accr_id");
              $rs->MoveNext();
            }
        }
 
      //get the rights of the user
      $monitor = (in_array(ACCR_VIEW, $arrAccLevel) ? 'true' : 'false');  
      $modify = (in_array(ACCR_EDIT, $arrAccLevel) ? 'true' : 'false');
      $financ = (in_array(ACCR_FINANC, $arrAccLevel) ? 'true' : 'false');
      $schedule = (in_array(ACCR_SCHEDULE, $arrAccLevel) ? 'true' : 'false');
      $superuser    = (in_array(ACCR_SU, $arrAccLevel) ? 'true' : 'false');
      $administrate = (in_array(ACCR_ADMIN, $arrAccLevel) ? 'true' : 'false');

      //save these as state variables of the menu
      $this->addSVars( array("monitor" => $monitor,
                             "modify" => $modify,
                             "financ" => $financ,
                             "schedule" => $schedule,
                             "superuser" => $superuser,
                             "administrate" => $administrate
                             ), 
                       "DB" );

      //select an initial tab 
      if      ($monitor=='true')        $initial_tab = "monitor";
      else if ($modify=='true' 
               or $financ=='true')      $initial_tab = "modify";
      else if ($schedule=='true')       $initial_tab = "schedule";
      else if ($superuser=='true')      $initial_tab = "superuser";
      else if ($administrate=='true')   $initial_tab = "administrate";
      $this->addSVar("tab", $initial_tab);

      WebApp::addSVar("module", $initial_tab, "DB");
    }

  function on_select($event_args)
    {
      $menu_tab = $event_args["tab"];
      $this->setSVar("tab", $menu_tab);
      WebApp::setSVar("module", $menu_tab);
    }

  function onRender()
    {
      //add class variables for the tabs
      WebApp::addVars( array(
                             "class_monitor"    => "mainMenu-tab",
                             "class_modify"     => "mainMenu-tab",
                             "class_schedule"   => "mainMenu-tab",
                             "class_administrate" => "mainMenu-tab",
                             "class_superuser"  => "mainMenu-tab"
                             ));

      //set another class for the selected tab
      global $session;
      $menu_tab = $this->getSVar("tab");
      WebApp::addVar("class_".$menu_tab, "mainMenu-tab-current");
    }
}
?>