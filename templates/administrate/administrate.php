<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class administrate extends WebObject
{
  function onParse()
    {
      $username = WebApp::getSVar("username");
      if ($username=="superuser")
        {
          WebApp::addVar("selectedUsers", "su_selectedUsers");
        }
      else
        {
          WebApp::addVar("selectedUsers", "user_selectedUsers");
        }
    }

  function onRender()
    {
      //these variables determine which recordsets
      //will be used; "su_" recordsets are used for
      //the superuser, and "user_" recordsets are
      //used for the other users 
      $username = WebApp::getSVar("username");
      if ($username=="superuser")
        {
          WebApp::addVar("offices", "su_offices");
          WebApp::addVar("departments", "su_departments");
          WebApp::addVar("office_departments", "su_office_departments");
          WebApp::addVar("accessRights", "su_accessRights");
          WebApp::addVar("selectedUsers", "su_selectedUsers");
        }
      else
        {
          WebApp::addVar("offices", "user_offices");
          WebApp::addVar("departments", "user_departments");
          WebApp::addVar("office_departments", "user_office_departments");
          WebApp::addVar("accessRights", "user_accessRights");
          WebApp::addVar("selectedUsers", "user_selectedUsers");
        }    
    }
}
?>