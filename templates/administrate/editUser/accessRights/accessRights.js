// -*-C-*-
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//access right constants
var ACCR_VIEW = 1;
var ACCR_EDIT = 2;
var ACCR_FINANC = 3;
var ACCR_SCHEDULE = 4;
var ACCR_ADMIN = 5;

//arrays that keep the egzisting, added and removed access rights
var egzistingAccRights = new Array();
var addedAccRights     = new Array();
var removedAccRights   = new Array();

function setAccRight(office, dept, accr)
     //check the checkbox corresponding 
     //to this department and this access right
     //and also add it to the array 'egzistingAccRights'
{
  var form = document.accessRights; 

  //alert("setAccRight("+office+","+dept+","+accr+")");

  if (accr != 0)
    {
      //check the access right checkbox
      chbox = eval("form.accRight_"+dept+"["+(accr-1)+"]");
      chbox.checked = 1;
    }
  else
    {
      //check the department checkbox
      chbox = eval("form.dept_"+dept);
      chbox.checked = 1;  
    }

  //add it to the egzisting access rights
  size = egzistingAccRights.length;
  egzistingAccRights[size] = office + "-" + dept + "-" + accr;
}

function addAccRight(office, dept, accr)
     //saves this access right in the array 'addedAccRights';
     //make sure that 'added' and 'egzisting' are disjoint
     //and also 'added' and 'removed' are disjoint
{
  var size;
  var pos;

  //add it to the array 'addedAccRights' only
  //if it is not in the array 'egzistingAccRights'
  pos = findAccInArray(office, dept, accr, egzistingAccRights);
  if (pos==-1)
    {
      size = addedAccRights.length;
      addedAccRights[size] = office+"-"+dept+"-"+accr;
    }

  //remove it from the array 'removedAccRights' 
  //if it exists there
  pos = findAccInArray(office, dept, accr, removedAccRights);
  if (pos != -1)   removedAccRights.splice(pos,1);
}

function rmAccRight(office, dept, accr)
     //saves this access right in the array 'removedAccRights';
     //make sure that 'removed' and 'added' are disjoint and
     //that 'removed' is a subset of 'egzisting'
{
  var size;
  var pos;

  //add it to the array 'removedAccRights' only
  //if it egzists in the array 'egzistingAccRights'
  pos = findAccInArray(office, dept, accr, egzistingAccRights);
  if (pos != -1)
    {
      size = removedAccRights.length;
      removedAccRights[size] = office+"-"+dept+"-"+accr;
    }

  //remove it from the array 'addedAccRights', 
  //if it exists there
  pos = findAccInArray(office, dept, accr, addedAccRights);
  if (pos != -1)   addedAccRights.splice(pos,1);
}

function findAccInArray(office, dept, accr, arr)
     //returns the position of this access right 
     //in the given array, or -1 if not found
{
  var i;
  var str = office + "-" + dept + "-" + accr;
  for (i=0; i < arr.length; i++)
    if (arr[i]==str)
      return i; //found
  //not found
  return -1;
}

function getAccRightChanges()
     //returns a string containing the values
     //of the 'added' and 'removed' arrays
     //(this string will be sent to the server 
     //for updating the database)
{ 
  var str;

  //alert("egzisting: "+egzistingAccRights.toString());
  //alert("added: " + addedAccRights.toString());
  //alert("removed: " + removedAccRights.toString());

  str = "remove(" + removedAccRights.toString() + ")"
    + " add(" + addedAccRights.toString() + ")";

  return str;
}

function onClickDept(office, dept)
     //called when a department checkbox is clicked
{
  var form = document.accessRights;
  var chbox = eval("form.dept_"+dept);

  if (chbox.checked)
    {
      addAccRight(office, dept, 0);
    }
  else
    {
      rmAccRight(office, dept, 0);
      //also remove any given accesses for this department
      arrChbox = eval("form.accRight_"+dept);
      for (i=0; i < arrChbox.length; i++)
        {
          chb = arrChbox[i];
          if (chb.checked)  chb.click();
        }
    }
}

function onClickAccRight(office, dept, accr)
{
  var form = document.accessRights;
  var chbox = eval("form.accRight_"+dept+"["+(accr-1)+"]");

  var form1 = document.editUser;
  var idx1 = form1.office.selectedIndex;
  var selectedOffice = form1.office.options[idx1].value;
  var admin_id = document.editUser.admin.value;
  var user_id = session.getVar("editUser->user");

  if (user_id==admin_id && accr==ACCR_ADMIN)
    {
      //the administrator cannot change his admin rights
      alert("You cannot change your admin rights.");
      chbox.checked = !chbox.checked;
      return;
    }
  
  if (chbox.checked)
    {
      if (office!=selectedOffice
          && (accr==ACCR_EDIT || accr==ACCR_FINANC || accr==ACCR_ADMIN))
        {
          alert("You cannot give this access right outside of user's office,\n"
                +"or you have not selected an office for this user.");
          chbox.checked = false;
        }
      else
        {
          addAccRight(office, dept, accr);
          //make sure that the department is checked as well
          chb = eval("form.dept_"+dept);
          if (!chb.checked)  chb.click();
        }
    }
  else
    {
      rmAccRight(office, dept, accr);
    }
}
