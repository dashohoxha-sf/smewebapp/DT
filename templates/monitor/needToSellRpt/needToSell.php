<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class needToSell extends WebObject
{
  //the parameters of the report
  var $curr_month;
  var $curr_year;
  var $office;
  var $department;

  var $report_date;
  var $rs_fy;   //fiscal year RS

  function needToSell()
    {
      $curr_month = WebApp::getSVar("time_frame->currMonth");
      $this->curr_month = mon2int($curr_month);
      $this->curr_year = WebApp::getSVar("time_frame->currYear");

      //ensure single selection
      global $webPage;
      $select_by_dept = $webPage->getObject("select_by_dept");
      $select_by_dept->ensure_single_selection();

      $dept = WebApp::getSVar("select_by_dept->values");
      if ($dept<>"")
        {
          list($off_id,$dept_id) = explode("_", $dept);
          $this->office = $off_id;
          $this->department = $dept_id;
        }
      else
        {
          $this->department = WebApp::getSVar("u_dept");
          $this->office =  WebApp::getSVar("u_office");
        }

      //get 01 of the next to $curr_month and set this as the report date
      $t = mktime(0,0,0, $this->curr_month + 1, 1, $this->curr_year);
      $this->report_date = date("Y-m-d", $t);

      $this->rs_fy = $this->getFiscalYearRS();
    }

  function onRender()
    {
      $this->set_restOfMonths();
      $this->add_tpl_vars();
      $this->fill_recordsets();
    }

  function getFiscalYearRS()
    //returns a RS of months from Jun to May
    {
      $curr_month = $this->curr_month;
      $curr_year = $this->curr_year;

      if ($curr_month < 6)
        {
          $fiscal_year = $curr_year;
        }
      else
        {
          $fiscal_year = $curr_year + 1;
        }

      $rs = new EditableRS;
      $rs->Open();
      for ($i=6; $i <= 12; $i++)
        {
          $rec = array(
                       "m_id" => $i,
                       "Mon" => int2mon($i),
                       "year" => $fiscal_year - 1
                       );
          $rs->addRec($rec);
        }
      for ($i=1; $i < 6; $i++)
        {
          $rec = array(
                       "m_id" => $i,
                       "Mon" => int2mon($i),
                       "year" => $fiscal_year
                       );
          $rs->addRec($rec);
        }

      return $rs;
    }

  function getCurrMonthPos()
    //get the position of the current month
    //in the fiscal year RS ($this->rs_fy)
    {
      $rs = &$this->rs_fy;
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          if ( ($rs->Field("m_id")==$this->curr_month) 
               and ($rs->Field("year")==$this->curr_year) )
            {
              break;
            }
          $rs->MoveNext();
        }

      return $rs->pos;
    }

  function set_restOfMonths()
    //fills programatically the recordset "restOfMonths"
    {
      $rs = new EditableRS;
      $rs->Open();
      $pos = $this->getCurrMonthPos();
      $rs = $this->rs_fy->slice($pos+1);

      //add the recordset to the $webPage
      global $webPage;
      $rs->ID = "restOfMonths_1";
      $webPage->addRecordset($rs);

      //add the record Total
      $rec = array("m_id"=>"0", "Mon"=>"Total");
      $rs->MoveLast();
      $rs->addRec($rec);
      $rs->MoveFirst();

      //add the recordset to the $webPage
      $rs->ID = "restOfMonths";
      $webPage->addRecordset($rs);
    }

  function add_tpl_vars()
    {
      $curr_month = $this->curr_month;
      $curr_year = $this->curr_year;

      //add {{department}}
      $dept_name = WebApp::getSVar("select_by_dept->names");
      WebApp::addVar("department", $dept_name);
      
      //add {{nr_depts}}
      $rs = WebApp::openRS("dept_list", array("office"=>$this->office));
      WebApp::addVar("nr_depts", $rs->count);
      
      //add {{curr_Mon}}
      WebApp::addVar("curr_Mon", int2mon($curr_month));

      //add {{prev_months}}
      if ($curr_month==1)
        {
          $prev_month = 12;
          $prev_year = $curr_year - 1;
        }
      else
        {
          $prev_month = $curr_month - 1;
          $prev_year = $curr_year;
        }
      $prev_months = "Jun - ".int2mon($prev_month)." $prev_year";
      WebApp::addVar("prev_months", $prev_months);

      ////////////////////////////////////////////////
      //add {{NSR_YTD}} and {{curr_NSR}}
      //
      $params = array(
                      "dept_id" => $this->department,
                      "first_year" => $curr_year - 1,
                      "last_year" => $curr_year
                      );
      if ($curr_month >= 6)
        {
          $params["first_year"] = $curr_year;
          $params["last_year"] = $curr_year + 1;
        }
      //open the nsr recordset
      $rs_nsr = WebApp::openRS("nsr", $params);

      //find the current month
      $rs_nsr->MoveLast();
      while (!$rs_nsr->BOF())
        {
          $rec = $rs_nsr->Fields();
          if ($rec["month"]==$curr_month and $rec["year"]==$curr_year)  break;
          $rs_nsr->MovePrev();
        }
      //add {{curr_NSR}}
      WebApp::addVar("curr_NSR", $rs_nsr->Field("nsr"));

      //sum up the previous nsr's
      $rs_nsr->MovePrev();
      while (!$rs_nsr->BOF())
        {
          $sum += $rs_nsr->Field("nsr");
          $rs_nsr->MovePrev();
        }
      //add {{NSR_YTD}}
      WebApp::addVar("NSR_YTD", $sum);
    }

  function fill_recordsets()
    //fills the recordsets that are used in the template
    {
      //create the new recordsets and open them
      $proj_contracted  = new EditableRS("proj_contracted");
      $proj_proposals   = new EditableRS("proj_proposals");
      $proj_opportunities = new EditableRS("proj_opportunities");

      $contracted       = new EditableRS("contracted");
      $opportunities    = new EditableRS("opportunities");
      $prop_80          = new EditableRS("prop_80");
      $prop_65          = new EditableRS("prop_65");
      $prop_25          = new EditableRS("prop_25");

      $this->get_proj_recordsets($proj_contracted,
                                 $proj_proposals,
                                 $proj_opportunities);

      $this->get_contracted($contracted, $proj_contracted);
      $this->get_opportunities($opportunities, $proj_opportunities);
      $this->get_proposals($prop_80, $prop_65, $prop_25, $proj_proposals);


      //calculate the derived recordsets
      $total_delivered = $this->total_delivered($contracted);
      $budgeted_NSR = $this->budgeted_NSR();
      $gap1 = $this->gap($budgeted_NSR, $total_delivered);
      $prop_total = $this->sum3($prop_80, $prop_65, $prop_25);
      $w_prop_80 = $this->weight(80, $prop_80);
      $w_prop_65 = $this->weight(65, $prop_65);
      $w_prop_25 = $this->weight(25, $prop_25);
      $w_prop_total = $this->sum3($w_prop_80, $w_prop_65, $w_prop_25);
      $gap2 = $this->gap($gap1, $w_prop_total, "adjust");
      $w_opportunities = $this->weight(10, $opportunities);
      $gap3 = $this->gap($gap2, $w_opportunities, "adjust");

      //add the totals to the end of each recordset
      $this->add_total($contracted);
      $this->add_total($total_delivered);
      $this->add_total($prop_80);
      $this->add_total($prop_65);
      $this->add_total($prop_25);
      $this->add_total($opportunities);
      $this->add_total($budgeted_NSR);
      $this->add_total($gap1);
      $this->add_total($prop_total);
      $this->add_total($w_prop_80);
      $this->add_total($w_prop_65);
      $this->add_total($w_prop_25);
      $this->add_total($w_prop_total);
      $this->add_total($gap2);
      $this->add_total($w_opportunities);
      $this->add_total($gap3);

      //round to one digit after the decimal point
      $contracted->apply("needToSell_round");
      $total_delivered->apply("needToSell_round");
      $prop_80->apply("needToSell_round");
      $prop_65->apply("needToSell_round");
      $prop_25->apply("needToSell_round");
      $opportunities->apply("needToSell_round");
      $budgeted_NSR->apply("needToSell_round");
      $gap1->apply("needToSell_round");
      $prop_total->apply("needToSell_round");
      $w_prop_80->apply("needToSell_round");
      $w_prop_65->apply("needToSell_round");
      $w_prop_25->apply("needToSell_round");
      $w_prop_total->apply("needToSell_round");
      $gap2->apply("needToSell_round");
      $w_opportunities->apply("needToSell_round");
      $gap3->apply("needToSell_round");

      $success_factor = $this->success_factor($w_prop_total,
                                              $prop_total);

      //filter $proj_contracted, $proj_proposal and $proj_opportunities
      $d_id = $this->department;
      $proj_contracted = $proj_contracted->filter("dept_id='$d_id'");
      $proj_proposals = $proj_proposals->filter("dept_id='$d_id'");
      $proj_opportunities = $proj_opportunities->filter("dept_id='$d_id'");
      //calculate the field 'Total' of $proj_contracted,
      //$proj_proposals and $proj_opportunities
      $this->set_fld_total($proj_contracted);
      $this->set_fld_total($proj_proposals);
      $this->set_fld_total($proj_opportunities);
      //get the 'totals' recordsets
      $totals_contracted = $this->get_totals($proj_contracted);
      $totals_proposals = $this->get_totals($proj_proposals);
      $totals_opportunities = $this->get_totals($proj_opportunities);

      //set recordset id's
      $proj_contracted->ID = "proj_contracted";
      $proj_proposals->ID = "proj_proposals";
      $proj_opportunities->ID = "proj_opportunities";
      $total_delivered->ID = "total_delivered";
      $budgeted_NSR->ID = "budgeted_NSR";
      $gap1->ID = "gap1";
      $prop_total->ID = "prop_total";
      $w_prop_80->ID = "w_prop_80";
      $w_prop_65->ID = "w_prop_65";
      $w_prop_25->ID = "w_prop_25";
      $w_prop_total->ID = "w_prop_total";
      $gap2->ID = "gap2";
      $w_opportunities->ID = "w_opportunities";
      $gap3->ID = "gap3";
      $success_factor->ID = "success_factor";
      $totals_contracted->ID = "totals_contracted";
      $totals_proposals->ID = "totals_proposals";
      $totals_opportunities->ID = "totals_opportunities";
      //add the recordsets to the $webPage
      global $webPage;
      $webPage->addRecordset($contracted);
      $webPage->addRecordset($total_delivered);
      $webPage->addRecordset($prop_80);
      $webPage->addRecordset($prop_65);
      $webPage->addRecordset($prop_25);
      $webPage->addRecordset($opportunities);
      $webPage->addRecordset($budgeted_NSR);
      $webPage->addRecordset($gap1);
      $webPage->addRecordset($prop_total);
      $webPage->addRecordset($w_prop_80);
      $webPage->addRecordset($w_prop_65);
      $webPage->addRecordset($w_prop_25);
      $webPage->addRecordset($w_prop_total);
      $webPage->addRecordset($gap2);
      $webPage->addRecordset($w_opportunities);
      $webPage->addRecordset($gap3);
      $webPage->addRecordset($success_factor);
      $webPage->addRecordset($proj_contracted);
      $webPage->addRecordset($proj_proposals);
      $webPage->addRecordset($proj_opportunities);
      $webPage->addRecordset($totals_contracted);
      $webPage->addRecordset($totals_proposals);
      $webPage->addRecordset($totals_opportunities);
    }

  function get_proj_recordsets(&$proj_contracted,
                               &$proj_proposals,
                               &$proj_opportunities)
    {
      //get all active projects in this office
      $params["off_id"] = $this->office;
      $params["report_date"] = $this->report_date;
      $projects = WebApp::openRS("projects", $params);

      //add some additional fields for the month and dept distribs
      $this->add_dept_cols($projects);
      $this->add_month_cols($projects);

      //extract from $projects the recordsets for each status 
      //and fillin the month and dept distribs for them
      $this->get_proj_rs($proj_contracted, $projects, array(CONTRACTED));
      $this->get_proj_rs($proj_proposals, $projects, array(PROPOSAL));
      $this->get_proj_rs($proj_opportunities, $projects, array(QUALIFIED, OPPORTUNITY));
    }

  function add_dept_cols(&$rs)
    //completes the given recordset with additional fields
    //for the departments of this office, with default values
    {
      $params["office"] = $this->office;
      $depts = WebApp::openRS("dept_list", $params);
      while (!$depts->EOF())
        {
          $dept_id = $depts->Field("dept_id");
          $rs->addCol("dept_".$dept_id, "0");
          $depts->MoveNext();
        }
    }

  function add_month_cols(&$rs)
    //completes the given recordset with additional fields
    //for the rest of the months, with default values
    {
      global $webPage;
      $months = $webPage->getRecordset("restOfMonths");
      $months->MoveFirst();
      while (!$months->EOF())
        {
          $mon = $months->Field("Mon");
          $rs->addCol($mon, "-");
          $months->MoveNext();
        }
    }

  function get_proj_rs(&$rs, &$projects, $arr_status)
    {
      for ($i=0; $i < sizeof($arr_status); $i++)
        {
          $status_id = $arr_status[$i];
          $filtered_rs = $projects->filter("status_id='$status_id'"); 
          $rs->append($filtered_rs);
        }
      $proj_list = $this->get_projId_list($rs);
      if ($proj_list=="")  return;
      $this->fill_month_distribs($rs, $proj_list, $arr_status);
      $this->fill_dept_distribs($rs, $proj_list, $arr_status);

      if (TEST)
        {
          $msg = "All the projects of this office "
            . "with status: " . implode(",", $arr_status);
          //WebApp::debug_msg($rs->toHtmlTable(), $msg);
        }
    }

  function get_projId_list(&$rs)
    //return a list of proj_id's in $rs
    {
      $arr = array();
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $arr[] = $rs->Field("proj_id");
          $rs->MoveNext();
        }
      return implode(",", $arr);
    }

  function fill_month_distribs(&$rs, $projId_list, $arr_status)
    {
      //get month distribs for these projects
      $params["projId_list"] = $projId_list;
      $params["status_list"] = implode(",", $arr_status);
      $distribs = WebApp::openRS("getMonthDistribs", $params);

      //put them to the corresponding projects,
      //assumes that $rs and $distribs are ordered 
      //in asscending order by proj_id
      $rs->MoveFirst();
      $distribs->MoveFirst();
      while (!$rs->EOF() and !$distribs->EOF())
        {
          $id1 = $rs->Field("proj_id");
          while ($id1 == $distribs->Field("proj_id")) 
            {
              $mon = int2mon($distribs->Field("month"));
              $amount = $distribs->Field("amount");
              if ($rs->Field($mon)<>UNDEFINED)
                {
                  $rs->setFld($mon, $amount);
                }
              $distribs->MoveNext();
            }
          $id2 = $distribs->Field("proj_id");
          if ($id1 < $id2)  $rs->MoveNext();
          if ($id1 > $id2)  $distribs->MoveNext();
        }
    }

  function fill_dept_distribs(&$rs, $projId_list, $arr_status)
    {
      //get dept distribs for these projects
      $params["projId_list"] = $projId_list;
      $params["status_list"] = implode(",", $arr_status);
      $distribs = WebApp::openRS("getDeptDistribs", $params);

      //put them to the corresponding projects,
      //assumes that $rs and $distribs are ordered
      //in asscending order by proj_id
      $rs->MoveFirst();
      $distribs->MoveFirst();
      while (!$rs->EOF() and !$distribs->EOF())
        {
          $id1 = $rs->Field("proj_id");
          while ($id1 == $distribs->Field("proj_id")) 
            {
              $dept_id = $distribs->Field("dept");
              $percent = $distribs->Field("percent");
              $rs->setFld("dept_".$dept_id, $percent);
              $distribs->MoveNext();
            }
          $id2 = $distribs->Field("proj_id");
          if ($id1 < $id2)  $rs->MoveNext();
          if ($id1 > $id2)  $distribs->MoveNext();
        }
    }

  function get_contracted(&$contracted, &$proj_contracted)
    {
      $d_id = $this->department;

      //get the month recordset
      $this->getCurrMonthPos();
      $month_list = &$this->rs_fy;
      $month_list->MoveNext();

      //for each month, sum up all the amounts in $proj_contracted
      while (!$month_list->EOF())
        {
          $m_id = $month_list->Field("m_id");
          $mon = int2mon($m_id);

          //sum up all the amounts for this month,
          //weighted by the percentage for this department
          $sum = 0.0;
          $proj_contracted->MoveFirst();
          while (!$proj_contracted->EOF())
            {
              $amount = $proj_contracted->Field($mon);
              $percent = $proj_contracted->Field("dept_".$d_id);
              $sum += $amount * $percent / 100;
              $proj_contracted->MoveNext();
            }

          //add a new record for this month to $contracted
          $new_rec = array("m_id"=>$m_id, "amount"=>$sum, "class"=>"amount");
          $contracted->addRec($new_rec);

          $month_list->MoveNext();
        }
    }

  function get_opportunities(&$opportunities, &$proj_opportunities)
    {
      $d_id = $this->department;

      //get the month recordset
      $this->getCurrMonthPos();
      $month_list = &$this->rs_fy;
      $month_list->MoveNext();

      //for each month, sum up all the amounts in $proj_opportunities
      while (!$month_list->EOF())
        {
          $m_id = $month_list->Field("m_id");
          $mon = int2mon($m_id);

          //sum up all the amounts for this month,
          //weighted by the percentage for this department
          $sum = 0.0;
          $proj_opportunities->MoveFirst();
          while (!$proj_opportunities->EOF())
            {
              $amount = $proj_opportunities->Field($mon);
              $percent = $proj_opportunities->Field("dept_".$d_id);
              $sum += $amount * $percent / 100;
              $proj_opportunities->MoveNext();
            }

          //add a new record for this month to $opportunities
          $new_rec = array("m_id"=>$m_id, "amount"=>$sum, "class"=>"amount");
          $opportunities->addRec($new_rec);

          $month_list->MoveNext();
        }
    }

  function get_proposals(&$prop_80, &$prop_65, &$prop_25, &$proj_proposals)
    {
      $d_id = $this->department;

      //get the month recordset
      $this->getCurrMonthPos();
      $month_list = &$this->rs_fy;
      $month_list->MoveNext();

      //for each month, sum up all the amounts in $proj_proposals
      while (!$month_list->EOF())
        {
          $m_id = $month_list->Field("m_id");
          $mon = int2mon($m_id);

          //add a new record for this month to $proposals
          $new_rec = array("m_id"=>$m_id, "amount"=>"0", "class"=>"amount");
          $prop_80->addRec($new_rec);
          $prop_65->addRec($new_rec);
          $prop_25->addRec($new_rec);

          //sum up all the amounts for this month,
          //weighted by the percentage for this department
          $sum_80 = $sum_65 = $sum_25 = 0.0;
          $proj_proposals->MoveFirst();
          while (!$proj_proposals->EOF())
            {
              $amount = $proj_proposals->Field($mon);
              $percent = $proj_proposals->Field("dept_".$d_id);
              $prob = $proj_proposals->Field("prob");

              $w_amount = $amount * $percent / 100;
              if      ($prob >= 80)  $sum_80 += $w_amount;
              else if ($prob < 50)   $sum_25 += $w_amount;
              else                   $sum_65 += $w_amount;

              $proj_proposals->MoveNext();
            }
          $prop_80->setFld("amount", $sum_80);
          $prop_65->setFld("amount", $sum_65);
          $prop_25->setFld("amount", $sum_25);

          $month_list->MoveNext();
        }
    }

  function set_fld_total(&$rs)
    //calcultes the field total for each row of the
    //given $rs
    {
      global $webPage;
      $months = $webPage->getRecordset("restOfMonths");

      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $rec = $rs->Fields();
          $total = 0;
          $months->MoveFirst();
          while (!$months->EOF())
            {
              $mon = $months->Field("Mon");
              $total += $rec[$mon];
              $months->MoveNext();
            }
          if ($total<>0)   $rs->setFld("Total", $total);
          $rs->MoveNext();
        }
    }

  function get_totals(&$rs)
    //returns a recordset with the totals for each month column
    {
      global $webPage;
      $months = $webPage->getRecordset("restOfMonths");

      $rs_totals = new EditableRS;
      $rs_totals->Open();
      $months->MoveFirst();
      while (!$months->EOF())
        {
          $mon = $months->Field("Mon");
          $total = 0;
          $rs->MoveFirst();
          while (!$rs->EOF())
            {
              $total += $rs->Field($mon);
              $rs->MoveNext();
            }
          $rs_totals->addRec(array("total"=>$total));
          $months->MoveNext();
        }
      $rs_totals->MoveFirst();
      return $rs_totals;
    }

  function total_delivered(&$contracted)
    //creates and returns the $total_delivered RS
    {
      $rs = new EditableRS;
      $rs->Open();

      $nsr_ytd = WebApp::getVar("NSR_YTD");
      $rec = array("amount"=>$nsr_ytd, "class"=>"total");
      $rs->addRec($rec);

      $curr_nsr = WebApp::getVar("curr_NSR");
      $rec = array("amount"=>$curr_nsr, "class"=>"total");
      $rs->addRec($rec);

      $contracted->MoveFirst();
      while (!$contracted->EOF())
        {
          $rec["amount"] = $contracted->Field("amount");
          $rec["class"] = "total";
          $rs->addRec($rec);
          $contracted->MoveNext();
        }

      $rs->MoveFirst();
      return $rs;
    }

  function budgeted_NSR()
    //get and return the budgeted_NSR recordset
    {
      $curr_month = $this->curr_month;
      $curr_year = $this->curr_year;
      $dept_id = $this->department;

      $params = array(
                      "dept_id" => $dept_id,
                      "first_year" => $curr_year - 1,
                      "last_year" => $curr_year
                      );
      if ($curr_month >= 6)
        {
          $params["first_year"] = $curr_year;
          $params["last_year"] = $curr_year + 1;
        } 
      $rs = WebApp::openRS("nsr_budget", $params);

      //get the sum of the previous months
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $rec = $rs->Fields();
          if ($rec["month"]==$curr_month and $rec["year"]==$curr_year)   break;
          $sum += $rec["nsr"];
          $rs->MoveNext();
        }

      $budgeted_NSR = new EditableRS;
      $budgeted_NSR->Open();
      $rec = array("amount"=>$sum, "class"=>"amount");
      $budgeted_NSR->addRec($rec);
      while (!$rs->EOF())
        {
          $rec["amount"] = $rs->Field("nsr");
          $rec["class"] = "amount";
          $budgeted_NSR->addRec($rec);
          $rs->MoveNext();
        }

      $budgeted_NSR->MoveFirst();
      return $budgeted_NSR;
    }

  function gap(&$rs1, &$rs2, $adjust =false)
    {
      $rs_gap = new EditableRS;
      $rs_gap->Open();

      $rs1->MoveFirst();
      $rs2->MoveFirst();
      if ($adjust)
        {
          $amount = $rs1->Field("amount");
          $rs_gap->addRec(array("amount"=>"$amount", "class"=>"strong_total"));
          $rs1->MoveNext();
          $amount = $rs1->Field("amount");
          $rs_gap->addRec(array("amount"=>"$amount", "class"=>"strong_total"));
          $rs1->MoveNext();
        }

      while (!$rs1->EOF())
        {
          $amount = $rs1->Field("amount") - $rs2->Field("amount");
          $rs_gap->addRec(array("amount"=>"$amount", "class"=>"strong_total"));
          //move next
          $rs1->MoveNext();
          $rs2->MoveNext();
        }
      $rs_gap->MoveFirst();
      return $rs_gap;
    }

  function weight($percent, &$rs)
    {
      $rs_w = new EditableRS;
      $rs_w->Open();
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $amount = $rs->Field("amount") * $percent;
          $amount = round($amount/100);
          $rs_w->addRec(array("amount"=>"$amount", "class"=>"amount"));
          $rs->MoveNext();
        }
      $rs_w->MoveFirst();
      return $rs_w;
    }

  function sum3(&$rs1, &$rs2, &$rs3)
    {
      $rs_total = new EditableRS;
      $rs_total->Open();
      $rs1->MoveFirst();
      $rs2->MoveFirst();
      $rs3->MoveFirst();
      while (!$rs1->EOF())
        {
          $amount = $rs1->Field("amount")
	    + $rs2->Field("amount")
	    + $rs3->Field("amount");
          $rs_total->addRec(array("amount"=>"$amount", "class"=>"strong_total"));
          //move next
          $rs1->MoveNext();
          $rs2->MoveNext();
          $rs3->MoveNext();
        }
      $rs_total->MoveFirst();
      return $rs_total;
    }

  function success_factor(&$w_prop_total, &$prop_total)
    {
      $success_factor = new EditableRS;
      $success_factor->Open();
      $w_prop_total->MoveFirst();
      $prop_total->MoveFirst();
      while (!$w_prop_total->EOF())
        {
          $amount1 = $w_prop_total->Field("amount");
          $amount2 = $prop_total->Field("amount");
          if ($amount2==0)
            {
              $amount = "-";
            }
          else
            {
              $amount = floor($amount1/$amount2 * 100);
              $amount .= "%";
            }
          $success_factor->addRec(array("amount"=>"$amount", "class"=>"amount"));
          //move next
          $w_prop_total->MoveNext();
          $prop_total->MoveNext();
        }
      //$success_factor->setFld("class", "total");
      $success_factor->MoveFirst();
      return $success_factor;
    }

  function add_total(&$rs)
    //calculates the total of the amounts 
    //and adds a new record with the total
    {
      $total = 0;
      $rs->MoveFirst();
      $class = $rs->Field("class");
      while (!$rs->EOF())
        {
          $total += $rs->Field("amount");
          $rs->MoveNext();
        }
      $total_class = ($class=="amount" ? "total" : "strong_total");
      $rec = array("amount"=>"$total", "class"=>$total_class);
      $rs->addRec($rec);
    }
}

/** round to one digit after the decimal point */
function needToSell_round(&$rec)
{
  $amount = $rec["amount"];
  $amount = (round($amount * 10.0)) / 10;
  $rec["amount"] = $amount;
}

?>