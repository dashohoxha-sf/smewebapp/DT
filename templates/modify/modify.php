<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class modify extends WebObject
{
  function init()
    {
      $this->addSVar("selectFile", "{{TPL_PATH}}selectProjects/selectProjects.html");
      //file: 'projectList/projectList.html' 
      //   or 'editProject/edit.html'
      //   or 'editNSR/editNSR.html'
      $this->addSVar("editFile", "projectList/projectList.html");
      $this->addSVar("submodule", "modify", "DB");
    }

  function on_edit($event_args)
    {
      $this->setSVar("selectFile", "{{TPL_PATH}}selectProjects/selectProjects.html");
      $this->setSVar("editFile", "editProject/edit.html");

      extract($event_args);
      WebApp::setSVar("editProject->mode", "edit");
      WebApp::setSVar("editProject->proj_id", $proj_id);
      WebApp::setSVar("editProject->status_id", $status_id);
    }

  function on_editNSR($event_args)
    {
      $this->setSVar("selectFile", "{{TPL_PATH}}modify/editNSR/select.html");
      $this->setSVar("editFile", "editNSR/editNSR.html");
      $this->setSVar("submodule", "financ");
    }

  function on_list($event_args)
    {
      $this->setSVar("selectFile", "{{TPL_PATH}}selectProjects/selectProjects.html");
      $this->setSVar("submodule", "modify");
      $file = $this->getSVar("editFile");
      if ($file=="projectList/projectList.html")
        {
          WebApp::setSVar("projects->current_page", "1");
        }
      else
        {
          $this->setSVar("editFile", "projectList/projectList.html");
        }
    }

  function on_add($event_args)
    {
      $this->setSVar("selectFile", "{{TPL_PATH}}selectProjects/selectProjects.html");
      $this->setSVar("editFile", "editProject/editProject.html");

      WebApp::setSVar("editProject->mode", "add");
      WebApp::setSVar("editProject->proj_id", UNDEFINED);
      WebApp::setSVar("editProject->status_id", OPPORTUNITY);
    }
}
?>