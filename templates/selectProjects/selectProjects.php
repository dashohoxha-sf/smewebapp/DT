<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class selectProjects extends WebObject
{
  function init()
    {
      $this->addSVar("condition", $this->timeFrameCondition());
    }

  function onRender()
    {
      $this->setSVar("condition", $this->buildQueryCondition());
    }

  function buildQueryCondition()
    {
      //build status condition
      $status = WebApp::getSVar("select_by_status->values");
      $arr_stat_condition = array();
      $arr_status = explode(";",$status);
      while ( list($i,$st_id) = each($arr_status) )
        {
          if ($st_id<>"") 
            $arr_stat_condition[] = 'projects.status_id='.$st_id;
        }
      $stat_condition = implode(" OR ", $arr_stat_condition);
             
      //build department condition
      $office_depts = WebApp::getSVar("select_by_dept->values");
      $arr_depts = array();
      $arr_office_depts = explode(";",$office_depts);
      while ( list($i,$item) = each($arr_office_depts) )
        {
          list($off_id,$dept_id) = explode("_", $item);
          if ($dept_id<>"")  $arr_depts[] = $dept_id;
        }
      if (sizeof($arr_depts) > 0) 
        $dept_condition = "projects.dept_id IN ("
	  . implode(',', $arr_depts) . ")"; 

      //build select condition
      $condition = $this->timeFrameCondition();
      if ($stat_condition<>"")  $condition .= " AND ($stat_condition)";
      if ($dept_condition<>"")  $condition .= " AND ($dept_condition)";

      return $condition;
    }

  function timeFrameCondition()
    //returns the condition for selecting the projects 
    //according to the timeframe
    {
      //get the start and end time of the timeframe
      $currMonthCheck = WebApp::getSVar("select_by_time->currMonthCheck");
      if ($currMonthCheck=="checked")
        {
          $fromMonth = get_curr_date("M");  //current month
          $toMonth = get_curr_date("M");
          $fromYear = get_curr_date("Y");   //current year
          $toYear = get_curr_date("Y");
        }
      else
        {
          $fromMonth = WebApp::getSVar("select_by_time->fromMonth");
          $toMonth   = WebApp::getSVar("select_by_time->toMonth");
          $fromYear  = WebApp::getSVar("select_by_time->fromYear");
          $toYear    = WebApp::getSVar("select_by_time->toYear");
        }

      //format the starting and ending date of the timeframe
      //in the format required by the database: yyyy-mm-dd;
      //start of the timeframe is 01 of the start month,
      //end of the timeframe is 01 of the next to end month
      $t1 = strtotime("01 $fromMonth $fromYear");
      $t1 = date("Y-m-d", $t1);
      $t2 = strtotime("01 $toMonth $toYear");
      $t2 = mktime(0,0,0, date("m",$t2)+1, 1, date("Y",$t2));
      $t2   = date("Y-m-d", $t2);

      //build and return the condition that selects the projects
      //that have a time intersection with the timeframe
      $condition = '(NOT (projects.register_date >= "'.$t2.'"
                         OR (projects.register_date < "'.$t1.'"
                             AND close_date IS NOT NULL
                             AND close_date < "'.$t1.'")
                         ))';
      return $condition;
    }
}
?>