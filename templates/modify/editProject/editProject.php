<?php
/*
This  file is  part of  DT.   DT is  web application  written for  the
Albanian branch of Deloitte & Touche company.

Copyright (C) 2002 Dashamir Hoxha, dashohoxha@users.sf.net

DT is  free software; you can  redistribute it and/or  modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

DT is distributed in the hope  that it will be useful, but WITHOUT ANY
WARRANTY;  without even  the  implied warranty  of MERCHANTABILITY  or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DT; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

class editProject extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      //status_id: CONTRACTED, QUALIFIED, PROPOSAL, OPPORTUNITY
      $this->addSVar("status_id", OPPORTUNITY); 
      $this->addSVar("proj_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function onParse()
    {
      $status_id = $this->getSVar("status_id");
      switch ($status_id)
        {
        case CONTRACTED:
          $editProjType = "editContracted";
          break;
        case PROPOSAL:
          $editProjType = "editProposal";
          break;
        default:
        case QUALIFIED:
        case OPPORTUNITY:
          $editProjType = "editOpportunity";
          break;
        }
      //this variable keeps which specific template
      //will be loaded, according to the type of the project
      WebApp::addVar("editProjType", $editProjType);

      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getProject");
          $close_date = $rs->Field("close_date");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table projects
          $rs = WebApp::execQuery("SHOW FIELDS FROM projects");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
            }
          //add some default values
          WebApp::addVars(array(
                                "information_staff" => WebApp::getSVar("u_id"),
                                "information_date"  => get_curr_date("Y-m-d"),
                                "follow_up_person"  => WebApp::getSVar("u_id"),
                                "dept_id" => WebApp::getSVar("u_dept")
                                ));
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getProject");
          $fields = $rs->Fields();
          WebApp::addVars($fields);
        }

      $status_id = $this->getSVar("status_id");
      WebApp::addVar("checked_".$status_id, "checked");
      
      //add the title variable, which shows 
      //the editing state of the form
      $title = (($mode=="add") ? "Add" : "Edit");
      switch ($status_id)
        {
        case CONTRACTED:
          $title .= " Contracted Project";
          break;
        case PROPOSAL:
          $title .= " Project Proposal";
          break;
        case QUALIFIED:
        case OPPORTUNITY:
          $title .= " Project Opportunity";
          break;
        default:
          $title .= " Undefined Status";
          break;
        }
      WebApp::addVar("title", $title);

      //add the recordset listbox::client_status
      global $webPage;
      $rs = new EditableRS("listbox::client_status");
      $rs->Open();
      $rs->addRec(array("id"=>"CCJC", "label"=>"CCJC"));
      $rs->addRec(array("id"=>"OC",   "label"=>"OC"));
      $rs->addRec(array("id"=>"KT",   "label"=>"KT"));
      $rs->addRec(array("id"=>"OT",   "label"=>"OT"));
      $webPage->addRecordset($rs);

      //add the recordset listbox::information_source
      $rs = new EditableRS("listbox::information_source");
      $rs->Open();
      $rs->addRec(array("id"=>"Personal", "label"=>"Personal"));
      $rs->addRec(array("id"=>"DT Info",  "label"=>"DT Info"));
      $rs->addRec(array("id"=>"Internet", "label"=>"Internet"));
      $rs->addRec(array("id"=>"Client",   "label"=>"Client"));
      $webPage->addRecordset($rs);

      $this->add_listsize_rs();
    }

  function on_changeStatus($event_args)
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //change the state var status_id
          $status_id = $event_args["status_id"];
          $this->setSVar("status_id", $status_id);
          return;
        }
      //else mode=="edit"

      //close the current project
      $params["close_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("closeProject", $params);

      //get the current document (before changing the status_id)
      $rs = WebApp::openRS("getProject");
      $params = $rs->Fields();

      //change the state var status_id
      $prev_status_id = $this->getSVar("status_id");
      $status_id = $event_args["status_id"];
      $this->setSVar("status_id", $status_id);

      //create a new record for the project 
      switch ($status_id)
        {
        case CONTRACTED:
          $newRecord = "newContractedRecord";
          break;
        case PROPOSAL:
          $newRecord = "newProposalRecord";
          break;
        case QUALIFIED:
        case OPPORTUNITY:
          $newRecord = "newOpportunityRecord";
          break;
        }
      $params["status_id"] = $status_id;
      $params["current_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newProjectRecord", $params);
      WebApp::execDBCmd($newRecord, $params);

      $this->copy_related_data($prev_status_id);
    }

  function copy_related_data($prev_status_id)
    //copy the data of deptDistrib, timeDistrib, 
    //managers and partners to the new document,
    //when the status of the document changes
    {
      $proj_id = $this->getSVar("proj_id");
      $status_id = $this->getSVar("status_id");
      $prev_doc["proj_id"] = $proj_id;
      $prev_doc["status_id"] = $prev_status_id;

      //copy deptDistrib data
      $rs = WebApp::openRS("get_deptDistrib", $prev_doc);
      $values = array();
      while (!$rs->EOF())
        {
          $dept = $rs->Field("dept");
          $percent = $rs->Field("percent");
          $values[] = "($proj_id, $status_id, $dept, $percent)";
          $rs->MoveNext();
        }
      if (sizeof($values)<>0)
        {
          $param["values"] = implode(",", $values);
          WebApp::execDBCmd("ins_deptDistrib", $param);
        }

      //copy timeDistrib data
      $rs = WebApp::openRS("get_timeDistrib", $prev_doc);
      $values = array();
      while (!$rs->EOF())
        {
          $amount = $rs->Field("amount");
          $month = $rs->Field("month");
          $year = $rs->Field("year");
          $values[] = "($proj_id, $status_id, $amount, $month, $year)";
          $rs->MoveNext();
        }
      if (sizeof($values)<>0)
        {
          $param["values"] = implode(",", $values);
          WebApp::execDBCmd("ins_timeDistrib", $param);
        }

      //copy managers data
      $rs = WebApp::openRS("get_managers", $prev_doc);
      $values = array();
      while (!$rs->EOF())
        {
          $user_id = $rs->Field("user_id");
          $values[] = "($proj_id, $status_id, $user_id)";
          $rs->MoveNext();
        }
      if (sizeof($values)<>0)
        {
          $param["values"] = implode(",",$values);
          WebApp::execDBCmd("ins_managers", $param);
        }

      //copy partners data
      $rs = WebApp::openRS("get_partners", $prev_doc);
      $values = array();
      while (!$rs->EOF())
        {
          $user_id = $rs->Field("user_id");
          $values[] = "($proj_id, $status_id, $user_id)";
          $rs->MoveNext();
        }
      if (sizeof($values)<>0)
        {
          $param["values"] = implode(",",$values);
          WebApp::execDBCmd("ins_partners", $param);
        }
    }

  function on_close($event_args)
    {
      $params["close_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("closeProject", $params);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->add_new_project($event_args);
        }
      else //mode=="edit"
        {
          $this->update_project($event_args);
        }
    }

  function update_project($event_args)
    {
      WebApp::execDBCmd("updateProject", $event_args);

      //save the specific data according to the status of the project
      $status_id = $this->getSVar("status_id");
      switch ($status_id)
        {
        default:
        case CONTRACTED:
          $query = "updateContracted";
          break;
        case PROPOSAL:
          $query = "updateProposal";
          break;
        case QUALIFIED:
        case OPPORTUNITY:
          $query = "updateOpportunity";
          break;
        }
      WebApp::execDBCmd($query, $event_args);
    }

  function add_new_project($event_args)
    {
      //check that the name of the project is unique
      $rs = WebApp::openRS("getProjName", $event_args);
      if (!$rs->EOF())
        {
          $msg = "There is another project with the same name.\n"
            . "Please change the name and save again, or cancel.";
          WebApp::message($msg);
          return;
        }

      //find a proj_id for the new project
      $rs = WebApp::openRS("getLastProjId");
      $last_proj_id = $rs->Field("last_proj_id");
      if ($last_proj_id==UNDEFINED)
        {
          //first project
          $proj_id = 1;
        }
      else
        {
          $proj_id = $last_proj_id + 1;
        }

      //set the proj_id state of the webobj
      $this->setSVar("proj_id", $proj_id);

      //add the new project
      $event_args["register_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newProject", $event_args);

      //save the specific data according to the status of the project
      $status_id = $this->getSVar("status_id");
      switch ($status_id)
        {
        default:
        case CONTRACTED:
          $query = "newContracted";
          break;
        case PROPOSAL:
          $query = "newProposal";
          break;
        case QUALIFIED:
        case OPPORTUNITY:
          $query = "newOpportunity";
          break;
        }
      WebApp::execDBCmd($query, $event_args);

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'projects->recount' to "true"
      //so that the records are counted again
      //and the new project is displayed at the end of the list
      WebApp::setSVar("projects->recount", "true");

      WebApp::setSVar("modify->editFile", "editProject/edit.html");
    }

  function add_listsize_rs()
    {
      //get the count RS (the number of subsectors for each sector)
      $rs = WebApp::openRS("subsect_counts");

      //find the max count
      $max_count = 0;
      while (!$rs->EOF())
        {
          $count = $rs->Field("count");
          if ($count > $max_count)  $max_count = $count;
          $rs->MoveNext();
        }

      //build the RS listsize
      $rs = new EditableRS("subsectlistsize");
      for ($i=0; $i < $max_count; $i++)
        {
          $rec = array("dummy"=>"");
          $rs->addRec($rec);
        }

      //add this recordset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>